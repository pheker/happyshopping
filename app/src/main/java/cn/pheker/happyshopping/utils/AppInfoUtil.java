package cn.pheker.happyshopping.utils;

import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;

/**
 * @author: zn E-mail:zhangn@jtv.com.cn 
 * @version:2014-10-22
 * 类说明
 */

public class AppInfoUtil {

	/**
	 * 获取应用程序包名
	 * @author:zn
	 * @version:2014-10-22
	 * @param context
	 * @return
	 */
	public static String getAppPackageName(Context context) {
 		try {
 			return context.getPackageName();
 		} catch (Exception e) {
 		}
 		return null;
 	}
	
	/**
	 * 获取版本号
	 * @return 当前应用的版本号
	 */
	public static int getVersion(Context context) {
	    try {
	        PackageManager manager = context.getPackageManager();
	        PackageInfo info = manager.getPackageInfo(context.getPackageName(), 0);
	        int version = info.versionCode;
	        return version;
	    } catch (Exception e) {
	        e.printStackTrace();
	        return 1;
	    }
	}
}
