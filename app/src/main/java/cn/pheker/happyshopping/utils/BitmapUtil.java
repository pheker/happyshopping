package cn.pheker.happyshopping.utils;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.util.Log;
import android.view.Display;
import android.view.View;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author: zn E-mail:zhangn@jtv.com.cn
 * @version:2015-1-28 类说明
 */

public class BitmapUtil {

	private static BitmapUtil imageUtil = new BitmapUtil();

	private BitmapUtil(){
	}

	public static BitmapUtil getInstance() {
		if (imageUtil == null) {
			imageUtil = new BitmapUtil();
			return imageUtil;
		}
		return imageUtil;
	}

	// 将byte[]转换成InputStream
	public InputStream Byte2InputStream(byte[] b) {
		ByteArrayInputStream bais = new ByteArrayInputStream(b);
		return bais;
	}

	// 将InputStream转换成byte[]
	public byte[] InputStream2Bytes(InputStream is) {
		String str = "";
		byte[] readByte = new byte[1024];
		int readCount = -1;
		try {
			while ((readCount = is.read(readByte, 0, 1024)) != -1) {
				str += new String(readByte).trim();
			}
			return str.getBytes();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

	// 将Bitmap转换成InputStream
	public InputStream Bitmap2InputStream(Bitmap bm) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.JPEG, 100, baos);
		InputStream is = new ByteArrayInputStream(baos.toByteArray());
		return is;
	}

	// 将Bitmap转换成InputStream
	public InputStream Bitmap2InputStream(Bitmap bm, int quality) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, quality, baos);
		InputStream is = new ByteArrayInputStream(baos.toByteArray());
		return is;
	}

	// 将InputStream转换成Bitmap
	public Bitmap InputStream2Bitmap(InputStream is) {
		return BitmapFactory.decodeStream(is);
	}

	// Drawable转换成InputStream
	public InputStream Drawable2InputStream(Drawable d) {
		Bitmap bitmap = this.drawable2Bitmap(d);
		return this.Bitmap2InputStream(bitmap);
	}

	// InputStream转换成Drawable
	public Drawable InputStream2Drawable(InputStream is) {
		Bitmap bitmap = this.InputStream2Bitmap(is);
		return this.bitmap2Drawable(bitmap);
	}

	// Drawable转换成byte[]
	public byte[] Drawable2Bytes(Drawable d) {
		Bitmap bitmap = this.drawable2Bitmap(d);
		return this.Bitmap2Bytes(bitmap);
	}

	// byte[]转换成Drawable
	public Drawable Bytes2Drawable(byte[] b) {
		Bitmap bitmap = this.Bytes2Bitmap(b);
		return this.bitmap2Drawable(bitmap);
	}

	// Bitmap转换成byte[]
	public byte[] Bitmap2Bytes(Bitmap bm) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		bm.compress(Bitmap.CompressFormat.PNG, 100, baos);
		return baos.toByteArray();
	}

	// byte[]转换成Bitmap
	public Bitmap Bytes2Bitmap(byte[] b) {
		if (b.length != 0) {
			return BitmapFactory.decodeByteArray(b, 0, b.length);
		}
		return null;
	}

	// Drawable转换成Bitmap
	public Bitmap drawable2Bitmap(Drawable drawable) {
		Bitmap bitmap = Bitmap
				.createBitmap(
						drawable.getIntrinsicWidth(),
						drawable.getIntrinsicHeight(),
						drawable.getOpacity() != PixelFormat.OPAQUE ? Bitmap.Config.ARGB_8888
								: Bitmap.Config.RGB_565);
		Canvas canvas = new Canvas(bitmap);
		drawable.setBounds(0, 0, drawable.getIntrinsicWidth(),
				drawable.getIntrinsicHeight());
		drawable.draw(canvas);
		return bitmap;
	}

	// Bitmap转换成Drawable
	public Drawable bitmap2Drawable(Bitmap bitmap) {
		BitmapDrawable bd = new BitmapDrawable(bitmap);
		Drawable d = (Drawable) bd;
		return d;
	}
	
	
	/**
	 * 获取缩略图路径
	 * @author:zn
	 * @version:2015-2-13
	 * @param oldPath
	 * @param bitmapMaxWidth
	 * @param photoPath
	 * @return
	 * @throws Exception
	 */
	public static String getThumbUploadPath(String oldPath,int bitmapMaxWidth,String photoPath) throws Exception {
        Options options = new Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(oldPath, options);
        int height = options.outHeight;
        int width = options.outWidth;
        int reqHeight = 0;
        int reqWidth = bitmapMaxWidth;
        reqHeight = (reqWidth * height)/width;
        // 在内存中创建bitmap对象，这个对象按照缩放大小创建的
        options.inSampleSize = calculateInSampleSize(options, bitmapMaxWidth, reqHeight);
        options.inJustDecodeBounds = false;
        Bitmap bitmap = BitmapFactory.decodeFile(oldPath, options);
        //Log.e("asdasdas", "reqWidth->"+reqWidth+"---reqHeight->"+reqHeight);
        Bitmap bbb = compressImage(Bitmap.createScaledBitmap(bitmap, bitmapMaxWidth, reqHeight, false));
        //String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        return BitmapUtil.saveImg(bbb, photoPath);
	}
	
	public  static int calculateInSampleSize(Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
                if (width > height) {
                        inSampleSize = Math.round((float) height / (float) reqHeight);
                }else{
                        inSampleSize = Math.round((float) width / (float) reqWidth);
                }
        }
        return inSampleSize;
	}

	public static Bitmap compressImage(Bitmap image) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.JPEG, 30, baos);// 质量压缩方法，这里100表示不压缩，把压缩后的数据存放到baos中
        int options = 100;
        while (options >= 50 && baos.toByteArray().length / 1024 > 200) { // 循环判断如果压缩后图片是否大于100kb,大于继续压缩
                options -= 10;// 每次都减少10
                baos.reset();// 重置baos即清空baos
                image.compress(Bitmap.CompressFormat.PNG, options, baos);// 这里压缩options%，把压缩后的数据存放到baos中
        }
        ByteArrayInputStream isBm = new ByteArrayInputStream(baos.toByteArray());// 把压缩后的数据baos存放到ByteArrayInputStream中
        Bitmap bitmap = BitmapFactory.decodeStream(isBm, null, null);// 把ByteArrayInputStream数据生成图片
        return bitmap;
	}
	

    /**
     *
     * @param   b
     * @return 图片存储的位置
     * @throws FileNotFoundException
     */
    public static String saveImg(Bitmap b,String photoPath) throws Exception{
            //String path = Environment.getExternalStorageDirectory().getPath()+File.separator+"test/headImg/";
    		File mediaFile = new File(photoPath);
            if(mediaFile.exists()){
                 mediaFile.delete();
        
           }
//            if(!new File(path).exists()){
//                    new File(path).mkdirs();
//            }
            mediaFile.createNewFile();
            FileOutputStream fos = new FileOutputStream(mediaFile);
            b.compress(Bitmap.CompressFormat.JPEG, 30, fos);
            fos.flush();
            fos.close();
            b.recycle();
            b = null;
            System.gc();
            return mediaFile.getPath();
    }

    
    
 	/**
 	 * 有损压缩图片大小
 	 * @param filepath
 	 * @param quality
 	 * @return Bitmap
 	 * @throws IOException
 	 */
 	@SuppressWarnings("deprecation")
	public static Bitmap compressImage(Activity aty, String filepath, int quality) throws IOException {
 		Display display = aty.getWindowManager().getDefaultDisplay(); // 显示屏尺寸 
        float destWidth = display.getWidth(); 
        float destHeight = display.getHeight();  

 		Options options = new Options();
 		options.inJustDecodeBounds = true;// 设置为true，options依然对应此图片，但解码器不会为此图片分配内存
 		BitmapFactory.decodeFile(filepath, options);
 		
 		float srcWidth = options.outWidth; 
        float srcHeight = options.outHeight;  
        
        int inSampleSize = 1; 
        if (srcHeight > destHeight || srcWidth > destWidth) { // 当图片长宽大于屏幕长宽时 
            if (srcWidth > srcHeight) { 
                inSampleSize = Math.round(srcHeight / destHeight); 
            } else { 
                inSampleSize = Math.round(srcWidth / destWidth); 
            } 
//        	double temp = 0;
//        	if (srcWidth > srcHeight){
//        		temp = Math.log(destHeight / (double) srcHeight);
//        	} else {
//        		temp = Math.log(destWidth / (double) srcWidth);
//			}
//        	inSampleSize = (int) Math.pow(2.0, (int) Math.round(temp / Math.log(0.5)));
        } 
//        options = new BitmapFactory.Options(); 
        options.inSampleSize = inSampleSize; 
 		options.inJustDecodeBounds = false;
   
        Bitmap bitMap = BitmapFactory.decodeFile(filepath, options);
// 		// 上传图片最大宽高
// 		int IMAGE_MAX_WIDTH = 1000;
// 		int IMAGE_MAX_HEIGHT = 800;
//      int scale = 1;
// 		Bitmap bitMap = BitmapFactory.decodeFile(filepath, options);
// 		if (o.outWidth > IMAGE_MAX_WIDTH || o.outHeight > IMAGE_MAX_HEIGHT) {
// 			scale = (int) Math.pow(2.0, (int) Math.round(Math.log(IMAGE_MAX_WIDTH / (double) Math.max(o.outHeight, o.outWidth)) / Math.log(0.5)));
// 		}
// 		Log.d("image-scale", scale + " scale");

// 		options.inJustDecodeBounds = false;
// 		options.inSampleSize = scale;
// 		bitMap = BitmapFactory.decodeFile(filepath, o);
// 		bitMap = resizeBitmap(bitMap, IMAGE_MAX_WIDTH, IMAGE_MAX_HEIGHT);

// 		Bitmap bitMap = BitmapFactory.decodeFile(filepath);
 		BufferedOutputStream bos = null;
 		try {
 			bos = new BufferedOutputStream(new FileOutputStream(filepath));
 			bitMap.compress(Bitmap.CompressFormat.JPEG, quality, bos);
 			bos.close();
 		} catch (IOException ioe) {
 			Log.e("compress image", ioe.getMessage());
 		}
 		return bitMap;
 	}
 	
 	/**
 	 * 保持长宽比缩小Bitmap
 	 * 
 	 * @param bitmap
 	 * @param maxWidth
 	 * @param maxHeight
 	 * @return
 	 */
 	public static Bitmap resizeBitmap(Bitmap bitmap, int maxWidth, int maxHeight) {

 		int originWidth = bitmap.getWidth();
 		int originHeight = bitmap.getHeight();

 		// no need to resize
 		if (originWidth < maxWidth && originHeight < maxHeight) {
 			return bitmap;
 		}

 		int newWidth = originWidth;
 		int newHeight = originHeight;

 		// 若图片过宽, 则保持长宽比缩放图片
 		if (originWidth > maxWidth) {
 			newWidth = maxWidth;

 			double i = originWidth * 1.0 / maxWidth;
 			newHeight = (int) Math.floor(originHeight / i);

 			bitmap = Bitmap.createScaledBitmap(bitmap, newWidth, newHeight, true);
 		}

 		// 若图片过长, 则从中部截取
 		if (newHeight > maxHeight) {
 			newHeight = maxHeight;

 			int half_diff = (int) ((originHeight - maxHeight) / 2.0);
 			bitmap = Bitmap.createBitmap(bitmap, 0, half_diff, newWidth, newHeight);
 		}
 		return bitmap;
 	}
 	
	public static Bitmap ScreenShot(Activity activity) {
        // 获取windows中最顶层的view
        View view = activity.getWindow().getDecorView();
        view.buildDrawingCache();
        // 获取状态栏高度
        Rect rect = new Rect();
        view.getWindowVisibleDisplayFrame(rect);
        int statusBarHeights = rect.top;
        Display display = activity.getWindowManager().getDefaultDisplay();
 
        // 获取屏幕宽和高
        int widths = display.getWidth();
        int heights = display.getHeight();
 
        // 允许当前窗口保存缓存信息
        view.setDrawingCacheEnabled(true);
 
        // 去掉状态栏
        Bitmap bmp = Bitmap.createBitmap(view.getDrawingCache(), 0,statusBarHeights, widths, heights - statusBarHeights);
        // 销毁缓存信息
        view.destroyDrawingCache();
        return bmp;
    }
	
	public static void saveToSD(Bitmap bmp, String dirName,String fileName) throws IOException {
        // 判断sd卡是否存在
//        if (Environment.getExternalStorageState().equals( Environment.MEDIA_MOUNTED)) {
//            
//        }
		File dir = new File(dirName);
        // 判断文件夹是否存在，不存在则创建
        if(!dir.exists()){
            dir.mkdirs();
        }
        File file = new File(dirName + fileName);
        // 判断文件是否存在，不存在则创建
        if (!file.exists()) {
            file.createNewFile();
        }
  
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
            if (fos != null) {
                // 第一参数是图片格式，第二个是图片质量，第三个是输出流
                bmp.compress(Bitmap.CompressFormat.PNG, 100, fos);
                // 用完关闭
                fos.flush();
                fos.close();
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
	
	public static Bitmap decodeBitmap(Activity aty,String path){
		Display display = aty.getWindowManager().getDefaultDisplay(); // 显示屏尺寸 
		return decodeBitmap(aty,path,display.getWidth(),display.getHeight());
	}
	
	public static Bitmap decodeBitmap(Activity aty,String path,int displayWidth,int dispalyHeight){
		Display display = aty.getWindowManager().getDefaultDisplay(); // 显示屏尺寸 
        int maxNumOfPixels = display.getWidth() * display.getHeight();//dispalyHeight * dispalyHeight*3/4;//4:3,1280*960;
		
		Options opts = new Options();
		opts.inJustDecodeBounds = true;
		BitmapFactory.decodeFile(path, opts);
		opts.inSampleSize = computeSampleSize(opts,-1,maxNumOfPixels);
		opts.inJustDecodeBounds = false;
		opts.inInputShareable = true;
		opts.inDither = false;
		opts.inPurgeable = true;
		opts.inTempStorage = new byte[16*1024];
		FileInputStream is = null;
		Bitmap bmp = null;
		ByteArrayOutputStream baos = null;
		try{
			is = new FileInputStream(path);
			bmp = BitmapFactory.decodeFileDescriptor(is.getFD(),null,opts);
			
			ExifInterface exif = new ExifInterface(path);//取出图片的宽高比
			//三星机型拍照时，默认会把图片旋转90度
			int result = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
					ExifInterface.ORIENTATION_UNDEFINED);
			int rotate = 0;
			if(result == ExifInterface.ORIENTATION_ROTATE_90)
				rotate = 90;
			else if(result == ExifInterface.ORIENTATION_ROTATE_180)
				rotate = 180;
			else if(result == ExifInterface.ORIENTATION_ROTATE_270)
				rotate = 270;
			if(rotate >0){
				Matrix matrix = new Matrix(); 
		        matrix.setRotate(rotate); 
		        bmp = Bitmap.createBitmap(bmp, 0, 0, bmp.getWidth(), bmp.getHeight(), matrix, true); 
			}
			
			Bitmap bmp2 = null;
			if(opts.inSampleSize == 1)
				bmp2 = bmp;
			else{
				if(opts.outWidth < opts.outHeight || rotate == 90 || rotate == 270)//竖着拍照
					bmp2 = Bitmap.createScaledBitmap(bmp,displayWidth,dispalyHeight,true);
				else{//横着拍照
					int dheight = displayWidth;
					int width = exif.getAttributeInt(ExifInterface.TAG_IMAGE_WIDTH,0);
					int height = exif.getAttributeInt(ExifInterface.TAG_IMAGE_LENGTH, 0);
					if(width != 0 && height !=0){
						if((double)width/height == (double)16/9)//16:9,1280*720
							dheight = displayWidth * 9 /16;
						else if((double)width/height == (double)4/3)//16:9,1280*720
							dheight = displayWidth * 3 /4;
						else if(width == height)//1:1
							dheight = displayWidth;
						bmp2 = Bitmap.createScaledBitmap(bmp,displayWidth,dheight,true);
					}
				}
			}
			
			baos = new ByteArrayOutputStream();
			bmp2.compress(Bitmap.CompressFormat.JPEG , 100, baos);
			
			bmp.recycle();
			bmp2.recycle();
		}catch(FileNotFoundException e){
			Log.e("", e.toString());			
		}catch (Exception e) {
			Log.e("", e.toString());
		}finally{
			try{
				is.close();
				baos.close();
			}catch(IOException e){
				
			}
			System.gc();
		}
		byte[] data =  baos.toByteArray();
		return data==null?null:BitmapFactory.decodeByteArray(data, 0, data.length);
	}
	
	private static double getScaling(int src,int des) {
		double scale = Math.sqrt((double)des/(double)src);
		return scale;
	}
	
	public static int computeSampleSize(Options options,
			int minSideLength,int maxNumOfPixels){
		int initialSize = computeInitialSampleSize(options,minSideLength,maxNumOfPixels);
		
		int roundedSize;
		if(initialSize <=8){
			roundedSize = 1;
			while (roundedSize < initialSize) {
				roundedSize <<=1;
			}
		}else{
			roundedSize = (initialSize + 7)/8*8;
		}
		return roundedSize;
	}
	
	public static int computeInitialSampleSize(Options options,
			int minSideLength,int maxNumOfPixels){
		double w = options.outWidth;
		double h = options.outHeight;
		
		int lowerBound = (maxNumOfPixels == -1)?1:(int)Math.ceil(Math
				.sqrt(w*h/maxNumOfPixels));
		int upperBound = (minSideLength == -1)?1:(int)Math.min(
				Math.floor(w/minSideLength), Math.floor(h/minSideLength));
		
		if(upperBound <lowerBound)
			return lowerBound;
		
		if(maxNumOfPixels == -1 && minSideLength == -1)
			return 1;
		else if(minSideLength == -1)
			return lowerBound;
		else
			return upperBound;
	}
}
