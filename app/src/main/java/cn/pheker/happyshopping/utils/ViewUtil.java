package cn.pheker.happyshopping.utils;

import android.app.Activity;
import android.content.Context;
import android.graphics.Rect;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout.LayoutParams;

import java.util.ArrayList;
import java.util.List;

/**
 * @author: zn E-mail:zhangn@jtv.com.cn 
 * @version:2015-3-18
 * 类说明
 */

public class ViewUtil {

	/**
	 * 获取状态栏高度
	 * @author:zn
	 * @version:2015-3-18
	 * @param context
	 * @return
	 */
	public static int getStatusHeight(Activity context){
		Rect frame = new Rect();
		context.getWindow().getDecorView().getWindowVisibleDisplayFrame(frame);
		return frame.top;
	}
	
	/**
	 * 获取view宽度
	 * @author:zn
	 * @version:2015-3-18
	 * @param view
	 * @return
	 */
	public static int getViewWidth(View view){
		int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
		int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
		view.measure(w, h);
  		
  		return view.getMeasuredWidth();
		//return view.getLayoutParams().width;
	}
	/**
	 * 获取view高度
	 * @author:zn
	 * @version:2015-3-18
	 * @param view
	 * @return
	 */
	public static int getViewHeight(View view){
		int w = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
		int h = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
		view.measure(w, h);
		return view.getMeasuredHeight();
		//return view.getLayoutParams().height;
	}

	public static View buildSpaceView(Context mContext){
		View view=new View(mContext);
		LayoutParams lp=new LayoutParams(DensityUtil.dip2px(mContext, 4), LayoutParams.MATCH_PARENT);
		view.setLayoutParams(lp);
		return view;
	}
	


	/**
	 * 获取layout中的所有子view
	 * 
	 * @Description: TODO(用一句话描述该方法做什么)
	 * @author mouw DateTime 2014-10-17 下午4:30:00
	 * @param aty
	 * @return
	 */
	public static List<View> getAllChildViews(Activity aty) {
		View view = aty.getWindow().getDecorView();
		return getAllChildViews(aty, view);

	}

	public static List<View> getAllChildViews(Activity aty, View view) {
		List<View> allchildren = new ArrayList<View>();
		if (view instanceof ViewGroup) {
			ViewGroup vp = (ViewGroup) view;
			for (int i = 0; i < vp.getChildCount(); i++) {
				View viewchild = vp.getChildAt(i);
				allchildren.add(viewchild);
				allchildren.addAll(getAllChildViews(aty, viewchild));
			}
		}

		return allchildren;

	}
}
