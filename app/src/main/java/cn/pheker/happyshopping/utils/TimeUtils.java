package cn.pheker.happyshopping.utils;

import java.util.Calendar;
import java.util.TimeZone;

/**
 * 
 * @author: Biao
 * @创建时间: 2016-1-13 上午11:43:38
 * @描述信息: 时间工具类封装
 * @svn提交者: $Author$
 * @提交时间: $Date$
 * @当前版本: $Rev$
 */
public class TimeUtils {
	/**
	 * 获取当前日期
	 * 
	 * @return
	 */
	public static String getCurrentDate() {
		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
		int mYear = c.get(Calendar.YEAR);
		// 获取当前月份
		int mMonth = c.get(Calendar.MONTH) + 1;
		// 获取当前月份的日期号码
		int mDay = c.get(Calendar.DAY_OF_MONTH);

		return mYear + "-" + mMonth + "-" + mDay;
	}

	/**
	 * 获取明天日期
	 * 
	 * @return
	 */
	public static String getTomorrowDate() {
		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
		int mYear = c.get(Calendar.YEAR);
		// 获取当前月份
		int mMonth = c.get(Calendar.MONTH) + 1;
		// 获取当前月份的日期号码
		int mDay = c.get(Calendar.DAY_OF_MONTH);

		return mYear + "-" + mMonth + "-" + (mDay + 1);
	}

	/**
	 * 获取当前时间
	 * 
	 * @return
	 */
	public static String getCurrentTime() {
		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
		int mHour = c.get(Calendar.HOUR_OF_DAY);
		// 获取当前分钟
		int mMinute = c.get(Calendar.MINUTE);

		return mHour + ":" + mMinute;
	}

	/**
	 * 获取当前小时数
	 * 
	 * @return
	 */
	public static int getCurrentHour() {
		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
		int mHour = c.get(Calendar.HOUR_OF_DAY);
		return mHour;
	}

	/**
	 * 获取当前分钟数
	 * 
	 * @return
	 */
	public static int getCurrentMinute() {
		Calendar c = Calendar.getInstance();
		c.setTimeZone(TimeZone.getTimeZone("GMT+8:00"));
		int mMinute = c.get(Calendar.MINUTE);
		return mMinute;
	}

}
