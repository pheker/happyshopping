package cn.pheker.happyshopping.utils;

import android.app.Activity;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup.MarginLayoutParams;
import android.view.ViewParent;
import android.widget.AbsListView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

/**
 * 
 * @author: Biao
 * @创建时间: 2016-1-12 下午5:21:24
 * @描述信息: 控件工具类封装
 * @svn提交者: $Author$
 * @提交时间: $Date$
 * @当前版本: $Rev$
 */
public class ViewUtils {

	/** 得到分辨率高度 */
	public static int heightPs = -1;
	/** 得到分辨率宽度 */
	public static int widthPs = -1;
	/** 得到屏幕密度 */
	public static int densityDpi = -1;
	/** 得到X轴密度 */
	public static float Xdpi = -1;
	/** 得到Y轴密度 */
	public static float Ydpi = -1;

	/** 切图上的宽 ***/
	private static int CutWidth = 1080;

	/**
	 * 
	 * 
	 * @param width
	 * @return
	 */
	public static int getViewWidth(int width) {
		return widthPs * width / CutWidth;
	}

	/**
	 * 针对切图,获取等比例的高
	 * 
	 * @param width
	 * @param height
	 * @return
	 */
	public static int getViewHeight(int width, int height) {
		return height * getViewWidth(width) / width;
	}

	/**
	 * 设置view的宽高大小,输入切图上的大小,代码会在不同机子上进行不同比例的放大缩小
	 * 
	 * @param view
	 * @param width
	 * @param height
	 */
	public static void setViewSize(View view, int width, int height) {
		if (view == null) {
			return;

		}
		android.view.ViewGroup.LayoutParams params = view.getLayoutParams();

		int trueWidth = widthPs * width / CutWidth;
		int trueHeight = height * trueWidth / width;

		try {
			ViewParent parent = view.getParent();
			if (parent != null) {
				if (parent instanceof LinearLayout) {
					if (null == params) {
						params = new LinearLayout.LayoutParams(trueWidth,
								trueHeight);
						view.setLayoutParams(params);
						return;
					}
				} else if (parent instanceof RelativeLayout) {
					if (null == params) {
						params = new RelativeLayout.LayoutParams(trueWidth,
								trueHeight);
						view.setLayoutParams(params);
						return;
					}
				} else if (parent instanceof AbsListView) {
					if (null == params) {
						params = new AbsListView.LayoutParams(trueWidth,
								trueHeight);
						view.setLayoutParams(params);
						return;
					}
				} else {
					if (null == params) {
						params = new LinearLayout.LayoutParams(trueWidth,
								trueHeight);
						view.setLayoutParams(params);
						return;
					}
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (params != null) {
			params.width = trueWidth;
			params.height = trueHeight;
		}
	}

	/**
	 * 设置控件的边缘
	 * 
	 * @param view
	 * @param left
	 * @param top
	 * @param right
	 * @param bottom
	 */
	public static void setViewMargin(View view, int left, int top, int right,
			int bottom) {
		try {
			MarginLayoutParams params = (MarginLayoutParams) view
					.getLayoutParams();
			params.leftMargin = getViewWidth(left);
			params.topMargin = getViewWidth(top);
			params.rightMargin = getViewWidth(right);
			params.bottomMargin = getViewWidth(bottom);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * 设置控件的间隙
	 * 
	 * @param view
	 * @param left
	 * @param top
	 * @param right
	 * @param bottom
	 */
	public static void setViewPadding(View view, int left, int top, int right,
			int bottom) {
		view.setPadding(getViewWidth(left), getViewWidth(top),
				getViewWidth(right), getViewWidth(bottom));
	}

	/**
	 * 设置字体大小
	 * 
	 * @param tv
	 * @param size
	 */

	public static void setTextSize(TextView tv, int size) {
		tv.setTextSize(TypedValue.COMPLEX_UNIT_PX, widthPs * size / CutWidth);
	}

	/***
	 * 得到手机的屏幕基本信息 一开始要先调用这个
	 * 
	 * @param activity
	 */
	public static void initScreen(Activity activity) {
		DisplayMetrics metrics = new DisplayMetrics();
		activity.getWindowManager().getDefaultDisplay().getMetrics(metrics);
		heightPs = metrics.heightPixels;
		widthPs = metrics.widthPixels;
		densityDpi = metrics.densityDpi;
		Xdpi = metrics.xdpi;
		Ydpi = metrics.ydpi;
		Log.d("", "分辨率：" + widthPs + "X" + heightPs + "    屏幕密度：" + densityDpi
				+ "    宽高密度：" + Xdpi + "X" + Ydpi);
	}

	/**
	 * 把密度dip单位转化为像数px单位
	 * 
	 * @param context
	 * @param dip
	 * @return
	 */
	public static int dipToPx(Context context, int dip) {
		float scale = context.getResources().getDisplayMetrics().density;
		return (int) (dip * scale + 0.5f * (dip >= 0 ? 1 : -1));
	}
}
