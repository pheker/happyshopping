package cn.pheker.happyshopping.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author: Biao
 * @创建时间: 2016-3-17 上午11:33:53
 * @描述信息: 电话工具类 封装
 * @svn提交者: $Author$
 * @提交时间: $Date$
 * @当前版本: $Rev$
 */
public class TelephoneUtils {
	/**
	 * 判断是否为有效的手机号
	 * 
	 * @param mobiles
	 *            手机号
	 * @return true代表有效 false代表无效
	 */
	public static boolean isMobileNO(String mobiles) {
		Pattern p = Pattern
				.compile("^((13[0-9])|(15[^4,\\D])|(17[0678])|(14[457])|(18[0-9]))\\d{8}$");
		Matcher m = p.matcher(mobiles);
		return m.matches();
	}

	/**
	 * 屏蔽手机号的中间4位以*代替
	 * 
	 * @param phoneNumber
	 * @return
	 */
	public static String shieldPhoneNumber(String phoneNumber) {
		return phoneNumber.replaceAll("(\\d{3})\\d{4}(\\d{4})", "$1****$2");
	}
}
