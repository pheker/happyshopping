package cn.pheker.happyshopping.utils;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 
 * @author: Biao
 * @创建时间: 2015-8-18 下午12:30:37
 * @描述信息: MD5加密工具类
 * @svn提交者: $Author$
 * @提交时间: $Date: 2015-11-21 14:56:11 +0800 (Sat, 21 Nov 2015) $
 * @当前版本: $Rev: 39 $
 */
public class Md5Utils {
	/**
	 * md5加密
	 * 
	 * @param str
	 * @return
	 */
	public static String encode(String str) {
		StringBuilder sb = new StringBuilder();

		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] digest = md.digest(str.getBytes());
			for (byte b : digest) {
				int data = b & 0xff;
				String hex = Integer.toHexString(data);
				if (hex.length() == 1) {
					hex = "0" + hex;
				}
				sb.append(hex);
			}
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/**
	 * 获取文件的md5值
	 * 
	 * @param file
	 * @return
	 */
	public static String getFileMd5(File file) {
		StringBuilder sb = new StringBuilder();
		try {
			// 创建md5实例
			MessageDigest md = MessageDigest.getInstance("MD5");
			// 读取文件内容
			BufferedInputStream bis = new BufferedInputStream(
					new FileInputStream(file));
			int len = -1;
			byte[] bys = new byte[1024];
			// 读取数据
			while ((len = bis.read(bys)) != -1) {
				// 加密数据
				md.update(bys, 0, len);
			}
			bis.close();
			// 获取加密后的数据
			byte[] digest = md.digest();
			// 遍历
			for (byte b : digest) {
				int data = b & 0xff;
				String hex = Integer.toHexString(data);
				if (hex.length() == 1) {
					hex = "0" + hex;
				}
				sb.append(hex);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return sb.toString();
	}

	/**
	 * 获取内容摘要
	 * 
	 * @param buffer
	 * @return
	 */
	public final static String getMessageDigest(byte[] buffer) {
		char hexDigits[] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
				'a', 'b', 'c', 'd', 'e', 'f' };
		try {
			MessageDigest mdTemp = MessageDigest.getInstance("MD5");
			mdTemp.update(buffer);
			byte[] md = mdTemp.digest();
			int j = md.length;
			char str[] = new char[j * 2];
			int k = 0;
			for (int i = 0; i < j; i++) {
				byte b = md[i];
				str[k++] = hexDigits[b >>> 4 & 0xf];
				str[k++] = hexDigits[b & 0xf];
			}
			return new String(str);
		} catch (Exception e) {
			return null;
		}
	}
}
