package cn.pheker.happyshopping.utils;
/**
 * @author: zn E-mail:zhangn@jtv.com.cn 
 * @version:2015-3-16
 * 类说明
 */

public class StatusInfoUtil {

	/**
	 * 周、月、季度
	 * 通过状态名返回对应状态参数
	 * @author:zn
	 * @version:2015-3-16
	 * @param statusCode(如：本周返回周，本月返回月)
	 * @return
	 */
	public static String getStatusSjd(String statusCode){
		if("本周".equalsIgnoreCase(statusCode)){
			return "week";
		}else if("本月".equalsIgnoreCase(statusCode)){
			return "month";
		}else if("本季度".equalsIgnoreCase(statusCode)){
			return "quarter";
		}
		return "";
	}
	/**
	 * 检查或者整改
	 * @author:zn
	 * @version:2015-3-16
	 * @param statusCode(检查:1;整改:2)
	 * @return
	 */
	public static String getStatusJcorZg(String statusCode){
		if("检查".equalsIgnoreCase(statusCode)){
			return "1";
		}else if("整改".equalsIgnoreCase(statusCode)){
			return "2";
		}
		return "";
	}
	
}
