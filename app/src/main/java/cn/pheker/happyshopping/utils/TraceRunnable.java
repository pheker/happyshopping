package cn.pheker.happyshopping.utils;

import java.io.BufferedReader;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
public class TraceRunnable implements Runnable{
	@Override
	public void run() {
		Process mLogcatProc = null;   
		BufferedReader reader = null;
		OutputStream out=null;
		try {   
		    //获取logcat日志信息   
		    mLogcatProc = Runtime.getRuntime().exec(new String[] {"logcat","*:e"});   
		    reader = new BufferedReader(new InputStreamReader(mLogcatProc.getInputStream()));   
		    out=new FileOutputStream("/mnt/sdcard/marineLog.txt");
		    String line;   
		 while(true){      
		    while ((line = reader.readLine()) != null) {   
		          out.write(line.getBytes()); 
		          out.flush();
      	    }   
		  }
		} catch (Exception e) {   
		   
		    e.printStackTrace();   
		}   
	}

}
