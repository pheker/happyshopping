package cn.pheker.happyshopping.utils;

/**
 * 
 * @author: Biao
 * @创建时间: 2016-3-9 下午3:18:58
 * @描述信息: 整个应用常量的定义
 * @svn提交者: $Author$
 * @提交时间: $Date$
 * @当前版本: $Rev$
 */
public class LotusConstants {
	// 共享参数文件名
	public static final String APK_CONFIG = "config.xml";
	// 调试模式是否打开(正式发布时建议关闭)
	public static final boolean DEBUG_MODE_ENABLE = false;
	// 是否设置过向导界面
	public static final String IS_SET = "isset";
	// 是否设置过卖家号
	public static final String IS_SET_NAI_CHA_ACCOUNT = "is_set_nai_cha_account";
	// 手机号标记
	public static final String PHONE_NUMBER = "phone";
	// 密码
	public static final String PASSWORD = "password";
	// 帐户
	public static final String ACCOUNT = "phone";
	// 邮资
	public static final String POSTAGE = "postage";
	// 代发货编码
	public static final String REPLACE_DELIVERY_CODE = "replaceDeliveryCode";
	// 代发货退回标记
	public static final String DELIVERY_RETURN_FLAG = "delivery_return_flag";
	// 代发货商品信息
	public static final String REPLACE_GOODS_INFO = "replaceGoodsInfo";
	// 收货信息编号
	public static final String RECEIVE_INFO_ID = "receiveingInfoId";
	// 收货地址变化标记
	public static final String RECEIVE_ADDRESS_CHANGE_FLAG = "receive_address_change_flag";
	// 用户标记
	public static final String TOKEN = "token";
	// 状态
	public static final String STATE = "state";
	// 商品条目编号
	public static final String GOODS_ITEM_ID = "itemId";
	// 商品标题
	public static final String GOODS_TITLE = "title";
	// 商店主标题
	public static final String SHOP_TITLE = "shopTitle";
	// 商店子标题
	public static final String SHOP_SUBTITLE = "shopSubtitle";
	// 商品价格
	public static final String GOODS_PRICE = "price";
	// 描述
	public static final String GOODS_DESC = "content";
	// 商品推荐标记
	public static final String GOODS_RECOMMEND_FLAG = "goods_recommend_flag";
	// 购买方式
	public static final String GOODS_BUY_WAY = "way";
	// 需求编号
	public static final String GOODS_REQUIRE_ID = "requireId";
	// 商品数量
	public static final String GOODS_AMONUT = "amount";
	// 商品总价
	public static final String GOODS_SUM = "sum";
	// 商品收获信息
	public static final String GOODS_RECEIVE_INFO = "receiveInfo";
	// 商品备注
	public static final String GOODS_REMARK = "remark";
	// 商品首张图片路径
	public static final String GOODS_PICPATH = "picPath";
	// 用户类型
	public static final String USER_TYPE = "userType";
	// 商业类型
	public static final String BUSINESS_TYPE = "businessType";
	// 买家发布需求标记
	public static final String BUYER_RELEASE_NEED_GOODS_FLAG = "buyer_release_need_goods_flag";
	// 物流回执标记
	public static final String LOGISTICS_RECEIPT_FLAG = "logistics_receipt_flag";
	// 任务类型
	public static final String TASK_TYPE = "taskType";
	// 赏金
	public static final String REWARD = "reward";
	// 朋友的头像路径
	public static final String FRIEND_HEAD_PICTURE_URL = "friend_head_picture_url";
	// 朋友的名字
	public static final String FRIEND_NAME = "friend_name";
	// 朋友的性别
	public static final String FRIEND_SEX = "friend_sex";
	// 朋友的职业
	public static final String FRIEND_PROFESSION = "friend_profession";
	// 单价赏金
	public static final String PERREWARD = "perReward";
	// 服务时间
	public static final String SERVICES_TIME = "servicesTime";
	// 时长
	public static final String TIME_LENGTH = "timeLength";
	// 备注
	public static final String NOTES = "notes";
	// 维度
	public static final String LATITUDE = "latitude";
	// 经度
	public static final String LONGITUDE = "longitude";
	// 任务未选
	public static final String NOT_CHECK_TASK = "0";
	// 任务过期
	public static final String TASK_OVERDUE = "-1";
	// 任务未评价
	public static final String NOT_EVALUATE_TASK = "2";
	// 任务已支付
	public static final String ALREADY_PAYMENT_TASK = "3";
	// 任务已完成
	public static final String ALREADY_FINISH_TASK = "4";
	// 性别标记
	public static final String SEX = "sex";
	// 验证码
	public static final String VALIDATE_CODE = "validateCode";
	// 当前图片路径
	public static final String CURRENT_PHOTO_PATH = "current_photo_path";
	// 网络错误的提示消息
	public static final String NET_ERROR_MESSAGE = "网络连接不可用,请稍后再试";
	// 网络错误的提示消息
	public static final String SERVER_ERROR_MESSAGE = "系统繁忙,请稍后再试";

	// 服务类别
	public static final String CATEGORY = "category";
	// 服务时长
	public static final String DURATION = "duration";
	// 相册最多数量
	public static final int PHOTO_MAX_NUMBER = 9;
	// 物流回执相册最多数量
	public static final int LOGISTICS_RECEIPT_MAX_NUMBER = 3;
	// 认证
	public static final int AUTH_PHOTO_MAX_NUMBER = 3;
	// 团购标志值
	public static final int GROUP_BUY_FLAG = 2;
	// 团购
	public static final String GROUP_BUY_METHOD = "2";
	// 自己采购
	public static final String SELF_BUY_METHOD = "1";
	// 没有数据了
	public static final String NO_MORE_DATA = "-1";
	// 任务时长单位
	public static final String TASK_DURATION_UNIT = "分钟";
	// 已收藏
	public static final String TASK_STATE_STORE = "0x0000";
	// 已报名
	public static final String TASK_STATE_APPLY = "0x0001";
	// 已中标
	public static final String TASK_STATE_TAKE_IT = "0x0002";
	// 已完成
	public static final String TASK_STATE_FINISH = "0x0003";
	// 省级位置索引
	public static final String PROVINCE_POSITION = "province_position";
	// 省级名称
	public static final String PROVINCE_NAME = "province_name";
	// 城市位置索引
	public static final String CITY_POSITION = "city_position";
	// 城市的名称
	public static final String CITY_NAME = "city_name";
	// 地区
	public static final String AREA = "area";
	// 名字
	public static final String NAME = "name";
	// 商家描述
	public static final String SHOP_DESCRIBE = "shopDesc";
	// 好评率
	public static final String SHOP_GOOD_RATE = "shopGoodRate";
	// 商店logo
	public static final String SHOP_LOGO = "shopLogo";
	// 交易数量
	public static final String SHOP_TRADE_NUMBER = "shopTradeNumber";
	// 卖家号
	public static final String NAI_CHA_ACCOUNT = "naichaNo";
	// 手机号
	public static final String PHONE_ACCOUNT = "phone";
	// 选中图片的路径
	public static final String SELECT_PATH = "select_path";
	// 搜索关键字
	public static final String SEARCH_KEYWORD = "keyword";
	//是否是搜商品
	public static final String IS_SEARCH_GOODS = "is_search_goods";
	// 已经选择相册数据
	public static final String ALREADY_SELECT_PHOTO_LIST = "already_select_photo_list";
	// 裁剪后图片的路径
	public static final String CROP_PATH = "crop_path";
	// 买家名字
	public static final String EMPLOYER_NAME = "employer_name";
	// 买家角色
	public static final String BUYER_ROLE = "2";
	// 卖家角色
	public static final String SELLER_ROLE = "1";
	// 过滤查找的索引记录
	public static final String SEARCH_EMPLOYER_FILTER_INDEX = "search_employer_filter_index";
	public static final String SEARCH_TEACHER_FILTER_INDEX = "search_teacher_filter_index";
	// 点击位置标记
	public static final String ALBUM_CLICK_INDEX_FLAG = "album_click_index_flag";
	// 卖家点击标记
	public static final String CLICK_BY_SELLER_FLAG = "click_by_teacher_flag";
	// 首页搜索进入
	public static final String CLICK_SEARCH_FROM_HOME_FLAG = "click_search_from_home_flag";
	// 商家搜索标记
	public static final String SEARCH_SELLER_FLAG = "search_seller_flag";
	// 点击的位置
	public static final String CLICK_POSITION = "click_position";
	// 从卖家参与任务那边进入的标记
	public static final String CLICK_BY_TEACHER_ME_FLAG = "click_by_teacher_me_flag";
	// 卖家认证进入标记
	public static final String CLICK_BY_TEACHER_AUTHENTICATE_FLAG = "click_by_teacher_authenticate_flag";
	// 点击条目标记
	public static final String CLICK_GRIDVIEM_ITEM_FLAG = "click_gridviem_item_flag";
	// 年费缴纳状态改变的标记
	public static final String ANNUAL_PAY_STATE_CHANGE_FLAG = "annual_pay_state_change_flag";
	// 认证状态改变的标记
	public static final String AUTH_STATE_CHANGE_FLAG = "auth_state_change_flag";
	// 相册当前长度
	public static final String ALBUM_CURRENT_SIZE = "album_current_size";
	// 继续支付
	public static final String ORDER_STATE_CONTINUE_PAYMENT = "继续支付";
	// 收货确认
	public static final String ORDER_STATE_RECEIVE_GOODS_CONFIRM = "收货确认";
	// 去评价
	public static final String ORDER_STATE_GOTO_COMMENT = "去评价";
	// 去评价
	public static final String ORDER_STATE_ALREADY_FINISH = "已完成";
	// 选中照片的捆标记
	public static final String PHOTO_SELECT_LIST_BUNDLE = "album_select_bundle";
	// 选中照片为空标记
	public static final String ALBUM_SELECT_IS_EMPTY_FLAG = "album_select_is_empty_flag";
	// 认证相册选择标记
	public static final String AUTHENTICATION_MULTI_PHOTO_SELECT_FLAG = "authentication_multi_photo_select_flag";
	// 未绑定银行账户标记
	public static final String UNBIND_BANK_ACCOUNT_FLAG = "unbind_bank_account_flag";
	// 更改评价标记
	public static final String UPDATE_EVALUATE_FLAG = "update_evaluate_flag";
	// 相册总共照片的捆标记
	public static final String ALBUM_TOTAL_BUNDLE = "album_total_bundle";
	// 收货地址更改的捆标记
	public static final String ADDRESS_UPDATE_BUNDLE = "address_update_bundle";
	// 订单详情的捆标记
	public static final String ORDER_DETAIL_BUNDLE = "order_detail_bundle";
	// 订单编号
	public static final String ORDER_ID = "ordersId";
	// 订单收件人姓名
	public static final String ORDER_CONSIGNEE_NAME = "consigneeName";
	// 订单收件人手机号
	public static final String ORDER_PHONE = "phone";
	// 订单收件人地址
	public static final String ORDER_ADDRESS = "address";
	// 市场的捆标记
	public static final String MARKET_BUNDLE = "market_bundle";
	// 任务详情的捆标记
	public static final String TASK_DETAIL_BUNDLE = "task_detail_bundle";
	// 商品详情的捆标记
	public static final String GOODS_DETAIL_BUNDLE = "goods_detail_bundle";
	// 代发货详情的捆标记
	public static final String REPLACE_GOODS_DETAIL_BUNDLE = "replace_goods_detail_bundle";
	// 位置
	public static final String POSITION = "position";
	// 删除标记
	public static final String DELETE_FLAG = "delete_flag";
	// 从任务详情传过来的
	public static final String TASK_DETAIL_FLAG = "task_detail_flag";
	// 是否已经报名标记
	public static final String TASK_IS_ALREADY_APPLY_FLAG = "task_is_already_apply_flag";
	// 侍评价进入
	public static final String WAIT_COMMENT_FLAG = "wait_comment_flag";
	// 用户的捆标记
	public static final String USER_BUNDLE = "user_bundle";
	// 收支详情的捆标记
	public static final String INCOME_DETAIL_BUNDLE = "income_detail_bundle";
	// 年费详情的捆标记
	public static final String ANNUAL_PAY_DETAIL_BUNDLE = "annual_pay_detail_bundle";
	// 商店 的捆标记
	public static final String SHOP_BUNDLE = "shop_bundle";
	// 消息用户信息的捆标记
	public static final String MESSAGE_USER_INFO_BUNDLE = "message_user_info_bundle";
	// 报名用户的捆标记
	public static final String APPLY_USER_BUNDLE = "apply_user_bundle";
	// 与买家聊天标记
	public static final String CHAT_WITH_EMPLOYER_FLAG = "chat_with_employer_flag";
	// 用户的编号
	public static final String USER_ID = "userId";
	// 开机运营广告
	public static final String OPERATION_STARTUP_AD = "1";
	// 买家首次下单弹出图片
	public static final String OPERATION_BUYER_FIRST_MARK_ORDER_AD = "3";
	// 卖家首次下单弹出图片
	public static final String OPERATION_SELLER_FIRST_MARK_ORDER_AD = "2";
	// 需求编号
	public static final String REQUIRE_ID = "requireId";
	// 市场的编号
	public static final String MARKET_ID = "marketId";
	// 商户类型
	public static final String SHOP_PROPERTY = "shopProperty";
	// 银行名
	public static final String BANK_NAME = "bankName";
	// 银行卡号
	public static final String BANK_CARD_NO = "bankCardNo";
	// 银行卡号用户名
	public static final String BANK_CARD_USER_NAME = "bankCardAccountName";
	// 提现密码
	public static final String WITHDRAW_PASSWORD = "withdrawPw";
	// 提现金额
	public static final String WITHDRAW_AMOUNT = "withdraw_amount";
	// 被评价的卖家编号
	public static final String COMMENT_IDSTR = "beCommentIdStr";
	// 卖家编号
	public static final String NAICHA_IDSTR = "naichaIdStr";
	// 是否为来电拨号
	public static final String ISCOMINGCALL = "isComingCall";
	// 佣金数量
	public static final String REWARD_AMOUNT = "amount";
	// 朋友的编号
	public static final String FRIEND_ID = "userIdFriend";
	// 评价等级
	public static final String RANK = "rank";
	// 评价等级
	public static final String RATE = "rate";
	// 代发货的编号
	public static final String REPLACE_DELIVERY_ID = "deliverInsteadId";
	// 报名编号
	public static final String APPLY_ID = "applyId";
	// 选标标记
	public static final String CHOICE_TEACHER_FLAG = "choice_teacher_flag";
	// 间接点击买家标记
	public static final String INDIRECT_CLICK_BUYER_HOME_FLAG = "indirect_click_employer_flag";
	// 间接点击商品详情标记
	public static final String INDIRECT_CLICK_GOODS_DETAIL_FLAG = "indirect_click_goods_detail_flag";
	// 间接进入标志
	public static final String INDIRECT_COMING_FLAG = "indirect_coming_flag";
	// 选标的卖家编号
	public static final String CHOICE_TEACHER_ID = "choice_teacher_id";
	// 用户的生日
	public static final String BIRTHDAY = "birthday";
	// 用户性质
	public static final String USER_PROPERTY = "shopProperty";
	// 水平
	public static final String LEVEL = "rankStr";
	// 学校
	public static final String SCHOOL = "school";
	// 系统消息名
	public static final String SYSTEM_MESSAGE_NAME = "system";
	// 系统消息捆绑标记
	public static final String SYSTEM_MESSAGE_BUNDLE = "system_message_bundle";
	// 用户的地址
	public static final String ADDRESS = "address";
	// 首页轮播图的缓存
	public static final String HOME_PICTURE_CACHE = "home_picture_cache";
	// 找Ta女生的缓存
	public static final String FIND_SPEAKING_MAX_GIRL_CACHE = "find_he_girl_cache";
	// 找Ta男生的缓存
	public static final String FIND_SPEAKING_MAX_BOY_CACHE = "find_he_boy_cache";
	// 找达人全部的缓存
	public static final String FIND_SPEAKING_MAX_ALL_CACHE = "find_he_all_cache";
	// 找认证商家全部的缓存
	public static final String FIND_MARKET_ALL_CACHE = "find_market_all_cache";
	// 找Ta等级水平的缓存
	public static final String FIND_SPEAKING_MAX_LEVEL_CACHE = "find_he_level_cache";
	// 找买家需求的缓存
	public static final String FIND_BUYER_RELEASE_CACHE = "find_buyer_release_cache";
	// 联系人的缓存
	public static final String CONTACTS_CACHE = "contacts_cache";
	// 通讯录缓存
	public static final String ADDRESS_BOOK_CACHE = "address_book_cache";
	// 匹配通讯录缓存
	public static final String MATCH_ADDRESS_BOOK_CACHE = "match_address_book_cache";
	// 联系人客服的缓存
	public static final String CONTACTS_CUSTOM_SERVER_CACHE = "contacts_custom_server_cache";
	// 页面记号
	public static final String PAGENO = "pageNo";
	// 拉黑事件
	public static final String EVENT_ACTION_PULL_BLACKLIST = "event_action_pull_blacklist";
	// 设备唯一标识号
	public static final String DEVICEID = "deviceId";
	// 用户的当前的经纬度
	public static final String JINWEI = "jinwei";
	// 热门话题详情的路径
	public static final String PROMOTION_TOPIC_DETAIL_URL = "promotion_topic_detail_url";
	// 话题详情的捆标记
	public static final String TOPIC_DETAIL_BUNDLE = "topic_detail_bundle";
	// 任务邀约时传递卖家的id
	public static final String TEACHER_ID = "teacher_id";
	// 最后一次卖家过滤标记
	public static final String LAST_TEACHER_FILTER_FLAG = "last_teacher_filter_flag";
	// 最后一次任务过滤标记
	public static final String LAST_TASK_FILTER_FLAG = "last_task_filter_flag";
	// 最后一次伙伴过滤标记
	public static final String LAST_PARTNER_FILTER_FLAG = "last_partner_filter_flag";
	// 卖家
	public static final String TASK_REWARD_TEA_TIPS = "15元请TA喝杯卖家吧";
	// 咖啡
	public static final String TASK_REWARD_COFFEE_TIPS = "28元请TA喝杯咖啡吧";
	// 礼物
	public static final String TASK_REWARD_GIFT_TIPS = "元心意让他惊喜一下吧";
	// 查找全部任务
	public static final int TASK_FILTER_BY_ALL = 1;
	// 查找男生任务
	public static final int TASK_FILTER_BY_BOY = 2;
	// 查找女生任务
	public static final int TASK_FILTER_BY_GIRL = 3;
	// 查找附近任务
	public static final int TASK_FILTER_BY_NEARBY = 4;
	// 查找全部卖家
	public static final int TEACHER_FILTER_BY_ALL = 5;
	// 查找男生卖家
	public static final int TEACHER_FILTER_BY_BOY = 6;
	// 查找女生卖家
	public static final int TEACHER_FILTER_BY_GIRL = 7;
	// 按卖家认证等级查找
	public static final int TEACHER_FILTER_BY_RANK = 8;
	// 查找全部伙伴
	public static final int PARTNER_FILTER_BY_ALL = 9;
	// 查找男生伙伴
	public static final int PARTNER_FILTER_BY_BOY = 10;
	// 查找女生伙伴
	public static final int PARTNER_FILTER_BY_GIRL = 11;
	// 记录当前手机的DENSITY值大小
	public static final String DENSITY = "density";
	// 需求页面是否选中标记
	public static final String HOME_CURRENT_CHECK = "is_need_check";
	// 下线通知标记
	public static final String OFFLINE_INFORM_FLAG = "offline_inform_flag";
	// 客服的默认头像标记
	public static final String LOTUS_CUSTOM_SERICE_HEADPIC = "lotus_custom_serice_headpic";
	// 卖家小秘书的名称
	public static final String ORALHUB_SECRETARY_NAME = "卖家小秘书";
	// 莲花易城官方
	public static final String LOTUS_CUSTOM_SERICE = "官方客服";
	// 莲花易城官方Id编号
	public static final int LOTUS_CUSTOM_SERICE_USERID = 1;
	// 卖家小秘书的ID编号
	public static final int ORALHUB_SECRETARY_ID = -1;
	// 网络连接状态
	public static final String NETWORK_CONNECT_STATE = "network_connect_state";
	//TODO 服务器地址
//	public static final String BASE_URL = "http://114.55.57.158:8282/";
//	public static final String BASE_URL = "http://10.10.10.31:8080/";//田
	public static final String BASE_URL = "http://125.69.73.246:8680/";
//	public static final String BASE_URL = "http://10.10.10.123:8080/";//陈
	// 资源的基本路径服务器地址
//	public static final String RESOURCE_BASE_URL = "http://114.55.57.158:8282/lotus/resource/";
	public static final String RESOURCE_BASE_URL = BASE_URL + "lotus/resource/";
	// 服务器的基本路径
//	public static final String SERVER_BASE_URL = "http://114.55.57.158:8282/lotus/";
	public static final String SERVER_BASE_URL = BASE_URL + "lotus/";
	// 服务器的域名路径
//	public static final String SERVER_DOMAIN_URL = "http://www.ehehua.cn:8282/lotus/";
	public static final String SERVER_DOMAIN_URL = SERVER_BASE_URL;
	// 服务协议路径
//	public static final String USER_AGREEMENT_URL = "http://114.55.57.158:8282/lotus/privacy.htm";
	public static final String USER_AGREEMENT_URL = BASE_URL + "lotus/privacy.htm";
	//申请卖后付卖后付路径
	public static final String APPLY_PAY_SALED_URL = SERVER_BASE_URL+"ecity/after_sell_pay.html";
	//常见问题url
	public static final String COMMOM_REQUESTION_URL = SERVER_BASE_URL+"ecity/normal_problem.html";
	//关于赊购url
		public static final String ABOUT_URL = SERVER_BASE_URL+"ecity/about.html";
	// 访问成功的标记
	public static final String SUCCESS_CODE = "0001";
	// 消息编码
	public static final String MSG_CODE = "0000";
	// 运营广告活动已读
	public static final String ALREADY_READ_CODE = "0031";
	// 已经刷新过的标记
	public static final String ALREADY_REFRESH_CODE = "-2";
	// 已经存在的标记
	public static final String STATE_EXIST = "-3";
	// Token过期的标记
	public static final String TOKEN_EXPIRED = "0005";
	// 个人信息资料填写不足
	public static final String PERSONAL_INFOMATION_UN_SETING = "-6";
	// 卖家认证信息未填写(或未通过)
	public static final String TEACHER_AUTHENTICATE_UN_PASS = "-7";
	// 已经报名的标记
	public static final String ALREADY_APPLY = "-1";
	// 访问验证码错误的标记
	public static final String VALIDATE_CODE_ERROR = "-1";
	// 密码错误的标记
	public static final String PASSWORD_ERROR = "-1";
	// 每页显示的数据量
	public static final int PAGER_SIZE = 20;
	// 拍照的请求码
	public static final int TAKE_PHOTO_REQUEST_CODE = 10001;
	// 认证商家
	public static final int HOME_FIND_MARKET_TAG = 4;
	// 商品搜索
	public static final int HOME_GOODS_SEARCH_TAG = 0;
	// 首页找任务墙的标签编号
	public static final int HOME_FIND_BUYER_RELEASE_TAG = 5;
	// 消息的标签编号
	public static final int MESSAGE_TAG = 1;
	// 消息标记
	public static final String MESSAGE_FLAG = "message_flag";
	// 系统报名消息标记
	public static final String SYSTEM_APPLY_MESSAGE_FLAG = "apply";
	// 从系统消息点击进入标记
	public static final String SYSTEM_MESSAGE_CLICK_FLAG = "system_message_click_flag";
	// 系统评价消息标记
	public static final String SYSTEM_COMMENT_MESSAGE_FLAG = "comment";
	// 订单的标签编号
	public static final int ORDER_TAG = 2;
	// 我的标签编号
	public static final int ME_TAG = 3;
	// 位置的标记
	public static final String LOCATION_FLAG = "location_flag";
	// 已经登陆标记
	public static final String ALREADY_LOGIN_FLAG = "already_login_flag";
	// 个人信息变化标记
	public static final String USERINFO_CHANGE_FLAG = "userinfo_change_flag";
	// 联系人数据发生变化标记
	public static final String CONTACTS_DATA_CHANGE_FLAG = "contacts_data_change_flag";
	// 间接进入用户主页标记
	public static final String INDIRECT_CLICK_SELLER_HOME_FLAG = "coming_user_home_from_indirect_flag";
	// 任务状态发生变化标记
	public static final String TASK_STATUS_CHANGE_FLAG = "task_status_change_flag";
	// 话题的全部编号
	public static final int TOPIC_TITLE_ALL = 0;
	// 话题的生活编号
	public static final int TOPIC_TITLE_LIFE = 1;
	// 话题的商务编号
	public static final int TOPIC_TITLE_BUSINESS = 2;
	// 话题的旅游编号
	public static final int TOPIC_TITLE_TRAVEL = 3;
	// 话题的其他编号
	public static final int TOPIC_TITLE_OTHERS = 4;
	// 单聊
	public static final String CHAT_SINGLE = "chat_single";
	// 群聊
	public static final String CHAT_GROUP = "chat_group";

	// 单聊
	public static final int CHATTYPE_SINGLE = 1;
	// 群聊
	public static final int CHATTYPE_GROUP = 2;
	// 聊天室
	public static final int CHATTYPE_CHATROOM = 3;
	// 视屏电话聊天标记
	public static final String CHAT_VIDEO_CALL = "chat_video_call";
	// 语音电话聊天标记
	public static final String CHAT_VOICE_CALL = "chat_voice_call";
	// 聊天图片选择标记
	public static final String CHAT_PHOTO_SELECT_FLAG = "chat_photo_select_flag";
	// 代发货申请标记
	public static final String CHAT_REPLACE_DELIVERY_APPLY = "replaceDeliveryApply";
	// 选择商品
	public static final String CHAT_CHOOSE_GOODS = "chooseGoods";
	// 代发货退回标记
	public static final String CHAT_SYSTEM_RETURN_REPLACE_DELIVERY = "systemReturnReplaceDelivery";
	// 个人名片的标记
	public static final String CHAT_PERSONAL_BUSINESS_CARD_FLAG = "chat_personal_business_card_flag";
	// 代发货回执标记
	public static final String CHAT_REPLACE_DELIVERY_RECEIPT = "replaceDeliveryReceipt";
	// 商家名片
	public static final String CHAT_SELLER_CARDCASE = "sellerCardcase";
	// 自营企业标记
	public static final String SELF_COMPANY_FLAG = "self_company_flag";
	// 极光推送新需求的类型
	public static final String JPUSH_NEW_REQUIRE_TYPE = "1";
	// 极光推送订单付款成功给买家时的类型
	public static final String JPUSH_ORDER_PAYMENT_SUCCESS_TO_BUYER_TYPE = "2";
	// 极光推送已发货时的类型
	public static final String JPUSH_GOODS_ALREADY_DELIVERY_TYPE = "3";
	// 极光推送新的订单并未付款的类型
	public static final String JPUSH_NEW_ORDER_WITHOUT_PAYMENT_TYPE = "4";
	// 极光推送订单付款成功给卖家时的类型
	public static final String JPUSH_ORDER_PAYMENT_SUCCESS_TO_SELLER_TYPE = "5";
	// 极光推送买家已收货时的类型
	public static final String JPUSH_TAKE_DELIVERY_TYPE = "6";
	// 极光推送买家已评价时的类型
	public static final String JPUSH_COMMENT_TYPE = "7";
	// 极光推送运营广告的类型
	public static final String JPUSH_OPERATION_AD_TYPE = "12";
	// 极光推送商家完善个人资料的类型
	public static final String JPUSH_SELLER_COMPLETE_INFORMATION_TYPE = "21";
	// 极光推送买家完善个人资料的类型
	public static final String JPUSH_BUYER_COMPLETE_INFORMATION_TYPE = "24";
	// 极光推送商家完成认证的类型
	public static final String JPUSH_SELLER_COMPLETE_AUTHENTICATION_TYPE = "22";
	// 极光推送商家上传商品的类型
	public static final String JPUSH_SELLER_UPLOAD_GOODS_TYPE = "23";
	// 极光推送的消息标记
	public static final String JPUSH_MESSAGE = "jpush_message";
	// 环信帐号的默认密码
	public static final String HX_DEFAULT_PASSWORD = "123123123";
	// 聊天类型
	public static final String EXTRA_CHAT_TYPE = "chatType";
	// 聊天用户Id
	public static final String EXTRA_USER_ID = "userId";
	// 群聊Id
	public static final String EXTRA_GROUP_ID = "groupId";
	// 微信充值
	public static final String RECHARGE_BY_WECHAT = "recharge_by_wechat";
	// 微信年费缴纳
	public static final String PAYMENT_ANNUAL_BY_WECHAT = "payment_annual_by_wechat";
	// 微信支付
	public static final String PAYMENT_BY_WECHAT = "payment_by_wechat";
	// 微信支付回调
	public static final String CALLBACK_BY_WECHAT_FLAG = "callback_by_wechat_flag";
	// 支付宝支付
	public static final String PAYMENT_BY_ALIPAY = "payment_by_alipay";
	// 帐户余额支付
	public static final String PAYMENT_BY_ACCOUNT_BALANCE = "payment_by_account_balance";
	// 支付宝商户PID
	public static final String ALIPAY_PARTNER = "2088221421487775";
	// 支付充值类型
	public static final String PAYMENT_TYPE_RECHARGE = "recharge";
	// 支付担保类型
	public static final String PAYMENT_TYPE_GUARANTEE = "guarantee";
	// 支付年费类型
	public static final String PAYMENT_TYPE_ANNUAL = "annual";
	// 支付宝商户收款账号
	public static final String ALIPAY_SELLER = "ehehua@126.com";
	// 支付宝商户私钥，pkcs8格式
	public static final String ALIPAY_RSA_PRIVATE = "MIICdwIBADANBgkqhkiG9w0BAQEFAASCAmEwggJdAgEAAoGBAKVgXpPowNWOA+x+SrxI0YkSSy//lGb4MbxQnGlmrFU4jGfmpERujJeZlQ2cSngwomODhK81vneNoTeTtnpQAymt6jdBY32ADZzBm2sQk+NNI4nHAE3rlv2nNxV78q+4X0WD8CrcHjGEOs6FxCngkFF0IMpawVwnconZ/8sfmx1xAgMBAAECgYB4imLFmrybrDUN9wvQa59XMt9c8ASifTv7UCKB7RWl1F77CLEpNA8EHSz4y7e4HXLv6BgZgaoACFq/YMp8Ir/6P7OMpDzlcwNUAqnBE8r6D3y4xS4hupffZNJuksFACwvVOXNcaWiqeIYmC/aoLqD3Owq3yx0Th3SqtW7sWJPuRQJBANugD2lgZrkzE2yDI4Y5K/WRI/iXJldHF1aFm3BJhjRNxzhwcwgtmREcrPq4xn1G+jwr/UEVC5lK9m1woVr/5s8CQQDAxDPWsjoRLNjcojrlNRGxxb9Xeesego3eryd0gJV+y623XpwyjgPbMaVZpqmnt2K9m5P3smuuKeEhfZd+Tce/AkEAjFp7vuIGg7BqZ1xfJXX/6803abReokQiW/7CWT4DcAWMnTrvqHhLa64htZDD+GeVn0KVdDQY13wag9IIq/kB7QJABac7ZfaAIXTRP1c6pvIxfKcx/1zB+nkVtAwq+g2zVK6gf5o0tn7Xuhvwn3p1y0dji+/+HOuMoOT1h0fpo3igfQJBAIHARsVBMq7eNh1fRDu87ELgPEH5hEODUITn2wGbaAoeetTP3hv5GDbIKBNZuZDipPiolOQLHrV6l0gKFuF3r3c=";
	// 支付宝回调通知
//	public static final String ALIPAY_NOTIFY_URL = "http://114.55.57.158:8282/lotus/payment/receiveFromAlipay.do";
	public static final String ALIPAY_NOTIFY_URL = BASE_URL + "lotus/payment/receiveFromAlipay.do";
	// 微信回调通知
//	public static final String WECHAT_NOTIFY_URL = "http://114.55.57.158:8282/lotus/payment/unifiedorder2.do";
	public static final String WECHAT_NOTIFY_URL = BASE_URL + "lotus/payment/unifiedorder2.do";
	// 请同时修改 androidmanifest.xml里面,相应的属性<data
	// android:scheme="wxb4ba3c02aa476ea1"/>为新设置的appid
	// 微信支付的应用编号
	public static final String WECHAT_APP_ID = "wx7e92ae2741484956";
	// 微信支付的商户号
	public static final String WECHAT_MCH_ID = "1329133301";
	// 微信支付的API密钥，在商户平台设置
	public static final String WECHAT_API_KEY = "herunxinxiehehuaherunxinxiehehua";
	public static final String IS_PAY_SALED = "is_pay_saled";
	//是否支持卖后付的接口
	public static final String PAY_TYPE = "payType";
	public static final String SEARCH_TYPE = "search_type";
	public static final String MARKET_NAME = "market_name";
	//订单中商品的属性和对应的数量
	public static final String GOODS_ATTRIBUTE_AND_NUM = "goods_attribute_and_num";
	//直接付款
	public static final String PAY_DIRECTLY = "pay_directly";
	//用户登录的id
	public static final String LOGINID = "loginId";
	public static final String BUY_ID = "buyId";
	//选择省份的请求码和返回码
	public static final int REQUEST_CODE_PROVINCE = 101;
	public static final int REASULT_CODE_PROVINCE = 201;
	//地址 请求码返回码
	public static final int REQUEST_ADDRESS = 102;
	public static final int REASULT_ADDRESS = 202;
	public static final String PROVINCE = "province";
	public static final String CODE = "code";
	public static final String RECEIVEINGINFOID = "receiveingInfoId";
	public static final String ORDERVO = "orderVo";
	public static final String FROM_ORDER_CONFIRM_POP = "from_order_confirm_pop";
	public static final String ORDER_DETAIL_BEAN = "order_detail_bean";
	public static final String STORENAME = "storeName";
	public static final String USERIDBUYER = "userIdBuyer";
	public static final String USERIDSELLER = "userIdSeller";
	public static final String ITEMID = "itemId";
	public static final String AMOUNT = "amount";
	public static final String PIC = "pic";
	public static final String TRADENAME = "tradeName";
	public static final String PAYTYPE = "payType";
	public static final String ATTRIBUTE = "attribute";
	public static final String PRICE = "price";
	public static final String TOTALPRICE = "totalPrice";
	public static final String ID = "id";
	public static final String SHOPPING_CART_IDS = "shopping_cart_ids";
	public static final String ORDER_DETAIL_BEANS = "order_detail_beans";
	public static final String RECEIVER = "receiver";
	public static final String PHONE = "phone";
	public static final String LEAVEWORD = "leaveWord";
	public static final String FROM_ORDER_DETAIL = "from_order_detail";
	/**  订单状态   待付款  */
	public static final int ORDER_UNPAY = 0;
	/**  订单状态   待发货  */
	public static final int ORDER_WAIT_FOR_PAY = 1;
	/**  订单状态   已发货 */
	public static final int ORDER_ALREADY_SEND = 2;
	/**  订单状态   已收货  */
	public static final int ORDER_ALREADY_GET = 3;
	/**  订单状态   未评论  */
	public static final int ORDER_UNCOMMENT = 4;
	/**  订单状态   已评论  */
	public static final int ORDER_ALREADY_COMMENT = 5;
	public static final String STATUSCODE = "statusCode";
	public static final String ORDERID = "orderId";
	public static final String FROM_ORDER_MANAGER = "from_order_manager";
	public static final String FROM_TYPE = "from_type";
	public static final String TYPE = "type";
	public static final String EVALUATION = "evaluation";
	public static final String NEW_LEVEL = "level";
	public static final String MY_BILL = "my_bill";
	public static final String APPLICATIONAMOUNT = "applicationAmount";
	public static final String PAYTYPEPRICE = "payTypePrice";
	public static final String PAYTIME = "payTime";
	public static final String FAVORABLEPRICE = "favorablePrice";
	public static final String DREDGE_PAY_SALED_DISMISS = "费用缴纳后，工作人员会线下联系您，审核通过后开启卖后付;若没有通过审核，我们将退回您的保证金";
	public static final String DREDGE_PAY_SALED_SHOW = "费用缴纳后,您将获得为期一年的卖后付使用权利,一年后将自动续约";
	/**  开通卖后付价钱*/
	public static final float MONEY_FOR_PAY_SALED = 2000;
	public static final String OLDPAS = "oldPas";
	public static final String NEWPAS = "newPas";
	public static final String HAS_DREDGE_PAY_SALED = "has_dredge_pay_saled";
	public static final String IS_UPLOAD = "is_upload";
	public static final String HOTLINE = "028-83395779";
	public static final String LOGIN_USERID = "loginUserId";

}
