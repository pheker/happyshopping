package cn.pheker.happyshopping.utils;

import java.math.BigDecimal;
import java.text.DecimalFormat;

public class DecimalUtils {
	
	/**
	 * float 保留小数点后几位
	 * @param f
	 * @param scale
	 * @return
	 */
	public static float formatFloat(float f , int scale){
		BigDecimal bigDecimal =new BigDecimal(f);
		//四舍五入
		float floatValue = bigDecimal.setScale(scale, BigDecimal.ROUND_HALF_UP).floatValue();
		return floatValue;
		
	}
	
	/**
	 * float 保留小数点后几位
	 * @param f
	 * @param scale
	 * @return
	 */
	public static float formatFloat(String f , int scale){
		BigDecimal bigDecimal =new BigDecimal(f);
		//四舍五入
		float floatValue = bigDecimal.setScale(scale, BigDecimal.ROUND_HALF_UP).floatValue();
		return floatValue;
		
	}
	
	/**
	 * double 保留小数点后几位
	 * @param d
	 * @param scale
	 * @return
	 */
	public static double formatDouble(double d , int scale){
		BigDecimal bigDecimal =new BigDecimal(d);
		//四舍五入
		double doubleValue = bigDecimal.setScale(scale, BigDecimal.ROUND_HALF_UP).doubleValue();
		return doubleValue;
		
	}
}
