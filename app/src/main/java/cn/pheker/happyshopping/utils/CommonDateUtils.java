package cn.pheker.happyshopping.utils;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * @author: Biao
 * @创建时间: 2015-8-27 下午4:22:29
 * @描述信息: 通用的日期工具类封装
 * @svn提交者: $Author$
 * @提交时间: $Date: 2015-11-21 14:56:11 +0800 (Sat, 21 Nov 2015) $
 * @当前版本: $Rev: 39 $
 */
public class CommonDateUtils {
	@SuppressLint("SimpleDateFormat")
	/**
	 * 根据指定的日期获取当前是周几
	 * @param pTime 指定的日期
	 * @return
	 */
	public static String getDayOfWeek(String pTime) {
		String dayOfWeek = "周";
		SimpleDateFormat format = new SimpleDateFormat("yyyy年MM月dd日");
		Calendar c = Calendar.getInstance();
		try {
			c.setTime(format.parse(pTime));
			int day = c.get(Calendar.DAY_OF_WEEK);
			switch (day) {
			case 1:
				dayOfWeek += "日";
				break;
			case 2:
				dayOfWeek += "一";
				break;
			case 3:
				dayOfWeek += "二";
				break;
			case 4:
				dayOfWeek += "三";
				break;
			case 5:
				dayOfWeek += "四";
				break;
			case 6:
				dayOfWeek += "五";
				break;
			case 7:
				dayOfWeek += "六";
				break;
			}

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return dayOfWeek;
	}

	/**
	 * 根据指定的年月返回此月共有多少天
	 * 
	 * @param year
	 * @param month
	 * @return
	 */
	public static int getDayOfMonth(int year, int month) {
		int day = 30;
		// 闰年标记
		boolean flag = false;
		// 判断是否为闰年
		if (year % 4 == 0 && year % 100 != 0 || year % 400 == 0) {
			flag = true;
		} else {
			flag = false;
		}
		// 根据月份返回相应的天数
		switch (month) {
		case 1:
		case 3:
		case 5:
		case 7:
		case 8:
		case 10:
		case 12:
			day = 31;
			break;
		case 2:
			day = flag ? 29 : 28;
			break;
		default:
			day = 30;
			break;
		}
		return day;
	}
}
