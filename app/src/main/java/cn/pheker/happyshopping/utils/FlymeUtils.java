package cn.pheker.happyshopping.utils;

import java.lang.reflect.Method;

import android.os.Build;

/**
 * 
 * @author: Biao
 * @创建时间: 2015-12-30 下午5:59:41
 * @描述信息: 检测判断是否为魅族系统
 * @svn提交者: $Author$
 * @提交时间: $Date$
 * @当前版本: $Rev$
 */
public class FlymeUtils {

	/**
	 * 根据是否有smartbar来判断是否为魅族系统
	 * 
	 * @return
	 */
	public static boolean isFlyme() {
		try {
			// Invoke Build.hasSmartBar()
			final Method method = Build.class.getMethod("hasSmartBar");
			return method != null;
		} catch (final Exception e) {
			return false;
		}
	}
}
