package cn.pheker.happyshopping.utils;

import java.io.ByteArrayOutputStream;
import java.io.InputStream;

/**
 * @author: Biao
 * @创建时间: 2015-9-16 下午3:52:39
 * @描述信息: 流的工具类封装
 * @svn提交者: $Author$
 * @提交时间: $Date: 2015-11-21 14:56:11 +0800 (Sat, 21 Nov 2015) $
 * @当前版本: $Rev: 39 $
 */
public class StreamUtils {

	/**
	 * 将输入流转化为字符串
	 * 
	 * @param is
	 * @param codingFormat
	 *            编码格式
	 * @return
	 */
	public static String decodeStream(InputStream is, String codingFormat) {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		byte[] bys = new byte[1024];
		int len = -1;
		try {
			// 读取数据
			while ((len = is.read(bys)) != -1) {
				// 写入数据
				baos.write(bys, 0, len);
			}
			// 关闭资源
			IOUtils.close(is);
			// 返回对应的编码数据
			return new String(baos.toByteArray(), codingFormat);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}

}
