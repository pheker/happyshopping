package cn.pheker.happyshopping.utils;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.annotation.SuppressLint;
import android.view.View;
import android.view.animation.DecelerateInterpolator;

/**
 * @author: Biao
 * @创建时间: 2015-9-1 上午10:29:11
 * @描述信息: 缩放图片的工具类
 * @svn提交者: $Author$
 * @提交时间: $Date: 2015-11-21 14:56:11 +0800 (Sat, 21 Nov 2015) $
 * @当前版本: $Rev: 39 $
 */
@SuppressLint("NewApi")
public class ZoomImageUtil {

	public static final int DURATION = 300;

	/**
	 * 开始缩放动画
	 * 
	 * @param destView
	 */
	public static void zoomImageFromThumb(View destView) {

		AnimatorSet set = new AnimatorSet();
		set.play(ObjectAnimator.ofFloat(destView, "scaleX", 0, 1f)).with(
				ObjectAnimator.ofFloat(destView, "scaleY", 0, 1f));
		set.setDuration(DURATION);
		set.setInterpolator(new DecelerateInterpolator());
		set.start();

	}

	/**
	 * 关闭动画从左移动右边
	 * 
	 * @param destView
	 * @param animatorListener
	 */
	public static void closeZoomAnim(View destView,
			Animator.AnimatorListener animatorListener) {

		ObjectAnimator translationAnim = ObjectAnimator.ofFloat(destView,
				"translationX", 0f, destView.getWidth());
		translationAnim.setDuration(200);
		translationAnim.setInterpolator(new DecelerateInterpolator());
		translationAnim.addListener(animatorListener);
		translationAnim.start();
	}
}
