package cn.pheker.happyshopping.utils;

import android.annotation.SuppressLint;
import android.media.AudioManager;
import android.media.SoundPool;

/**
 * 
 * @author: Biao
 * @创建时间: 2015-11-21 下午5:32:50
 * @描述信息: 声音池工具类封装
 * @svn提交者: $Author$
 * @提交时间: $Date$
 * @当前版本: $Rev$
 */
public class SoundPoolUtils {

	private static SoundPoolUtils instance = null;

	/**
	 * 获取实例
	 * 
	 * @return
	 */
	public static synchronized SoundPoolUtils getInstance() {
		if (instance == null) {
			synchronized (SoundPoolUtils.class) {
				if (instance == null) {
					instance = new SoundPoolUtils();
				}
			}
		}
		return instance;

	}

	/**
	 * 播放声音
	 * 
	 * @param audioManager
	 *            音频管理者
	 * @param soundPool
	 *            声音池对象
	 * @param soundId
	 *            声音的资源编号
	 * @return
	 */
	public int playSound(AudioManager audioManager, SoundPool soundPool,
			int soundId) {
		try {
			// // 最大音量
			// float audioMaxVolume = audioManager
			// .getStreamMaxVolume(AudioManager.STREAM_RING);
			// // 当前音量
			// float audioCurrentVolume = audioManager
			// .getStreamVolume(AudioManager.STREAM_RING);
			// // 计算音量比率
			// float volumeRatio = audioCurrentVolume / audioMaxVolume;

			audioManager.setMode(AudioManager.MODE_RINGTONE);
			// 关闭扬声器
			audioManager.setSpeakerphoneOn(false);
			// 播放声音(声音资源,左声道,右声道,优先级0最低,循环次数0是不循环-1是永远循环,回放速度0.5-2.0之间1为正常速度)
			int id = soundPool.play(soundId, 0.3f, 0.3f, 1, -1, 1);
			return id;
		} catch (Exception e) {
			return -1;
		}
	}

	/**
	 * 打开扬声器
	 * 
	 * @param audioManager
	 */
	@SuppressLint("InlinedApi")
	public void openSpeakerOn(AudioManager audioManager) {
		// 判断扬声器是否已经打开
		if (!audioManager.isSpeakerphoneOn()) {
			audioManager.setSpeakerphoneOn(true);
		}
		// 设置为通信模式
		audioManager.setMode(AudioManager.MODE_IN_COMMUNICATION);
	}

}
