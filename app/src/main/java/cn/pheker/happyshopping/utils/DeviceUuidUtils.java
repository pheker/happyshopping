package cn.pheker.happyshopping.utils;

import android.content.Context;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;
import android.text.TextUtils;

import java.io.UnsupportedEncodingException;
import java.util.UUID;

/**
 * 
 * @author: Biao
 * @创建时间: 2016-5-17 下午4:18:35
 * @描述信息: 设备唯一uuid工具类封装
 * @svn提交者: $Author$
 * @提交时间: $Date$
 * @当前版本: $Rev$
 */
public class DeviceUuidUtils {
	protected static final String PREFS_DEVICE_ID = "device_id";

	protected static UUID uuid;

	private static DeviceUuidUtils mInstance;

	/**
	 * 获取实例
	 * 
	 * @return
	 */
	public static synchronized DeviceUuidUtils getInstance(Context context) {
		if (mInstance == null) {
			synchronized (DeviceUuidUtils.class) {
				if (mInstance == null) {
					mInstance = new DeviceUuidUtils(context);
				}
			}
		}
		return mInstance;

	}

	public DeviceUuidUtils(Context context) {

		if (uuid == null) {
			synchronized (DeviceUuidUtils.class) {
				if (uuid == null) {
					final String id = SharedPreferencesUtils.getString(context,
							PREFS_DEVICE_ID);

					if (!TextUtils.isEmpty(id)) {
						// Use the ids previously computed and stored in the
						// prefs file
						uuid = UUID.fromString(id);

					} else {

						final String androidId = Secure
								.getString(context.getContentResolver(),
										Secure.ANDROID_ID);

						// Use the Android ID unless it's broken, in which case
						// fallback on deviceId,
						// unless it's not available, then fallback on a random
						// number which we store
						// to a prefs file
						try {
							if (!"9774d56d682e549c".equals(androidId)) {
								uuid = UUID.nameUUIDFromBytes(androidId
										.getBytes("utf8"));
							} else {
								final String deviceId = ((TelephonyManager) context
										.getSystemService(Context.TELEPHONY_SERVICE))
										.getDeviceId();
								uuid = deviceId != null ? UUID
										.nameUUIDFromBytes(deviceId
												.getBytes("utf8")) : UUID
										.randomUUID();
							}
						} catch (UnsupportedEncodingException e) {
							throw new RuntimeException(e);
						}

						SharedPreferencesUtils.putString(context,
								PREFS_DEVICE_ID, uuid.toString());
					}

				}
			}
		}

	}

	/**
	 * 获取uuid值
	 * 
	 * @return
	 */
	public String getDeviceUuid() {
		return uuid.toString();
	}
}
