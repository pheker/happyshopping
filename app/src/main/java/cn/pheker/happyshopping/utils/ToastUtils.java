package cn.pheker.happyshopping.utils;


import android.app.Activity;
import android.view.Gravity;
import android.widget.Toast;

/**
 * 
 * @author: Biao
 * @创建时间: 2015-8-18 下午12:29:49
 * @描述信息: 吐司工具类,可以任何线程中中弹出吐司
 * @svn提交者: $Author$
 * @提交时间: $Date: 2015-11-21 14:56:11 +0800 (Sat, 21 Nov 2015) $
 * @当前版本: $Rev: 39 $
 */
public class ToastUtils {
	/**
	 * 弹土司
	 * 
	 * @param context
	 * @param msg
	 */
	public static void show(final Activity context, final String msg) {
		// 判断当前线程是为主线程
		if (Thread.currentThread().getName().equals("main")) {

			Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
		} else {
			// 不是主线程,调用activity的runonuithread方法
			context.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
				}
			});
		}
	}

	/**
	 * 居中弹土司
	 * 
	 * @param context
	 * @param msg
	 */
	public static void showInCenter(final Activity context, final String msg) {

		// 判断当前线程是为主线程
		if (Thread.currentThread().getName().equals("main")) {

			Toast toast = Toast.makeText(context, msg, Toast.LENGTH_SHORT);
			toast.setGravity(Gravity.CENTER, 0, 0);
			toast.show();
		} else {
			// 不是主线程,调用activity的runonuithread方法
			context.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					Toast toast = Toast.makeText(context, msg,
							Toast.LENGTH_SHORT);
					toast.setGravity(Gravity.CENTER, 0, 0);
					toast.show();
				}
			});
		}
	}

}
