package cn.pheker.happyshopping.utils;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
	public static Date getDateTimeFromStr(String strDate,String format){
		if(strDate==null || strDate.equals("")) return null;
		if( format == null || format.equals("")) 
			format = "yyyy-MM-dd HH:mm:ss";
		
		SimpleDateFormat sformat= new SimpleDateFormat(format);
		
		Date retDate  =  sformat.parse(strDate,new ParsePosition(0));
		return retDate;
	}
	/*
	 * 根据字符串和转换格式，将字符串转换为日期
	 * @param strDate 待转换字符串日期
	 * @param format  待转换的格式，如：HH:mm
	 */
	public static Date getDateFromStr(String strDate,String format){
		if(strDate==null || strDate.equals("")) return null;
		if( format == null || format.equals("")) 
			format = "yyyy-MM-dd";
		
		SimpleDateFormat sformat= new SimpleDateFormat(format);
		
		Date retDate  =  sformat.parse(strDate,new ParsePosition(0));
		return retDate;
	}
	
	public static Date getDateFromStr(String strDate)
	{
		return getDateFromStr(strDate,null);
	}
	
	/*
	 * 获取日期部分
	 */
	public static Date getTruncDateFromDate(Date date)
	{
		if(date == null) return null;
		String temp = getStrFromDate(date,"yyyy-MM-dd");
		return getDateFromStr(temp,"yyyy-MM-dd");
	}
	
	/*
	 * 根据日期和转换格式，将日期转换为字符串
	 * @param date    待转换日期
	 * @param format  待转换的格式，如：HH:mm
	 */
	public static String getStrFromDate(Date date,String format){
		if(date == null) return "";
		if(format == null || format.equals("")) 
			format = "yyyy-MM-dd HH:mm:ss";
		SimpleDateFormat sformat = new SimpleDateFormat(format);
		return sformat.format(date);
	}
	/*
	 * 根据日期和待加减的秒，计算相加减后的日期
	 * @param date    待转换日期
	 * @param day     相加减的秒
	 */
	public static Date getDateByAddSecond(Date date,int second){
		Calendar calendar=Calendar.getInstance();   
	    calendar.setTime(date); 
	    calendar.set(Calendar.SECOND,calendar.get(Calendar.SECOND)+second);//让日期加1 
	    return calendar.getTime();//(Calendar.DATE);
//	    start = new Date(start.getTime()+ rm.startBaseday*24*60*60*1000);
	}
	public static String getStrFromDate(Date date)
	{
		return getStrFromDate(date,null);
	}
	
	/*
	 * 根据日期和待加减的天数，计算相加减后的日期
	 * @param date    待转换日期
	 * @param day     相加减的天数
	 */
	public static Date getDateByAddDay(Date date,int day){
		if(date==null) return null;
		Calendar calendar=Calendar.getInstance();   
	    calendar.setTime(date); 
	    calendar.set(Calendar.DAY_OF_MONTH,calendar.get(Calendar.DAY_OF_MONTH)+day);//让日期加1 
	    return calendar.getTime();//(Calendar.DATE);
//	    start = new Date(start.getTime()+ rm.startBaseday*24*60*60*1000);
	}
	
	/*
	 * 根据日期和待加减的小时，计算相加减后的日期
	 * @param date    待转换日期
	 * @param day     相加减的小时
	 */
	public static Date getDateByAddHour(Date date,int hour){
		Calendar calendar=Calendar.getInstance();   
	    calendar.setTime(date); 
	    calendar.set(Calendar.HOUR,calendar.get(Calendar.HOUR)+hour);//让日期加1 
	    return calendar.getTime();//(Calendar.DATE);
//	    start = new Date(start.getTime()+ rm.startBaseday*24*60*60*1000);
	}
	
	/*
	 * 根据日期和待加减的分钟，计算相加减后的日期
	 * @param date    待转换日期
	 * @param day     相加减的分钟
	 */
	public static Date getDateByAddMinu(Date date,int minute){
		Calendar calendar=Calendar.getInstance();   
	    calendar.setTime(date); 
	    calendar.set(Calendar.MINUTE,calendar.get(Calendar.MINUTE)+minute);//让日期加1 
	    return calendar.getTime();//(Calendar.DATE);
//	    start = new Date(start.getTime()+ rm.startBaseday*24*60*60*1000);
	}
	
	/*
	 * 得到两个日期相差的秒数
	 * @param startDate    日期1
	 * @param endDate      日期2
	 */
	public static int secondsBetween(Date startDate,Date endDate){
//		int sec = startDate.getSeconds() - endDate.getSeconds();
//		if (sec <= 0 && sec > -1)
//            sec = 1;
		if(startDate==null)
			return -1;
		if(endDate==null)
			return -1;
		int sec = (int)(startDate.getTime() - endDate.getTime())/1000;
        return Math.abs((int)sec);
	}
	/*
	 * 得到两个日期相差的秒数
	 * @param startDate    日期1
	 * @param endDate      日期2
	 */
	public static int secondsBetween(String startDateStr,String endDateStr){
		Date startDate = getDateFromStr(startDateStr,null);
		Date endDate = getDateFromStr(endDateStr,null);
		if(startDate==null)
			return -1;
		if(endDate==null)
			return -1;
		int sec = (int)(startDate.getTime() - endDate.getTime())/1000;
		return sec;
	}
	
	/*
	 * 得到两个日期相差的分钟数
	 * @param startDate    日期1
	 * @param endDate      日期2
	 */
	public static int minutsBetween(Date startDate,Date endDate){
//		int sec = startDate.getMinutes() - endDate.getMinutes();
//        return Math.abs((int)sec);
		if(startDate==null || endDate == null ){
			return -1;
		} 
		long sec = (startDate.getTime() - endDate.getTime())/(1000*60);
        return Math.abs((int)sec);
	}

	/*
	 * 得到系统默认Date最大日期，java中不存在，先默认一个
	 */
	public static Date maxDate(){
		try {
			return new SimpleDateFormat("yyyy-MM-dd").parse("9999-12-31");
		} catch (ParseException e) {
			return null;
		}
	}
	
	public static String getNowString(){
		return getStrFromDate(new Date(),null);
	}
	
	/**
	 * 获取当前日期(不带时分秒，只显示日期)
	 * @author:zn
	 * @version:2014-9-12
	 * @return
	 */
	public static String getNowDateString(){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd");  
		Date date=new Date();
		return sdf.format(date); 
	}
	/**
	 * 获取当前日期
	 * @author:zn
	 * @version:2014-9-21
	 * @return
	 */
	public static String getNowTimeString(){
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		Date date=new Date();
		return sdf.format(date); 
	}
	
	/**
	 * 把字符串转为日期
	 * @author:zn
	 * @version:2014-9-21
	 * @param strDate
	 * @return
	 * @throws Exception
	 */
	public static Date ConverToDate(String strDate) throws Exception{
		DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		return df.parse(strDate);
	}
	   /** 
	     * 得到两个日期相差的时间
	     */  
	    public static String getBetweenDay(Date date1, Date date2) {  
	        Calendar d1 = new GregorianCalendar();  
	        d1.setTime(date1);  
	        Calendar d2 = new GregorianCalendar();  
	        d2.setTime(date2);  
	        long  minute=(date2.getTime()-date1.getTime())/(1000*60);
	        if(minute<1){
	        	return "刚刚";
	        }
	        else if (minute<2){
	        	return "1分钟前";
	        }
	        
            else if (minute<45){
            	return minute+"分钟前";
	         }
            else if (minute<90){
            	return "1小时前";
	         }
	        /*相差小时*/
            else if((minute/60)<24){
	            return (minute/60)+"小时前";
	        }
            else if((minute/60)<48){
            	return "昨天";
            }
	        /*天数*/
            else if ((minute/60/24) <30){
            	return (minute/60/24)+"天前";
            }
	         /*月*/
            else if((minute/60/24/30)<12){
            	return (minute/60/24/30)+"个月前";       	
            }
	        return (minute/60/24/30/12)<=1?"1年前":(minute/60/24/30/12)+"年前";    
	        /* */
//	       /*相差天数*/
//	        int days = d2.get(Calendar.DAY_OF_YEAR)- d1.get(Calendar.DAY_OF_YEAR);  
//	        System.out.println("days="+days);  
//            int y2 = d2.get(Calendar.YEAR);  
//	      if (d1.get(Calendar.YEAR) != y2) {  
//          do {  
//	          days += d1.getActualMaximum(Calendar.DAY_OF_YEAR);  
//              d1.add(Calendar.YEAR, 1);  
//             } while (d1.get(Calendar.YEAR) != y2);  
//         }
//	      
//	      if(days==0){
//	    	  result="今天";
//	      }
//	      else if(days>0){
//	    	  switch (days) {
//			case 1:
//				result="昨天";
//				break;
//            case 2:
//            	result="前天";
//				break;
//			default:
//				result=days+"天前";
//				break;
//			}
//	      }
//	      else if(days<0){
//	    	  switch (days) {
//			case -1:
//				result="明天";
//				break;
//            case -2:
//            	result="后天";
//				break;
//			default:
//				result=days+"天后";
//				result=result.substring(1);
//				break;
//			}
//	      }
	      
	       
	   }  
	    /**
	     * 获取现在时间
	     * 
	     * @return 返回时间类型 yyyy-MM-dd HH:mm:ss
	     */
	    public static Date getNowDate() {
	     Date currentTime = new Date();
	     SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	     String dateString = formatter.format(currentTime);
	     ParsePosition pos = new ParsePosition(0);
	     Date currentTime_2 = formatter.parse(dateString, pos);
	     return currentTime_2;
	    }

	/**
	 * QQ日期显示格式
	 * @param strData
	 * @return
     */
	public static String showAsQQFormat(String strData) {
		Date date = new Date();
		// 转换为标准时间
		SimpleDateFormat myFormatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
		Date issueDate = null;
		try {
			issueDate = myFormatter.parse(strData);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		long currTime = date.getTime();
		long issueTime = issueDate.getTime();
		long diff = currTime - issueTime;
		diff = diff / 1000;//秒
		if (diff / 60 < 1) {
			return "刚刚";
		}
		if (diff / 60 >= 1 && diff / 60 <= 60) {
			return diff / 60 + "分钟前";
		}
		if (diff / 3600 > 0 && diff / 3600 <= 24) {
			return diff / 3600 + "小时前";
		}
		if (diff / (3600 * 24) > 0 && diff / (3600 * 24) < 2) {
			return "昨天";
		}
		if (diff / (3600 * 24) > 1 && diff / (3600 * 24) < 3) {
			return "前天";
		}
		if (diff / (3600 * 24) > 2) {
			return formatter.format(issueDate);
		}
		return "";
	}



}
