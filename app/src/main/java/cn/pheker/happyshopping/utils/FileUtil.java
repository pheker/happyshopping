package cn.pheker.happyshopping.utils;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import android.R.bool;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;

/**
 * @author: zn E-mail:zhangn@jtv.com.cn
 * @version:2015-2-26 类说明
 */

public class FileUtil {
	public static FileUtil fileUtil = null;
	public static String SYS_ROOT = null;

	public static FileUtil getInstance() {
		if (fileUtil == null) {
			fileUtil = new FileUtil();
		}
		return fileUtil;
	}

	/**
	 * 通过保存、下载数据的目录
	 * 
	 * @author:zn
	 * @version:2015-3-2
	 * @param context
	 * @return
	 */
	public static String saveRootPath(Context context) {
		/************* 判断文件SDcard是否存在并且是否可以读写 ********/
		if (Environment.getExternalStorageState().equals(
				Environment.MEDIA_MOUNTED)) {
			/* 存在sdcard并且具有读写权限 根目录为sdcard根目录 */
			SYS_ROOT = Environment.getExternalStorageDirectory()
					.getAbsolutePath();
		} else {
			/* 不存在sdcard或者sdcard不具有读写权限 根目录为android系统根目录 */
			SYS_ROOT = Environment.getRootDirectory().getAbsolutePath();
		}
		/* 默认根目录为app包名目录 */
		String packageName = context.getApplicationInfo().packageName;
		return SYS_ROOT + "/" + packageName;
	}

	public void createDir(String filePath) {
		File dir = new File(filePath);
		if (!dir.exists())
			dir.mkdirs();
	}

	/**
	 * 判断当前文件是否存在 存在:true;不存在:false
	 * 
	 * @author:zn
	 * @version:2015-2-26
	 * @param filePath
	 * @return
	 */
	public boolean isFileExist(String filePath) {
		File file = new File(filePath);
		return file.exists();
	}

	/**
	 * 删除文件
	 * 
	 * @author:zn
	 * @version:2015-2-26
	 * @param filePath
	 *            (文件路径)
	 * @return
	 */
	public boolean deleteFile(String filePath) {
		File delFile = new File(filePath);
		if (delFile.exists()) {
			delFile.delete();
			return true;
		} else {
			return false;
		}
	}

	/**
	 * 递归删除指定目录
	 * 
	 * @author:zn
	 * @version:2015-3-10
	 * @param path
	 *            (目录的路径)
	 */
	public void recursionDeleteFile(String path) {
		if (StringUtil.isEmpty(path))
			return;
		File file = new File(path);
		recursionDeleteFile(file);
	}

	/**
	 * 递归删除指定目录
	 * 
	 * @author:zn
	 * @version:2015-2-28
	 * @param file
	 *            (目录)
	 */
	public void recursionDeleteFile(File file) {
		if (file.isFile()) {
			file.delete();
			return;
		}
		if (file.isDirectory()) {
			File[] childFile = file.listFiles();
			if (childFile == null || childFile.length == 0) {
				// file.delete();
				return;
			}
			for (File f : childFile) {
				recursionDeleteFile(f);
			}
			// file.delete();
		}
	}

	/**
	 * 获取根目录下的所有图片，然后根据名称获取图片的路径
	 * 
	 * @author:zn
	 * @version:2015-2-12
	 * @param imgRootPath
	 *            (保存图片的路径)
	 * @param photoName
	 *            (以这个名称开头的图片名称)
	 * @return 返回过滤完成的图片路径
	 */
	public List<String> getImgPathByRootPath(final String imgRootPath) {
		List<String> list = new ArrayList<String>();
		File file = new File(imgRootPath);
		File[] allfiles = file.listFiles();
		if (allfiles == null) {
			return null;
		}
		for (int k = 0; k < allfiles.length; k++) {
			final File fi = allfiles[k];
			if (fi.isFile()) {
				int idx = fi.getPath().lastIndexOf(".");
				if (idx <= 0) {
					continue;
				}
				String suffix = fi.getPath().substring(idx);
				boolean isImgFlag = suffix.toLowerCase().equals(".jpg")
						|| suffix.toLowerCase().equals(".jpeg")
						|| suffix.toLowerCase().equals(".bmp")
						|| suffix.toLowerCase().equals(".png")
						|| suffix.toLowerCase().equals(".gif");
				// if (isImgFlag && fi.getPath().contains(photoName)) {
				if (isImgFlag) {
					list.add(fi.getPath());
				}
			}
		}
		return list;
	}

	public List<String> getImgPathByRootPath(final String imgRootPath,
			String photoName) {
		List<String> list = new ArrayList<String>();
		File file = new File(imgRootPath);
		File[] allfiles = file.listFiles();
		if (allfiles == null) {
			return null;
		}
		for (int k = 0; k < allfiles.length; k++) {
			final File fi = allfiles[k];
			if (fi.isFile()) {
				int idx = fi.getPath().lastIndexOf(".");
				if (idx <= 0) {
					continue;
				}
				String suffix = fi.getPath().substring(idx);
				boolean isImgFlag = suffix.toLowerCase().equals(".jpg")
						|| suffix.toLowerCase().equals(".jpeg")
						|| suffix.toLowerCase().equals(".bmp")
						|| suffix.toLowerCase().equals(".png")
						|| suffix.toLowerCase().equals(".gif");
				if (isImgFlag && fi.getPath().contains(photoName)) {
					list.add(fi.getPath());
				}
			}
		}
		return list;
	}

	/**
	 * 通过路径列表获取图片
	 * 
	 * @author:zn
	 * @version:2015-2-12
	 * @param list
	 *            (图片的路径)
	 * @return
	 */
	public List<Bitmap> getBitMapListByPath(List<String> list) {
		List<Bitmap> imageBitmapList = new ArrayList<Bitmap>();
		for (int i = 0; i < list.size(); i++) {
			String filepath = list.get(i);
			File file = new File(filepath);
			if (file.exists()) {
				Bitmap bm = BitmapFactory.decodeFile(filepath);
				imageBitmapList.add(bm);
				bm = null;
			}
		}
		return imageBitmapList;
	}

	/**
	 * 根据图片路径获取单张图片
	 * 
	 * @author:zn
	 * @version:2015-2-27
	 * @param filePath
	 *            (图片的路径)
	 * @return
	 */
	public Bitmap getBitMapByPath(String filePath) {
		File file = new File(filePath);
		if (file.exists()) {
			return BitmapFactory.decodeFile(filePath);
		}
		return null;
	}

	/**
	 * 通过路径获取PCM音频完整路径列表
	 * 
	 * @author:zn
	 * @version:2015-2-28
	 * @param fileRootPath
	 * @return
	 */
	public List<String> getPcmPathByRootPath(final String fileRootPath) {
		List<String> list = new ArrayList<String>();
		File file = new File(fileRootPath);
		File[] allfiles = file.listFiles();
		if (allfiles == null) {
			return null;
		}
		for (int k = 0; k < allfiles.length; k++) {
			final File fi = allfiles[k];
			if (fi.isFile()) {
				int idx = fi.getPath().lastIndexOf(".");
				if (idx <= 0) {
					continue;
				}
				String suffix = fi.getPath().substring(idx);
				boolean isFileFlag = suffix.toLowerCase().equals(".pcm");
				// if (isImgFlag && fi.getPath().contains(photoName)) {
				if (isFileFlag) {
					list.add(fi.getPath());
				}
			}
		}
		return list;
	}

	/**
	 * 通过图片名称找到图片对应的PCM音频
	 * 
	 * @author:zn
	 * @version:2015-2-28
	 * @param filePath
	 * @param fileRootPath
	 * @return
	 */
	public String getPcmPathByImgName(String filePath, final String fileRootPath) {
		String pcmName = filePath.substring(filePath.lastIndexOf("/") + 1,
				filePath.lastIndexOf("."));
		File file = new File(fileRootPath);
		File[] allfiles = file.listFiles();
		if (allfiles == null) {
			return null;
		}
		for (int k = 0; k < allfiles.length; k++) {
			final File fi = allfiles[k];
			if (fi.isFile()) {
				int idx = fi.getPath().lastIndexOf(".");
				if (idx <= 0) {
					continue;
				}
				String suffix = fi.getPath().substring(idx);
				boolean isFileFlag = suffix.toLowerCase().equals(".pcm");
				// if (isImgFlag && fi.getPath().contains(photoName)) {
				if (isFileFlag && fi.getPath().contains(pcmName)) {
					return fi.getPath();

				}
			}
		}
		return null;
	}

	/**
	 * 保证每次联网的filename唯一
	 * 
	 * @author:zn
	 * @version:2015-3-2
	 * @param url
	 *            (url地址)
	 * @return(文件名称)
	 */
	public String getHashString(String url, String subixx) {
		try {
			MessageDigest mDigest = MessageDigest.getInstance("MD5");
			mDigest.update(url.getBytes());
			StringBuilder builder = new StringBuilder();

			for (byte b : mDigest.digest()) {
				builder.append(Integer.toHexString((b >> 4) & 0xf));
				builder.append(Integer.toHexString(b & 0xf));
			}
			return builder.toString() + "." + subixx;
		} catch (NoSuchAlgorithmException e) {
			return "ic_launcher";
		}
	}

	/**
	 * 获取文件大小
	 * 
	 * @author:zn
	 * @version:2015-3-10
	 * @param f
	 * @return
	 * @throws Exception
	 */
	public static long getFileSizes(File f) throws Exception {// 取得文件大小
		long s = 0;
		if (f.exists()) {
			FileInputStream fis = null;
			fis = new FileInputStream(f);
			s = fis.available();
		} else {
			f.createNewFile();
			System.out.println("文件不存在");
		}
		return s;
	}

	/**
	 * 递归获取文件夹大小
	 * 
	 * @author:zn
	 * @version:2015-3-10
	 * @param fileDirPath
	 *            (文件夹路径)
	 * @return
	 * @throws Exception
	 */
	public long getFileSize(String fileDirPath) throws Exception {
		File file = new File(fileDirPath);
		if (!file.exists()) {
			return 0;
		}
		return getFileSize(file);
	}

	/**
	 * 递归获取文件夹大小
	 * 
	 * @author:zn
	 * @version:2015-3-10
	 * @param f
	 * @return
	 * @throws Exception
	 */
	public long getFileSize(File f) throws Exception {
		long size = 0;// 取得文件夹大小
		File flist[] = f.listFiles();
		for (int i = 0; i < flist.length; i++) {
			if (flist[i].isDirectory()) {
				size = size + getFileSize(flist[i]);
			} else {
				size = size + flist[i].length();
			}
		}
		return size;
	}

	/**
	 * 转换文件大小
	 * 
	 * @author:zn
	 * @version:2015-3-10
	 * @param fileS
	 * @return
	 */
	public String formatFileSize(long fileS) {// 转换文件大小
		DecimalFormat df = new DecimalFormat("#.00");
		String fileSizeString = "";
		if (fileS == 0)
			return "0.00B";

		if (fileS < 1024) {
			fileSizeString = df.format((double) fileS) + "B";
		} else if (fileS < 1048576) {
			fileSizeString = df.format((double) fileS / 1024) + "K";
		} else if (fileS < 1073741824) {
			fileSizeString = df.format((double) fileS / 1048576) + "M";
		} else {
			fileSizeString = df.format((double) fileS / 1073741824) + "G";
		}
		return fileSizeString;
	}

	public void saveByte2File(byte[] bfile, String filePath) {
		BufferedOutputStream bos = null;
		FileOutputStream fos = null;
		File file = null;
		try {
			File dir = new File(filePath).getParentFile();
			if (!dir.exists() && dir.isDirectory()) {// 判断文件目录是否存在
				dir.mkdirs();
			}
			file = new File(filePath);
			fos = new FileOutputStream(file);
			bos = new BufferedOutputStream(fos);
			bos.write(bfile);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
			if (fos != null) {
				try {
					fos.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
			}
		}
	}

	public static boolean copyFile(String oldPath, String newPath) {
		try {
			int bytesum = 0;
			int byteread = 0;
			File oldfile = new File(oldPath);
			if (!oldfile.exists()) { // 文件不存在时
				InputStream inStream = new FileInputStream(oldPath); // 读入原文件
				FileOutputStream fs = new FileOutputStream(newPath);
				byte[] buffer = new byte[1444];
				int length;
				while ((byteread = inStream.read(buffer)) != -1) {
					bytesum += byteread; // 字节数 文件大小
					System.out.println(bytesum);
					fs.write(buffer, 0, byteread);
				}
				inStream.close();
			}
			return true;
		} catch (Exception e) {
			System.out.println("复制单个文件操作出错");
		}
		return false;
	}
}
