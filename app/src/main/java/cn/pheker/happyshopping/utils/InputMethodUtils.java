package cn.pheker.happyshopping.utils;

import android.content.Context;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

/**
 * @author: Biao
 * @创建时间: 2015-9-15 下午12:23:17
 * @描述信息: 输入法工具类封装
 * @svn提交者: $Author$
 * @提交时间: $Date: 2015-11-21 14:56:11 +0800 (Sat, 21 Nov 2015) $
 * @当前版本: $Rev: 39 $
 */
public class InputMethodUtils {
	/**
	 * 显示输入法 即自动弹出键盘
	 * 
	 * @param view
	 */
	public static void show(EditText view) {
		view.setFocusable(true);
		view.setFocusableInTouchMode(true);
		view.requestFocus();
		InputMethodManager inputManager = (InputMethodManager) view
				.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.toggleSoftInput(0, InputMethodManager.HIDE_NOT_ALWAYS);
	}

	/**
	 * 隐藏输入法 即关闭键盘
	 * 
	 * @param view
	 */
	public static void hide(EditText view) {
		view.setFocusable(false);
		InputMethodManager inputManager = (InputMethodManager) view
				.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
		inputManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

}
