package cn.pheker.happyshopping.utils;

import java.io.Closeable;
import java.io.IOException;

/**
 * @author: Biao
 * @创建时间: 2015-9-8 上午9:53:49
 * @描述信息: IO流工具类的封装
 * @svn提交者: $Author$
 * @提交时间: $Date: 2015-11-21 14:56:11 +0800 (Sat, 21 Nov 2015) $
 * @当前版本: $Rev: 39 $
 */
public class IOUtils {
	/**
	 * 关闭流
	 * 
	 * @param io
	 * @return
	 */
	public static boolean close(Closeable io) {
		if (io != null) {
			try {
				io.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return true;
	}
}
