package cn.pheker.happyshopping.utils;

import android.content.Context;
import android.graphics.drawable.AnimationDrawable;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.view.animation.TranslateAnimation;

/**
 * @author: Biao
 * @创建时间: 2015-9-22 下午11:36:14
 * @描述信息: 动画工具类的封装
 * @svn提交者: $Author$
 * @提交时间: $Date: 2015-11-21 14:56:11 +0800 (Sat, 21 Nov 2015) $
 * @当前版本: $Rev: 39 $
 */
public class AnimationUtil implements AnimationListener {
	private Animation animation;
	// 动画完成监听器
	private OnAnimationEndListener animationEndListener;
	// 动画开始监听器
	private OnAnimationStartListener animationStartListener;
	// 动画重复时的监听器
	private OnAnimationRepeatListener animationRepeatListener;

	public AnimationUtil(Context context, int resId) {
		this.animation = AnimationUtils.loadAnimation(context, resId);
		this.animation.setAnimationListener(this);
	}

	/**
	 * 自定义一个位移类型的Animation
	 * 
	 * @param fromXDelta
	 * @param toXDelta
	 * @param fromYDelta
	 * @param toYDelta
	 */
	public AnimationUtil(float fromXDelta, float toXDelta, float fromYDelta,
			float toYDelta) {
		animation = new TranslateAnimation(fromXDelta, toXDelta, fromYDelta,
				toYDelta);
	}

	/**
	 * 两个动画之间的时间间隔
	 * 
	 * @param startOffset
	 * @return
	 */
	public AnimationUtil setStartOffSet(long startOffset) {
		animation.setStartOffset(startOffset);
		return this;
	}

	/**
	 * 设置一个动画的插入器
	 * 
	 * @param interpolator
	 * @return
	 */
	public AnimationUtil setInterpolator(Interpolator interpolator) {
		animation.setInterpolator(interpolator);
		return this;
	}

	/**
	 * 设置线性插入器
	 * 
	 * @return
	 */
	public AnimationUtil setLinearInterpolator() {
		animation.setInterpolator(new LinearInterpolator());
		return this;
	}

	/**
	 * 开始动画
	 * 
	 * @param view
	 */
	public void startAnimation(View view) {
		view.startAnimation(animation);
	}

	/**
	 * 开启一个帧动画
	 * 
	 * @param resId
	 * @param view
	 */
	public static void startAnimation(int resId, View view) {
		view.setBackgroundResource(resId);
		((AnimationDrawable) view.getBackground()).start();
	}

	/**
	 * 设置动画时长
	 * 
	 * @param durationMillis
	 * @return
	 */
	public AnimationUtil setDuration(long durationMillis) {
		animation.setDuration(durationMillis);
		return this;
	}

	/**
	 * 设置动画结尾的位置
	 * 
	 * @param fillAfter
	 * @return
	 */
	public AnimationUtil setFillAfter(boolean fillAfter) {
		animation.setFillAfter(fillAfter);
		return this;
	}

	/**
	 * 
	 * @author: Biao
	 * @创建时间: 2015-9-22 下午11:33:17
	 * @描述信息: 定义动画结束监听器回调接口
	 * @svn提交者: $Author$
	 * @提交时间: $Date: 2015-11-21 14:56:11 +0800 (Sat, 21 Nov 2015) $
	 * @当前版本: $Rev: 39 $
	 */
	public interface OnAnimationEndListener {
		void onAnimationEnd(Animation animation);
	}

	/**
	 * 
	 * @author: Biao
	 * @创建时间: 2015-9-22 下午11:33:56
	 * @描述信息: 定义动画开始监听器回调接口
	 * @svn提交者: $Author$
	 * @提交时间: $Date: 2015-11-21 14:56:11 +0800 (Sat, 21 Nov 2015) $
	 * @当前版本: $Rev: 39 $
	 */
	public interface OnAnimationStartListener {
		void onAnimationStart(Animation animation);
	}

	/**
	 * 
	 * @author: Biao
	 * @创建时间: 2015-9-22 下午11:34:12
	 * @描述信息: 定义动画重复监听器回调接口
	 * @svn提交者: $Author$
	 * @提交时间: $Date: 2015-11-21 14:56:11 +0800 (Sat, 21 Nov 2015) $
	 * @当前版本: $Rev: 39 $
	 */
	public interface OnAnimationRepeatListener {
		void onAnimationRepeat(Animation animation);
	}

	/**
	 * 设置动画结束监听器
	 * 
	 * @param listener
	 * @return
	 */
	public AnimationUtil setOnAnimationEndLinstener(
			OnAnimationEndListener listener) {
		this.animationEndListener = listener;
		return this;
	}

	/**
	 * 设置动画开始监听器
	 * 
	 * @param listener
	 * @return
	 */
	public AnimationUtil setOnAnimationStartLinstener(
			OnAnimationStartListener listener) {
		this.animationStartListener = listener;
		return this;
	}

	/**
	 * 设置动画重复监听器
	 * 
	 * @param listener
	 * @return
	 */
	public AnimationUtil setOnAnimationRepeatLinstener(
			OnAnimationRepeatListener listener) {
		this.animationRepeatListener = listener;
		return this;
	}

	/**
	 * 设置动画监听器
	 * 
	 * @param animationListener
	 */
	public void setAnimationListener(AnimationListener animationListener) {
		animation.setAnimationListener(animationListener);
	}

	@Override
	public void onAnimationStart(Animation animation) {
		if (this.animationStartListener != null) {
			this.animationStartListener.onAnimationStart(animation);
		}
	}

	@Override
	public void onAnimationEnd(Animation animation) {
		if (this.animationEndListener != null) {
			this.animationEndListener.onAnimationEnd(animation);
		}
	}

	@Override
	public void onAnimationRepeat(Animation animation) {
		if (this.animationRepeatListener != null) {
			this.animationRepeatListener.onAnimationRepeat(animation);
		}
	}

}
