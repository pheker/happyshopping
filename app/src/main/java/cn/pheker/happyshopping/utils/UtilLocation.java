package cn.pheker.happyshopping.utils;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.entities.Position;
import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import static android.content.Context.LOCATION_SERVICE;
import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/2
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description 定位工具类,获取当前位置坐标,并转换为物理地址
 *
 *  we can use it like the following.
 *
    UtilLocation.showLocation(this, new UtilLocation.PositionCallback() {
        @Override
        public void showPosition(final Position position) {
            runOnUiThread(new Runnable()
            {
                @Override
                public void run()
                {
                    position_show_position.setText(position.getResult().getFormatted_address());
                }

            });
        }
    });
 *
 * </pre>
 */

public class UtilLocation {


    public static String provider;//位置提供器
    public static LocationManager locationManager;//位置服务
    public static Location location;

    public static void showLocation(Context activity,final PositionCallback positionCallback){
        //定位
        locationManager = (LocationManager) activity.getSystemService(LOCATION_SERVICE);//获得位置服务
        provider = UtilLocation.judgeProvider(activity,locationManager);

        if (provider != null) {//有位置提供器的情况
            Log.d(TAG, "initView: "+provider);
            //为了压制getLastKnownLocation方法的警告
            if (ActivityCompat.checkSelfPermission(activity,
                    Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                    && ActivityCompat.checkSelfPermission(activity,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            location = locationManager.getLastKnownLocation(provider);
            if (location != null) {
                getLocation(location,positionCallback);//得到当前经纬度并开启线程去反向地理编码
            } else {
                Toast.makeText(activity, "暂时无法获得当前位置", Toast.LENGTH_SHORT).show();
            }
        }else{//不存在位置提供器的情况

        }
    }

    /**
     * 判断是否有可用的内容提供器
     * @return 不存在返回null
     */
    public static String judgeProvider(Context context ,LocationManager locationManager) {
        List<String> prodiverlist = locationManager.getProviders(true);
        if(prodiverlist.contains(LocationManager.NETWORK_PROVIDER)){
            return LocationManager.NETWORK_PROVIDER;
        }else if(prodiverlist.contains(LocationManager.GPS_PROVIDER)) {
            return LocationManager.GPS_PROVIDER;
        }else{
            Toast.makeText(context,"无法获取当前网络位置",Toast.LENGTH_SHORT).show();//没有可用的位置提供器
        }
        return null;
    }


    /**
     * 得到当前经纬度并开启线程去反向地理编码
     */
    public static void getLocation(Location location, final PositionCallback positionCallback) {
        String latitude = location.getLatitude()+"";
        String longitude = location.getLongitude()+"";
        String url = "http://api.map.baidu.com/geocoder/v2/?ak=pPGNKs75nVZPloDFuppTLFO3WXebPgXg&callback=renderReverse&location="+latitude+","+longitude+"&output=json&pois=0";

        OkHttpClient client = new OkHttpClient();
        final Request request = new Request.Builder().url(url).build();
        client.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {

            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                final String res = response.body().string();
                String str =  res.replace("renderReverse&&renderReverse","").replace("(","").replace(")","");

                Position position = UtilGson.fromJson(str, new TypeToken<Position>(){}.getType());
                positionCallback.showPosition(position);

            }
        });
    }

    /**
     * 自定义回调函数
     */
    public interface PositionCallback{
        public void showPosition(final Position position);
    }


}
