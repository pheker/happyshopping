package cn.pheker.happyshopping.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import android.content.Context;
import android.os.Environment;

/**
 * @author: zn E-mail:zhangn@jtv.com.cn 
 * @version:2015-3-2
 * 类说明
 */

public class VoiceUtil {
	private String SDPATH; 
	public String root=null;
	  
    public String getSDPATH() {  
        return SDPATH;  
    }  
    public VoiceUtil(Context context) {  
        //得到当前外部存储设备的目录  
        // /SDCARD  
       // SDPATH = Environment.getExternalStorageDirectory()+"/"; 
    	/*************判断文件SDcard是否存在并且是否可以读写********/
		if(Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)){
			/* 存在sdcard并且具有读写权限 根目录为sdcard根目录 */
			root = Environment.getExternalStorageDirectory().getAbsolutePath();
		}else{
			/* 不存在sdcard或者sdcard不具有读写权限 根目录为android系统根目录 */
			root = Environment.getRootDirectory().getAbsolutePath();
		}
		/* 默认根目录为app包名目录 */
		String packageName = context.getApplicationInfo().packageName;
		SDPATH =root+"/"+packageName+"/";
    }  
    /** 
     * 在SD卡上创建文件 
     *  
     * @throws IOException 
     */  
    public File creatSDFile(String fileName) throws IOException {  
        File file = new File(SDPATH + fileName);  
        file.createNewFile();  
        return file;  
    }  
      
    /** 
     * 在SD卡上创建目录 
     *  
     * @param dirName 
     */  
    public File creatSDDir(String dirName) {  
        File dir = new File(SDPATH + dirName);  
        dir.mkdir();  
        return dir;  
    }  
  
    /** 
     * 判断SD卡上的文件夹是否存在 
     */  
    public boolean isFileExist(String fileName){  
        File file = new File(SDPATH + fileName);  
        return file.exists();  
    }  
      
   /**
    * 将一个InputStream里面的数据写入到SD卡中  
    * @author:zn
    * @version:2015-3-2
    * @param path
    * @param fileName
    * @param input
    * @return
    */
    public File write2SDFromInput(String path,String fileName,InputStream input){  
        File file = null;  
        OutputStream output = null;  
        try{  
            creatSDDir(path);  
            file = creatSDFile(path + fileName);  
            output = new FileOutputStream(file);  
//            byte buffer [] = new byte[4 * 1024];  
//            while((input.read(buffer)) != -1){  
//                output.write(buffer);  
//            }  
//            output.flush();  
            byte[] voice_bytes = new byte[1024];  
            int len1 = -1;  
            while ((len1 = input.read(voice_bytes)) != -1) {  
                output.write(voice_bytes, 0, len1);  
                output.flush();  
            }  
        }  
        catch(Exception e){  
            e.printStackTrace();  
        }  
        finally{  
            try{  
                output.close();  
            }  
            catch(Exception e){  
                e.printStackTrace();  
            }  
        }  
        return file;  
    }  
	
    /**
     * 文件名称唯一性
     * @author:zn
     * @version:2015-3-2
     * @param url
     * @return
     */
	public String getHashString(String url) {
		try {
			MessageDigest mDigest = MessageDigest.getInstance("MD5");
			mDigest.update(url.getBytes());
			StringBuilder builder = new StringBuilder();

			for (byte b : mDigest.digest()) {
				builder.append(Integer.toHexString((b >> 4) & 0xf));
				builder.append(Integer.toHexString(b & 0xf));
			}
			return builder.toString() + ".pcm";
		} catch (NoSuchAlgorithmException e) {
			return "ic_launcher";
		}
	}
}
