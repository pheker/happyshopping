package cn.pheker.happyshopping.utils;

import java.util.List;

import android.app.ActivityManager;
import android.content.Context;

/**
 * 
 * @author: Biao
 * @创建时间: 2016-5-16 下午4:48:51
 * @描述信息: 系统工具类封装
 * @svn提交者: $Author$
 * @提交时间: $Date$
 * @当前版本: $Rev$
 */
public class SystemUtils {
	/**
	 * 判断应用是否已经启动
	 * 
	 * @param context
	 *            一个context
	 * @param packageName
	 *            要判断应用的包名
	 * @return boolean
	 */
	public static boolean isAppAlive(Context context, String packageName) {
		ActivityManager activityManager = (ActivityManager) context
				.getSystemService(Context.ACTIVITY_SERVICE);
		List<ActivityManager.RunningAppProcessInfo> processInfos = activityManager
				.getRunningAppProcesses();
		for (int i = 0; i < processInfos.size(); i++) {
			if (processInfos.get(i).processName.equals(packageName)) {
				return true;
			}
		}
		return false;
	}
}
