package cn.pheker.happyshopping.utils;


import android.graphics.Bitmap;
import android.support.v4.util.LruCache;

/**
 * 
 * @author: Biao
 * @创建时间: 2015-11-7 上午10:12:21
 * @描述信息: 图片缓存工具类封装
 * @svn提交者: $Author$
 * @提交时间: $Date: 2015-11-21 14:56:11 +0800 (Sat, 21 Nov 2015) $
 * @当前版本: $Rev: 39 $
 */
public class ImageCacheUtils {
	private static ImageCacheUtils imageCache = null;
	private LruCache<String, Bitmap> cache = null;

	private ImageCacheUtils() {
		// 定义缓存大小,1/8的存储空间
		cache = new LruCache<String, Bitmap>((int) (Runtime.getRuntime()
				.maxMemory() / 8)) {
			@Override
			protected int sizeOf(String key, Bitmap value) {
				return value.getRowBytes() * value.getHeight();
			}
		};
	}

	/**
	 * 获取实例
	 * 
	 * @return
	 */
	public static synchronized ImageCacheUtils getInstance() {
		if (imageCache == null) {
			synchronized (ImageCacheUtils.class) {
				if (imageCache == null) {
					imageCache = new ImageCacheUtils();
				}
			}
		}
		return imageCache;

	}

	/**
	 * 往缓存中放入图片
	 * 
	 * @param key
	 * @param value
	 * @return
	 */
	public Bitmap putBitmap(String key, Bitmap value) {
		return cache.put(key, value);
	}

	/**
	 * 通过key获取缓存图片
	 * 
	 * @param key
	 * @return
	 */
	public Bitmap getBitmap(String key) {
		return cache.get(key);
	}
}
