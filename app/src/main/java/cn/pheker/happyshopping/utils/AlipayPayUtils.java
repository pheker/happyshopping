package cn.pheker.happyshopping.utils;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

/**
 * 
 * @author: Biao
 * @创建时间: 2016-4-16 下午3:41:52
 * @描述信息: 支付宝支付工具类封装
 * @svn提交者: $Author$
 * @提交时间: $Date$
 * @当前版本: $Rev$
 */
public class AlipayPayUtils {

	/**
	 * 创建订单信息
	 * 
	 * @param subject
	 *            商品名称
	 * @param taskId
	 *            任务编号
	 * @param type
	 *            type:recharge充值，guarantee担保, annual年费
	 * @param userId
	 *            用户id
	 * @param price
	 *            支付金额
	 * @return
	 */
	public static String getOrderInfo(String subject, String taskId,
			String type, String userId, String price) {

		// 签约合作者身份ID
		String orderInfo = "partner=" + "\"" + LotusConstants.ALIPAY_PARTNER
				+ "\"";

		// 签约卖家支付宝账号
		orderInfo += "&seller_id=" + "\"" + LotusConstants.ALIPAY_SELLER + "\"";

		// 商户网站唯一订单号(以任务编号作为订单号)
		orderInfo += "&out_trade_no=" + "\"" + taskId + "\"";

		// 商品名称
		orderInfo += "&subject=" + "\"" + subject + "\"";

		// 商品详情
		orderInfo += "&body=" + "\"" + type + "," + userId + "\"";

		// 商品金额
		orderInfo += "&total_fee=" + "\"" + price + "\"";

		// 服务器异步通知页面路径
		orderInfo += "&notify_url=" + "\"" + LotusConstants.ALIPAY_NOTIFY_URL
				+ "\"";

		// 服务接口名称， 固定值
		orderInfo += "&service=\"mobile.securitypay.pay\"";

		// 支付类型， 固定值
		orderInfo += "&payment_type=\"1\"";

		// 参数编码， 固定值
		orderInfo += "&_input_charset=\"utf-8\"";

		// 设置未付款交易的超时时间
		// 默认30分钟，一旦超时，该笔交易就会自动被关闭。
		// 取值范围：1m～15d。
		// m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
		// 该参数数值不接受小数点，如1.5h，可转换为90m。
		orderInfo += "&it_b_pay=\"30m\"";

		// extern_token为经过快登授权获取到的alipay_open_id,带上此参数用户将使用授权的账户进行支付
		// orderInfo += "&extern_token=" + "\"" + extern_token + "\"";

		// 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
		orderInfo += "&return_url=\"m.alipay.com\"";

		// 调用银行卡支付，需配置此参数，参与签名， 固定值 （需要签约《无线银行卡快捷支付》才能使用）
		// orderInfo += "&paymethod=\"expressGateway\"";

		return orderInfo;
	}

	/**
	 * 按类型创建订单信息
	 * 
	 * @param subject
	 *            商品名称
	 * @param type
	 *            type:recharge充值，annual年费
	 * @param userId
	 *            用户id
	 * @param price
	 *            支付金额
	 * @return
	 */
	public static String getOrderInfoByType(String subject, String type,
			String userId, String price) {

		// 签约合作者身份ID
		String orderInfo = "partner=" + "\"" + LotusConstants.ALIPAY_PARTNER
				+ "\"";

		// 签约卖家支付宝账号
		orderInfo += "&seller_id=" + "\"" + LotusConstants.ALIPAY_SELLER + "\"";

		// 商户网站唯一订单号(以任务编号作为订单号)
		orderInfo += "&out_trade_no=" + "\"" + getOutTradeNo() + "\"";

		// 商品名称
		orderInfo += "&subject=" + "\"" + subject + "\"";

		// 商品详情
		orderInfo += "&body=" + "\"" + type + "," + userId + "\"";

		// 商品金额
		orderInfo += "&total_fee=" + "\"" + price + "\"";

		// 服务器异步通知页面路径
		orderInfo += "&notify_url=" + "\"" + LotusConstants.ALIPAY_NOTIFY_URL
				+ "\"";

		// 服务接口名称， 固定值
		orderInfo += "&service=\"mobile.securitypay.pay\"";

		// 支付类型， 固定值
		orderInfo += "&payment_type=\"1\"";

		// 参数编码， 固定值
		orderInfo += "&_input_charset=\"utf-8\"";

		// 设置未付款交易的超时时间
		// 默认30分钟，一旦超时，该笔交易就会自动被关闭。
		// 取值范围：1m～15d。
		// m-分钟，h-小时，d-天，1c-当天（无论交易何时创建，都在0点关闭）。
		// 该参数数值不接受小数点，如1.5h，可转换为90m。
		orderInfo += "&it_b_pay=\"30m\"";

		// extern_token为经过快登授权获取到的alipay_open_id,带上此参数用户将使用授权的账户进行支付
		// orderInfo += "&extern_token=" + "\"" + extern_token + "\"";

		// 支付宝处理完请求后，当前页面跳转到商户指定页面的路径，可空
		orderInfo += "&return_url=\"m.alipay.com\"";

		// 调用银行卡支付，需配置此参数，参与签名， 固定值 （需要签约《无线银行卡快捷支付》才能使用）
		// orderInfo += "&paymethod=\"expressGateway\"";

		return orderInfo;
	}

	/**
	 * 生成商户订单号，该值在商户端应保持唯一（可自定义格式规范）
	 * 
	 */
	private static String getOutTradeNo() {
		SimpleDateFormat format = new SimpleDateFormat("MMddHHmmss",
				Locale.getDefault());
		Date date = new Date();
		String key = format.format(date);

		Random r = new Random();
		key = key + r.nextInt();
		key = key.substring(0, 15);
		return key;
	}

	/**
	 * 对订单信息进行签名
	 * 
	 * @param content
	 *            待签名订单信息
	 */
	public static String signOrder(String content) {
		return SignUtils.sign(content, LotusConstants.ALIPAY_RSA_PRIVATE);
	}

	/**
	 * 获取签名方式
	 * 
	 */
	public static String getSignType() {
		return "sign_type=\"RSA\"";
	}

}
