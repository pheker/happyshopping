package cn.pheker.happyshopping.entities;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/2
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description
 *
 * </pre>
 */

public class Position {
    private Result result;

    private int status;

    public void setResult(Result result) {
        this.result = result;
    }

    public Result getResult() {
        return this.result;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Position{" +
                "result=" + result +
                ", status=" + status +
                '}';
    }

    public int getStatus() {
        return this.status;
    }

}




/**

 {
 "result": {
 "addressComponent": {
 "adcode": "410102",
 "city": "郑州市",
 "country": "中国",
 "country_code": 0,
 "direction": "",
 "distance": "",
 "district": "中原区",
 "province": "河南省",
 "street": "文化宫路",
 "street_number": ""
 },
 "business": "汝河路,淮河路,儿童公园",
 "cityCode": 268,
 "formatted_address": "河南省郑州市中原区文化宫路",
 "location": {
 "lat": 34.7368009738792,
 "lng": 113.62199499999994
 },
 "poiRegions": [
 {
 "direction_desc": "内",
 "name": "桐淮小学",
 "tag": "教育培训"
 }
 ],
 "pois": [],
 "sematic_description": "桐淮小学内,文化宫路7号院西南98米"
 },
 "status": 0
 }

 */