package cn.pheker.happyshopping.entities;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/1
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description
 *
 * </pre>
 */

public class Product {


    private String id;

    private String sellerId;

    private String title;

    private String content;

    private String freeDelivery;

    private String picpath;

    private String price;

    private String paynum;

    public void setId(String id){
        this.id = id;
    }
    public String getId(){
        return this.id;
    }
    public void setSellerId(String sellerId){
        this.sellerId = sellerId;
    }
    public String getSellerId(){
        return this.sellerId;
    }
    public void setTitle(String title){
        this.title = title;
    }
    public String getTitle(){
        return this.title;
    }
    public void setContent(String content){
        this.content = content;
    }
    public String getContent(){
        return this.content;
    }
    public void setFreeDelivery(String freeDelivery){
        this.freeDelivery = freeDelivery;
    }
    public String getFreeDelivery(){
        return this.freeDelivery;
    }
    public void setPicpath(String picpath){
        this.picpath = picpath;
    }
    public String getPicpath(){
        return this.picpath;
    }
    public void setPrice(String price){
        this.price = price;
    }
    public String getPrice(){
        return this.price;
    }
    public void setPaynum(String paynum){
        this.paynum = paynum;
    }
    public String getPaynum(){
        return this.paynum;
    }





}
