package cn.pheker.happyshopping.entities;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/2
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description
 *
 * </pre>
 */

public class PoiRegions {
    private String direction_desc;

    private String name;

    private String tag;

    @Override
    public String toString() {
        return "PoiRegions{" +
                "direction_desc='" + direction_desc + '\'' +
                ", name='" + name + '\'' +
                ", tag='" + tag + '\'' +
                '}';
    }

    public void setDirection_desc(String direction_desc) {
        this.direction_desc = direction_desc;
    }

    public String getDirection_desc() {
        return this.direction_desc;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getTag() {
        return this.tag;
    }

}