package cn.pheker.happyshopping.entities;

import java.util.Date;

import java.io.Serializable;

public class HsCollection implements Serializable {
    private String uuid;

    private String userId;

    /**
     * 商品或商家ID
     */
    private String collectionId;

    /**
     *  类型:0关注商家,1收藏商品,2购物车商品
     */
    private Integer type;

    private Date createTime;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getCollectionId() {
        return collectionId;
    }

    public void setCollectionId(String collectionId) {
        this.collectionId = collectionId == null ? null : collectionId.trim();
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public HsCollection(String uuid, String userId, String collectionId, Integer type) {
        this.uuid = uuid;
        this.userId = userId;
        this.collectionId = collectionId;
        this.type = type;
    }
}