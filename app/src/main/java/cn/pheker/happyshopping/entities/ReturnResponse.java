package cn.pheker.happyshopping.entities;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/5
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description
 *      create,delete,update 的返回json对应的实体类
 *
 * </pre>
 */

public class ReturnResponse {

    private int code;
    private String msg;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    @Override
    public String toString() {
        return "ReturnResponse{" +
                "code=" + code +
                ", msg='" + msg + '\'' +
                '}';
    }
}
