package cn.pheker.happyshopping.entities;

import java.util.Date;

import java.io.Serializable;

public class HsProduct implements Serializable {
    private String uuid;

    private String name;

    private String category;

    private String ownerId;

    private Float price;

    private Integer num;

    private Integer paynum;

    private Float evaluation;

    private String picspath;

    private String description;

    private Float discount;

    private Float groupPurchase;

    private Float coupon;

    private Date createTime;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name == null ? null : name.trim();
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category == null ? null : category.trim();
    }

    public String getOwnerId() {
        return ownerId;
    }

    public void setOwnerId(String ownerId) {
        this.ownerId = ownerId == null ? null : ownerId.trim();
    }

    public Float getPrice() {
        return price;
    }

    public void setPrice(Float price) {
        this.price = price;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Integer getPaynum() {
        return paynum;
    }

    public void setPaynum(Integer paynum) {
        this.paynum = paynum;
    }

    public Float getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Float evaluation) {
        this.evaluation = evaluation;
    }

    public String getPicspath() {
        return picspath;
    }

    public void setPicspath(String picspath) {
        this.picspath = picspath == null ? null : picspath.trim();
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description == null ? null : description.trim();
    }

    public Float getDiscount() {
        return discount;
    }

    public void setDiscount(Float discount) {
        this.discount = discount;
    }

    public Float getGroupPurchase() {
        return groupPurchase;
    }

    public void setGroupPurchase(Float groupPurchase) {
        this.groupPurchase = groupPurchase;
    }

    public Float getCoupon() {
        return coupon;
    }

    public void setCoupon(Float coupon) {
        this.coupon = coupon;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public HsProduct(String uuid) {
        this.uuid = uuid;
    }
}