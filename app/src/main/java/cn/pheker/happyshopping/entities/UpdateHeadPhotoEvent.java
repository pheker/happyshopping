package cn.pheker.happyshopping.entities;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/22
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description
 *
 * </pre>
 */

public class UpdateHeadPhotoEvent {
    private HsUser HsUser;
    private String headPhoto;

    public UpdateHeadPhotoEvent() {
    }

    public UpdateHeadPhotoEvent(HsUser hsUser) {
        HsUser = hsUser;
    }

    public UpdateHeadPhotoEvent(cn.pheker.happyshopping.entities.HsUser hsUser, String headPhoto) {
        HsUser = hsUser;
        this.headPhoto = headPhoto;
    }

    public HsUser getHsUser() {
        return HsUser;
    }

    public void setHsUser(HsUser hsUser) {
        HsUser = hsUser;
    }

    public String getHeadPhoto() {
        return headPhoto;
    }

    public void setHeadPhoto(String headPhoto) {
        this.headPhoto = headPhoto;
    }
}
