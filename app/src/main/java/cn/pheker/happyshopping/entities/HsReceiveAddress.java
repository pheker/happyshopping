package cn.pheker.happyshopping.entities;

import java.util.Date;

import java.io.Serializable;

public class HsReceiveAddress implements Serializable {
    public HsReceiveAddress() {
    }

    public HsReceiveAddress(String uuid, String userId, String deliveryAddress, String receivePersonName, String receivePersonPhone, String receiveAddressAlias) {
        this.uuid = uuid;
        this.userId = userId;
        this.deliveryAddress = deliveryAddress;
        this.receivePersonName = receivePersonName;
        this.receivePersonPhone = receivePersonPhone;
        this.receiveAddressAlias = receiveAddressAlias;
    }

    private String uuid;

    private String userId;

    private String deliveryAddress;

    private String receivePersonName;

    private String receivePersonPhone;

    private Date createTime;

    private String receiveAddressAlias;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress == null ? null : deliveryAddress.trim();
    }

    public String getReceivePersonName() {
        return receivePersonName;
    }

    public void setReceivePersonName(String receivePersonName) {
        this.receivePersonName = receivePersonName == null ? null : receivePersonName.trim();
    }

    public String getReceivePersonPhone() {
        return receivePersonPhone;
    }

    public void setReceivePersonPhone(String receivePersonPhone) {
        this.receivePersonPhone = receivePersonPhone == null ? null : receivePersonPhone.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getReceiveAddressAlias() {
        return receiveAddressAlias;
    }

    public void setReceiveAddressAlias(String receiveAddressAlias) {
        this.receiveAddressAlias = receiveAddressAlias == null ? null : receiveAddressAlias.trim();
    }
}