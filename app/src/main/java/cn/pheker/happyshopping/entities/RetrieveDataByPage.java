package cn.pheker.happyshopping.entities;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/8
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description
 *
 * </pre>
 */

public class RetrieveDataByPage<T> {
    /**
     * code : 1
     * count : 1
     * data : [{"category":"dsf","createTime":"2017/04/24 01:12:25","description":"hello,cn.pheker","evaluation":"0","name":"sdfa","num":"0","ownerid":"sdfa","paynum":"0","price":"12","uuid":"uuid"}]
     * maxPage : 1
     * msg : success
     * page : 1
     */

    private int code;
    private int count;
    private int maxPage;
    private String msg;
    private int page;
    private T data;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(int maxPage) {
        this.maxPage = maxPage;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }


}
