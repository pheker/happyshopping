package cn.pheker.happyshopping.entities;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/22
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description 订单 expandableList group
 *
 * </pre>
 */

public class OrderGroup {

    private String uuid;
    private String sumPrice;

    public OrderGroup(String uuid, String sumPrice) {
        this.uuid = uuid;
        this.sumPrice = sumPrice;
    }

    public String getUuid() {

        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getSumPrice() {
        return sumPrice;
    }

    public void setSumPrice(String sumPrice) {
        this.sumPrice = sumPrice;
    }

}
