package cn.pheker.happyshopping.entities;

import java.util.List;

/**
* <pre>
* @user cn.pheker
* @date 2017/5/2
* @email hkbxoic@gmail.com
* @version 1.0.0
* @description
*
* </pre>
*/

public class Result {

    private AddressComponent addressComponent;

    private String business;

    private int cityCode;

    private String formatted_address;

    private Location location;

    private List<PoiRegions> poiRegions;

    private List<Pois> pois;

    private String sematic_description;

    public void setAddressComponent(AddressComponent addressComponent) {
        this.addressComponent = addressComponent;
    }

    public AddressComponent getAddressComponent() {
        return this.addressComponent;
    }

    public void setBusiness(String business) {
        this.business = business;
    }

    public String getBusiness() {
        return this.business;
    }

    @Override
    public String toString() {
        return "Result{" +
                "addressComponent=" + addressComponent +
                ", business='" + business + '\'' +
                ", cityCode=" + cityCode +
                ", formatted_address='" + formatted_address + '\'' +
                ", location=" + location +
                ", poiRegions=" + poiRegions +
                ", pois=" + pois +
                ", sematic_description='" + sematic_description + '\'' +
                '}';
    }

    public void setCityCode(int cityCode) {
        this.cityCode = cityCode;
    }

    public int getCityCode() {
        return this.cityCode;
    }

    public void setFormatted_address(String formatted_address) {
        this.formatted_address = formatted_address;
    }

    public String getFormatted_address() {
        return this.formatted_address;
    }

    public void setLocation(Location location) {
        this.location = location;
    }

    public Location getLocation() {
        return this.location;
    }

    public void setPoiRegions(List<PoiRegions> poiRegions) {
        this.poiRegions = poiRegions;
    }

    public List<PoiRegions> getPoiRegions() {
        return this.poiRegions;
    }

    public void setPois(List<Pois> pois) {
        this.pois = pois;
    }

    public List<Pois> getPois() {
        return this.pois;
    }

    public void setSematic_description(String sematic_description) {
        this.sematic_description = sematic_description;
    }

    public String getSematic_description() {
        return this.sematic_description;
    }
}