package cn.pheker.happyshopping.entities;

import java.util.Date;

import java.io.Serializable;

public class HsOrder implements Serializable {
    private String uuid;

    private String productId;

    public HsOrder(String uuid, String orderGroup, String payType, Float sumMoney, Integer num, String userId, String productId) {
        this.uuid = uuid;
        this.orderGroup = orderGroup;
        this.payType = payType;
        this.sumMoney = sumMoney;
        this.num = num;
        this.userId = userId;
        this.productId = productId;
    }

    private String userId;

    private Integer num;

    private Float sumMoney;

    private String payType;

    private String orderGroup;

    private Date createTime;

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid == null ? null : uuid.trim();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId == null ? null : productId.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public Float getSumMoney() {
        return sumMoney;
    }

    public void setSumMoney(Float sumMoney) {
        this.sumMoney = sumMoney;
    }

    public String getPayType() {
        return payType;
    }

    public void setPayType(String payType) {
        this.payType = payType == null ? null : payType.trim();
    }

    public String getOrderGroup() {
        return orderGroup;
    }

    public void setOrderGroup(String orderGroup) {
        this.orderGroup = orderGroup == null ? null : orderGroup.trim();
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }
}