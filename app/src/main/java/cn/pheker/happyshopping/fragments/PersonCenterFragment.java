package cn.pheker.happyshopping.fragments;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.gson.reflect.TypeToken;
import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.activities.RegisterActivity;
import cn.pheker.happyshopping.activities.UserInfoAboutActivity;
import cn.pheker.happyshopping.activities.UserInfoActivity;
import cn.pheker.happyshopping.activities.UserInfoCreditCardActivity;
import cn.pheker.happyshopping.activities.UserInfoFavorActivity;
import cn.pheker.happyshopping.activities.UserInfoFeedBackActivity;
import cn.pheker.happyshopping.activities.UserInfoPositionActivity;
import cn.pheker.happyshopping.activities.UserInfoRankActivity;
import cn.pheker.happyshopping.activities.UserInfoSettingActivity;
import cn.pheker.happyshopping.activities.UserinfoBalanceActivity;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.EventBusUtils;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.commons.UtilLogin;
import cn.pheker.happyshopping.entities.HsUser;
import cn.pheker.happyshopping.entities.ReturnResponseWithData;
import cn.pheker.happyshopping.entities.UpdateHeadPhotoEvent;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;

public class PersonCenterFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "PersonCenterFragment";
    private LinearLayout userinfo_to_credit_card;
    private LinearLayout userinfo_to_position;
    private LinearLayout userinfo_to_favor;
    private LinearLayout userinfo_to_balance;
    private LinearLayout userinfo_to_rank;
    private LinearLayout userinfo_to_setting;
    private LinearLayout userinfo_to_about;
    private LinearLayout user_info_feed_back;

    public PersonCenterFragment() {
    }

    LinearLayout person_header;      //头部区域
    CircleImageView person_center_headphoto;  //头像
    ImageView person_center_rank_img;   //等级图片
    TextView person_center_rank,     //等级分数
            person_center_nick,     //昵称
            person_center_balance;  //余额

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_person_center, container, false);//fragment_personcenter
        AlexStatusBarUtils.setStatusColor(getActivity(), getResources().getColor(R.color.bg_light_greenblue), 0);

        ActivityCompat.requestPermissions(getActivity(),
                new String[]{Manifest.permission.CAMERA,Manifest.permission.WRITE_EXTERNAL_STORAGE},
                1);

        EventBusUtils.register(this);

        initView(view);
        initEvent(view);


        return view;
    }

    private void initView(View view) {
        person_header = ((LinearLayout) view.findViewById(R.id.person_header));
        person_center_headphoto = ((CircleImageView) view.findViewById(R.id.person_center_headphoto));
        person_center_rank_img = ((ImageView) view.findViewById(R.id.person_center_rank_img));
        person_center_rank = ((TextView) view.findViewById(R.id.person_center_rank));
        person_center_nick = ((TextView) view.findViewById(R.id.person_center_nick));
        person_center_balance = ((TextView) view.findViewById(R.id.person_center_balance));


        person_header.setOnClickListener(this);
        userinfo_to_credit_card = (LinearLayout) view.findViewById(R.id.userinfo_to_credit_card);
        userinfo_to_credit_card.setOnClickListener(this);
        userinfo_to_position = (LinearLayout) view.findViewById(R.id.userinfo_to_position);
        userinfo_to_position.setOnClickListener(this);
        userinfo_to_favor = (LinearLayout) view.findViewById(R.id.userinfo_to_favor);
        userinfo_to_favor.setOnClickListener(this);
        userinfo_to_balance = (LinearLayout) view.findViewById(R.id.userinfo_to_balance);
        userinfo_to_balance.setOnClickListener(this);
        userinfo_to_rank = (LinearLayout) view.findViewById(R.id.userinfo_to_rank);
        userinfo_to_rank.setOnClickListener(this);
        userinfo_to_setting = (LinearLayout) view.findViewById(R.id.userinfo_to_setting);
        userinfo_to_setting.setOnClickListener(this);
        userinfo_to_about = (LinearLayout) view.findViewById(R.id.userinfo_to_about);
        userinfo_to_about.setOnClickListener(this);
        user_info_feed_back = (LinearLayout) view.findViewById(R.id.user_info_feed_back);
        user_info_feed_back.setOnClickListener(this);
    }

    private void initEvent(View view) {
        String loginUserUuid = UtilLogin.getLoginHsUserUuid(getActivity());
        Log.i(TAG, "initEvent: " + loginUserUuid);
        //获取用户信息
        OkHttpUtils.post()
                .addParams("uuid", loginUserUuid)
                .url(Constants.Url.RETRIEVE_HsUser_byUUID)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Toast.makeText(getActivity(), e.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        Log.i(TAG, "onResponse: " + response);
                        ReturnResponseWithData<HsUser> responseEntity = UtilGson.fromJson(response.toString(),
                                new TypeToken<ReturnResponseWithData<HsUser>>() {
                                }.getType());
                        Log.d(TAG, "onResponse: " + responseEntity.toString());
                        int code = responseEntity.getCode();
                        if (code == 1) {
                            HsUser hsUser = responseEntity.getData();
                            if (hsUser != null) {
                                UtilLogin.saveHsUser(getActivity(), hsUser.getUuid(), UtilGson.toJson(hsUser));
                                showHsUser(hsUser);
                            } else {
                                Toast.makeText(getActivity(), "没有查询到该用户信息", Toast.LENGTH_SHORT).show();
                            }
                        } else if (code == 2) {//未注册
                            Toast.makeText(getActivity(), responseEntity.getMsg(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getActivity(), RegisterActivity.class));
                        } else if (code == 3) {//用户名或密码错误
                            Toast.makeText(getActivity(), responseEntity.getMsg(), Toast.LENGTH_SHORT).show();
                            startActivity(new Intent(getActivity(), RegisterActivity.class));
                        } else {
                            Toast.makeText(getActivity(), responseEntity.getMsg(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });

    }

    /**
     * 显示用户信息
     *
     * @param hsUser
     */
    private void showHsUser(HsUser hsUser) {
        //加载,显示头像
        String headPhotoPath = hsUser.getHeadphotopath();
        if (headPhotoPath != null && !"".equals(headPhotoPath) && headPhotoPath.startsWith("/static")) {
            headPhotoPath = Constants.Url.SERVER_ADDRESS + headPhotoPath;
        }
        Log.i(TAG, "showHsUser: "+headPhotoPath);
        Glide.with(getActivity())
                .load(headPhotoPath)
//                .error(R.drawable.ic_head_pic_default)
//                .crossFade(300)
//                .placeholder(R.drawable.ic_head_pic_default)
                .into(person_center_headphoto);
        //显示等级
        int rank = hsUser.getRank();
        if (rank >= 0 && rank < 200) {
            person_center_rank_img.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.rank0, null));
        } else if (rank < 500) {
            person_center_rank_img.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.rank200, null));
        } else if (rank < 1000) {
            person_center_rank_img.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.rank1000, null));
        } else if (rank < 5000) {
            person_center_rank_img.setImageDrawable(ResourcesCompat.getDrawable(getResources(), R.drawable.rank5000, null));
        }
        person_center_rank.setText(rank + "");
        //显示昵称和余额
        String nick = hsUser.getNick();
        if (nick == null || "".equals(nick)) {
            nick = hsUser.getName();
        }
        person_center_nick.setText(nick);
        person_center_balance.setText(hsUser.getMoney() + "");
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.person_header:
                startActivityForResult(new Intent(getActivity(), UserInfoActivity.class), Constants.ResultCode.USERINFO_ACTIVITY_UPDATE);
                break;
//            case R.id.person_about:
//                startActivity(new Intent(getActivity(), SwipeRefreshBaseActivity.class));
//                break;
            case R.id.userinfo_to_about:
                startActivity(new Intent(getActivity(), UserInfoAboutActivity.class));
                break;
            case R.id.userinfo_to_balance:
                startActivity(new Intent(getActivity(), UserinfoBalanceActivity.class));
                break;
            case R.id.userinfo_to_credit_card:
                startActivity(new Intent(getActivity(), UserInfoCreditCardActivity.class));
                break;
            case R.id.userinfo_to_favor:
                startActivity(new Intent(getActivity(), UserInfoFavorActivity.class));
                break;
            case R.id.userinfo_to_position:
                startActivity(new Intent(getActivity(), UserInfoPositionActivity.class));
                break;
            case R.id.userinfo_to_rank:
                startActivity(new Intent(getActivity(), UserInfoRankActivity.class));
                break;
            case R.id.userinfo_to_setting:
                startActivity(new Intent(getActivity(), UserInfoSettingActivity.class));
                break;
            case R.id.user_info_feed_back:
                startActivity(new Intent(getActivity(), UserInfoFeedBackActivity.class));
                break;
            default:
                break;
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (resultCode) {
            case Constants.ResultCode.USERINFO_ACTIVITY_UPDATE:
                showHsUser(UtilLogin.getHsUser(getActivity()));
                break;
            default:
                break;
        }
    }


    //EventBus 更新头像等信息 from UserInfoAcvity
    @Subscribe(threadMode = ThreadMode.MAIN, sticky = false, priority = 3)
    public void onMessageEventBackground(UpdateHeadPhotoEvent event) {
        if (Looper.myLooper() == Looper.getMainLooper()) {
            showHsUser(event.getHsUser());
            Log.v(TAG, " BACKGROUND id = " + Thread.currentThread().getId() + " -- " + event.getHeadPhoto());
        } else {
            Log.v(TAG, " BACKGROUND id = " + Thread.currentThread().getId() + " -- " + event.getHeadPhoto());
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        EventBusUtils.unregister(this);
    }
}


