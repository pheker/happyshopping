package cn.pheker.happyshopping.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.xys.libzxing.zxing.activity.CaptureActivity;
import com.yanxing.sortlistviewlibrary.CityListActivity;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.activities.CategoryProductsActivity;
import cn.pheker.happyshopping.activities.ProductDetailActivity;
import cn.pheker.happyshopping.activities.ShowQrcodeActivity;
import cn.pheker.happyshopping.activities.ShowQrcodeScanResultActivity;
import cn.pheker.happyshopping.adapter.MarketCardProductAdapter;
import cn.pheker.happyshopping.commons.BasePopupWindow;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.commons.UtilHsProducts;
import cn.pheker.happyshopping.commons.UtilPermission;
import cn.pheker.happyshopping.entities.HsProduct;
import cn.pheker.happyshopping.entities.Position;
import cn.pheker.happyshopping.entities.RetrieveDataByPage;
import cn.pheker.happyshopping.utils.UtilLocation;

import static android.app.Activity.RESULT_OK;

/**
 * Created by cn.pheker on 2017/4/7.
 * Mail to hkbxoic@gmail.com
 */

public class MarketFragment extends Fragment implements View.OnClickListener {

    private static final String TAG = "MarketFragment";

    private LinearLayout category;
    private GridView gl_latest,gl_hot;
    private TextView gl_latest_title,gl_hot_title,title_position;

    private ImageView title_qrcode;

    MarketCardProductAdapter latestAdapter,hotAdapter;
    private String[] category_titles = new String[]{
            "美食","服饰","商场","美妆","家居","母婴","酒店","电影","KTV",
            "休闲", "宠物", "运动", "骑行","美妆","医院","更多"
    };
    private List<HsProduct> hsProductsLatest;
    private List<HsProduct> hsProductsHot;


    public MarketFragment() {
    }

    Activity activity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_market, container, false);

        initView(view);
        initEvent(view);

        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.gl_latest_title:
                Intent latestIntent = new Intent(getActivity(), CategoryProductsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("orderby", "createtime");
                latestIntent.putExtras(bundle);
                startActivity(latestIntent);
                break;
            case R.id.gl_hot_title:
                Intent hotIntent = new Intent(getActivity(), CategoryProductsActivity.class);
                Bundle bundle2 = new Bundle();
                bundle2.putString("orderby", "paynum");
                hotIntent.putExtras(bundle2);
                startActivity(hotIntent);
                break;
            case R.id.titlebar_position:
//                startActivity(new Intent(activity, PositionActivity.class));

                Intent intent = new Intent(activity, CityListActivity.class);
                //当前城市
                intent.putExtra("city",title_position.getText().toString());
                startActivityForResult(intent,0xb2);
                break;
            case R.id.titlebar_qrcode:
                showQrcodePopupWindow();
                break;

            default:
                break;
        }
    }

    /**
     * 二维码弹出框
     */
    private void showQrcodePopupWindow() {

        class QrcodePopupWindow extends BasePopupWindow {

            public QrcodePopupWindow(Context context) {
                super(context);
            }
        }

        QrcodePopupWindow qrcodePopupWindow = new QrcodePopupWindow(activity);
        View view = LayoutInflater.from(activity).inflate(R.layout.popupwindow_qrcode, null);
        qrcodePopupWindow.setContentView(view);
        LinearLayout moneyOut = (LinearLayout) view.findViewById(R.id.qrcode_money_out);
        LinearLayout moneyIn = (LinearLayout) view.findViewById(R.id.qrcode_money_in);
        /*扫描二维码*/
        moneyOut.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(activity, "QrcodePopupWindow: " + ((TextView) ((LinearLayout) v).getChildAt(1)).getText(), Toast.LENGTH_SHORT).show();
                startActivityForResult(new Intent(activity, CaptureActivity.class), Constants.ResultCode.QRCODE_SCAN_RESULT);
            }
        });
        /*生成二维码*/
        moneyIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(activity, "QrcodePopupWindow: " + ((TextView) ((LinearLayout) v).getChildAt(1)).getText(), Toast.LENGTH_SHORT).show();
                startActivity(new Intent(activity, ShowQrcodeActivity.class));
            }
        });

        qrcodePopupWindow.showAsDropDown(title_qrcode);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case Constants.ResultCode.QRCODE_SCAN_RESULT://二维码扫描
                //获取二维码扫描结果,并启动ShowQrcodeScanResultActivity来显示
                String result = data.getExtras().getString("result");
                Toast.makeText(activity, result, Toast.LENGTH_SHORT).show();
                Log.i(TAG, "onActivityResult: " + result);
                Intent intent = new Intent(activity, ShowQrcodeScanResultActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("result", result);
                intent.putExtra("info", result);
                intent.putExtras(bundle);
                startActivity(intent);

                break;
            case 0xb2://定位
                String city = data.getExtras().getString("city");
                Log.i(TAG, "onActivityResult: " + city);
                title_position.setText(city);
                break;
            case RESULT_OK:
                Toast.makeText(activity, "返回", Toast.LENGTH_SHORT).show();
                break;
        }

    }

    /**
     * 初始化视图
     */
    private void initView(View view) {
        activity = getActivity();

        category = ((LinearLayout) view.findViewById(R.id.category));

        title_position = ((TextView) view.findViewById(R.id.titlebar_position));
        title_qrcode = ((ImageView) view.findViewById(R.id.titlebar_qrcode));

        gl_latest_title = (TextView) view.findViewById(R.id.gl_latest_title);
        gl_latest_title.setOnClickListener(this);
        gl_hot_title = (TextView) view.findViewById(R.id.gl_hot_title);
        gl_hot_title.setOnClickListener(this);
        gl_hot = (GridView) view.findViewById(R.id.gl_hot);
        gl_latest = (GridView) view.findViewById(R.id.gl_latest);
    }

    /**
     * 初始化事件
     *
     * @param view
     */
    private void initEvent(View view) {
        //事件
        title_position.setOnClickListener(this);
        title_qrcode.setOnClickListener(this);

        int categoryCount  = category.getChildCount();
        for (int i=0;i<categoryCount;i++) {
            final int index = i;
            LinearLayout type = (LinearLayout) category.getChildAt(i);
            type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(activity, CategoryProductsActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("category", category_titles[index]);
                    intent.putExtras(bundle);
                    startActivity(intent);
                }
            });
        }

        //获取所在城市
        UtilLocation.showLocation(getActivity(), new UtilLocation.PositionCallback() {
            @Override
            public void showPosition(final Position position) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        if(position!=null&&position.getResult()!=null) {
                            Log.i(TAG, "run: "+UtilGson.toJson(position.getResult()));
                            if (UtilPermission.isNetworkAvailable(activity)) {
                                title_position.setText(position.getResult().getAddressComponent().getCity());
                            }else{
                                Toast.makeText(activity, "请打开网络", Toast.LENGTH_SHORT).show();
                            }
                        }else{
                            title_position.setText("定位");
                        }
                    }

                });
            }
        });

        //最新
        Map<String, String> params = new HashMap<>();
        params.put("page",1+"");
        params.put("orderby", "createtime");
        UtilHsProducts.getRemoteDataWithCondition(activity, params, new UtilHsProducts.RemoteDataCallback() {
            @Override
            public void dealData(RetrieveDataByPage<List<HsProduct>> dataByPage) {
                hsProductsLatest = dataByPage.getData().subList(0, 6);
                latestAdapter = new MarketCardProductAdapter(activity, hsProductsLatest);
                gl_latest.setAdapter(latestAdapter);
                setListViewHeightBasedOnChildren(gl_latest);
                latestAdapter.notifyDataSetChanged();
            }
        });
        gl_latest.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("HsProduct", UtilGson.toJson( hsProductsLatest.get(position)));
                intent.setClass(activity, ProductDetailActivity.class);
                startActivity(intent);
            }
        });

        //最热
        params.put("orderby", "paynum");
        UtilHsProducts.getRemoteDataWithCondition(activity, params, new UtilHsProducts.RemoteDataCallback() {
            @Override
            public void dealData(RetrieveDataByPage<List<HsProduct>> dataByPage) {
                hsProductsHot = dataByPage.getData().subList(0, 6);
                hotAdapter = new MarketCardProductAdapter(activity, hsProductsHot);
                gl_hot.setAdapter(hotAdapter);
                setListViewHeightBasedOnChildren(gl_hot);
                hotAdapter.notifyDataSetChanged();
            }
        });
        gl_hot.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("HsProduct", UtilGson.toJson( hsProductsHot.get(position)));
                intent.setClass(activity, ProductDetailActivity.class);
                startActivity(intent);
            }
        });

    }





    /**
     * 动态设置 Gridview的高度
     * 调用此方法后，需要在调用notifyDataSetChanged()方法，实现界面刷新
     * @param listView
     */
    public static void setListViewHeightBasedOnChildren(GridView listView) {
        // 获取listview的adapter
        ListAdapter listAdapter = listView.getAdapter();
        if (listAdapter == null) {
            return;
        }
        // 固定列宽，有多少列
        int col = listView.getNumColumns();
        int totalHeight = 0;
        // i每次加4，相当于listAdapter.getCount()小于等于4时 循环一次，计算一次item的高度，
        // listAdapter.getCount()小于等于8时计算两次高度相加
        for (int i = 0; i < listAdapter.getCount(); i += col) {
            // 获取listview的每一个item
            View listItem = listAdapter.getView(i, null, listView);
            listItem.measure(0, 0);
            // 获取item的高度和
            totalHeight += listItem.getMeasuredHeight();
        }

        // 获取listview的布局参数
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        // 设置高度
        params.height = totalHeight+20;//todo 我自己又添加了20....
        // 设置margin
        ((ViewGroup.MarginLayoutParams) params).setMargins(10, 10, 10, 10);
        // 设置参数
        listView.setLayoutParams(params);
    }

}
