package cn.pheker.happyshopping.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.activities.ProductDetailActivity;
import cn.pheker.happyshopping.adapter.HsProductAdapter;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.entities.HsProduct;
import cn.pheker.happyshopping.entities.RetrieveDataByPage;
import cn.pheker.happyshopping.utils.DateUtil;
import okhttp3.Call;

public class GroupBuyingFragment extends Fragment {


    FragmentActivity activity;
    public GroupBuyingFragment() {
    }

    private PullToRefreshListView mPullToRefreshListView;
    private HsProductAdapter hsProductAdapter;
    private List<HsProduct> hsProducts = new LinkedList<>();

    public List<HsProduct> getHsProducts() {
        return hsProducts;
    }

    public void setHsProducts(List<HsProduct> hsProducts) {
        this.hsProducts = hsProducts;
    }

    int page = 1;//页数

    public void setPage(int page) {
        this.page = page;
    }

    public int getPage() {
        return page;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_groupbuying,container,false);

        activity = getActivity();

        AlexStatusBarUtils.setStatusColor(activity,
                ActivityCompat.getColor(activity,R.color.bg_login),0);

        initView(view);
        initEvent(view);

        return view;
    }


    protected void initView(View view) {
        // Set a listener to be invoked when the list should be refreshed.
        mPullToRefreshListView = (PullToRefreshListView) view.findViewById(R.id.pull_to_refresh_listview);

        /*刷新模式,下拉刷新+上拉加载*/
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);

        /*设置刷新显示的状态*/
        //设置下拉显示的日期和时间
        String label = DateUtil.getNowTimeString();

        /*设置下拉刷新状态*/
        ILoadingLayout refreshLabels = mPullToRefreshListView.getLoadingLayoutProxy(true, false);
        refreshLabels.setLastUpdatedLabel(label);
        refreshLabels.setPullLabel("快点下拉呀!");
        refreshLabels.setRefreshingLabel("正在刷新...");
        refreshLabels.setReleaseLabel("放开刷新");

        /*设置上拉加载状态*/
        ILoadingLayout loadingLabels = mPullToRefreshListView.getLoadingLayoutProxy(false, true);
        loadingLabels.setLastUpdatedLabel(label);
        loadingLabels.setPullLabel("快点上拉呀!");
        loadingLabels.setRefreshingLabel("正在加载...");
        loadingLabels.setReleaseLabel("放开加载");

    }

    protected void initEvent(View view) {
        hsProducts.add(new HsProduct("uuid"));      //防止为空时,pulltorefresh一直刷新/无法刷新的bug
        mPullToRefreshListView.setRefreshing();     //进入页面刷新数据
        /*初始化数据*/
        getRemoteData(page++, new GroupBuyingFragment.RemoteDataCallback() {
            @Override
            public void dealData(List<HsProduct> hsProducts) {
                setHsProducts(hsProducts);
                  /*设置适配器*/
                hsProductAdapter = new HsProductAdapter(activity, hsProducts);
                mPullToRefreshListView.setAdapter(hsProductAdapter);
                hsProductAdapter.notifyDataSetChanged();
            }
        });

        /*点击事件,有个bug position 需要-1才是真正正确的 */
        mPullToRefreshListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("HsProduct", UtilGson.toJson(getHsProducts().get(position - 1)));
                intent.setClass(activity, ProductDetailActivity.class);
                startActivity(intent);
            }
        });

        mPullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                page = 1;
                getRemoteData(page++);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                getRemoteData(page++);
            }
        });

    }

    private void getRemoteData(final int page) {
        Map<String, String> params = new HashMap<>();
        params.put("orderby", "group_purchase");//排序
        params.put("page", page + "");

        OkHttpUtils.get()
            .params(params)
            .url(Constants.Url.HSPRODUCT_BYPAGE_CONDITION_PATH)
            .build()
            .execute(new StringCallback() {
                @Override
                public void onError(Call call, Exception e, int id) {
                    Toast.makeText(activity, Constants.I18n.zh_cn.NET_ERROR, Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onResponse(final String response, int id) {

                    RetrieveDataByPage<List<HsProduct>> data = UtilGson.fromJson(response,
                            new TypeToken<RetrieveDataByPage<List<HsProduct>>>() {
                            }.getType());
                    int code = data.getCode();
                    if (code == 0) {
                        Toast.makeText(activity, data.getMsg(), Toast.LENGTH_SHORT).show();
                    } else if (code == 1) {//表明已获取到数据
//                                    Toast.makeText(activity, ""+data.getPage()+":"+data.getMaxPage(), Toast.LENGTH_SHORT).show();
                        int currPage = data.getPage();
                        if (currPage <= data.getMaxPage()) {
                            hsProducts.addAll(data.getData());
                            hsProductAdapter.notifyDataSetChanged();
                        } else {
                            ILoadingLayout loadingLabels = mPullToRefreshListView.getLoadingLayoutProxy(false, true);
                            loadingLabels.setLastUpdatedLabel(DateUtil.getNowTimeString());
                            String noMoreData = "没有更多数据了!";
                            loadingLabels.setPullLabel(noMoreData);
                            loadingLabels.setRefreshingLabel(noMoreData);
                            loadingLabels.setReleaseLabel("全部加载完毕");
                            Toast.makeText(activity, "没有更多数据了", Toast.LENGTH_SHORT).show();
                            setPage(data.getPage() - 1);
                        }

                        // 加载完成后停止刷新
                        mPullToRefreshListView.onRefreshComplete();
                    }

                }
            });//OkHttpUtils end
    }


    private void getRemoteData(int page, final GroupBuyingFragment.RemoteDataCallback rdcb) {
        Map<String, String> params = new HashMap<>();
        params.put("orderby", "group_purchase");//排序
        params.put("page", page + "");

        OkHttpUtils.get()
            .params(params)
            .url(Constants.Url.HSPRODUCT_BYPAGE_CONDITION_PATH)
            .build()
            .execute(new StringCallback() {
                @Override
                public void onError(Call call, Exception e, int id) {
                    Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onResponse(final String response, int id) {
                    RetrieveDataByPage<List<HsProduct>> data = UtilGson.fromJson(response,
                            new TypeToken<RetrieveDataByPage<List<HsProduct>>>() {}.getType());
                    int code = data.getCode();
                    if (code == 0) {
                        Toast.makeText(activity, data.getMsg(), Toast.LENGTH_SHORT).show();
                    } else if (code == 1) {
                        hsProducts = data.getData();
                        rdcb.dealData(hsProducts);
                        // 通知数据改变了
                        hsProductAdapter.notifyDataSetChanged();
                        // 加载完成后停止刷新
                        mPullToRefreshListView.onRefreshComplete();
                    }

                }
            });//OkHttpUtils end
    }

    interface RemoteDataCallback {
        void dealData(List<HsProduct> hsProducts);
    }

}


