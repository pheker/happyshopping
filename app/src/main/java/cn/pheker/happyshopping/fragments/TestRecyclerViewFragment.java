package cn.pheker.happyshopping.fragments;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bm.library.PhotoView;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.UtilHsProducts;
import cn.pheker.happyshopping.entities.HsProduct;
import cn.pheker.happyshopping.entities.RetrieveDataByPage;

public class TestRecyclerViewFragment extends Fragment {


    public TestRecyclerViewFragment() {
    }

    RecyclerView group_recyclerView;
    RecyclerView.Adapter myRecyclerViewAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_test_recycler_view, container, false);

        initView(view);
        initEvent(view);

        return view;
    }

    public void initView(View view) {
        group_recyclerView = (RecyclerView) view.findViewById(R.id.group_recyclerView);
        initRecyclerView(group_recyclerView);
    }

    public void initEvent(View view) {

    }


    /**
     * 初始化RecyclerView
     *
     * @param recyclerView 主控件
     */
    private void initRecyclerView(RecyclerView recyclerView) {
        recyclerView.setHasFixedSize(true); // 设置固定大小
        initRecyclerLayoutManager(recyclerView); // 初始化布局
        initRecyclerAdapter(recyclerView); // 初始化适配器
        initItemDecoration(recyclerView); // 初始化装饰
        initItemAnimator(recyclerView); // 初始化动画效果
    }

    private void initRecyclerLayoutManager(RecyclerView recyclerView) {
        // 错列网格布局
        recyclerView.setLayoutManager(new StaggeredGridLayoutManager(2,
                StaggeredGridLayoutManager.VERTICAL));
    }

    private void initRecyclerAdapter(RecyclerView recyclerView) {
        getRemoteData();
   }

    /*获取远程数据,并初始化适配器*/
    private void getRemoteData() {
        final Handler handler = new Handler(){
            @Override
            public void handleMessage(Message msg) {
                super.handleMessage(msg);
                if (msg.what == 1) {
                    RetrieveDataByPage<List<HsProduct>> dataByPage = (RetrieveDataByPage<List<HsProduct>>) msg.obj;
                    myRecyclerViewAdapter = new MyRecyclerViewAdapter(dataByPage.getData());
                    group_recyclerView.setAdapter(myRecyclerViewAdapter);
                    myRecyclerViewAdapter.notifyDataSetChanged();
                }
            }
        };
        final Map<String, String> params = new HashMap<>();
        params.put("page",1+"");
        params.put("orderby", "createtime");

        new Thread(new Runnable() {
            @Override
            public void run() {
                UtilHsProducts.getRemoteData(getActivity(), params, new UtilHsProducts.RemoteDataCallback() {
                    @Override
                    public void dealData(RetrieveDataByPage<List<HsProduct>> dataByPage) {
                        Message msg = handler.obtainMessage(1, dataByPage);
                        handler.sendMessage(msg);
                    }
                });
            }
        }).start();
    }

    private void initItemDecoration(RecyclerView recyclerView) {
        recyclerView.addItemDecoration(new MyItemDecoration(getActivity()));
    }


    public class MyRecyclerViewAdapter extends RecyclerView.Adapter<MyViewHolder> {

        private List<HsProduct> hsProducts;
        private List<Integer> mHeights;

        MyRecyclerViewAdapter(List<HsProduct> hsProducts) {
            if (hsProducts == null) {
                throw new IllegalArgumentException("DataModel must not be null");
            }
            this.hsProducts = hsProducts;
            mHeights = new ArrayList<>();
        }

        @Override
        public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_recycler_view, parent, false);
            return new MyViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(MyViewHolder holder, int position) {
            HsProduct hsProduct = hsProducts.get(position);
            // 随机高度, 模拟瀑布效果.
            if (mHeights.size() <= position) {
                mHeights.add((int) (200 + Math.random() * 200));
            }
            ViewGroup.LayoutParams lp = holder.getRecycler_item_photo().getLayoutParams();
            lp.height = mHeights.get(position);
            holder.getRecycler_item_photo().setLayoutParams(lp);
            Glide.with(getActivity()).load(hsProduct.getPicspath()).into(holder.getRecycler_item_photo());
        }

        @Override
        public int getItemCount() {
            return hsProducts.size();
        }

//        public void addData(int position) {
//            HsProduct model = new HsProduct();
//            model.setDateTime(getBeforeDay(new Date(), position));
//            model.setLabel("No. " + (int) (new Random().nextDouble() * 20.0f));
//
//            hsProducts.add(position, model);
//            notifyItemInserted(position);
//        }
//
//        public void removeData(int position) {
//            hsProducts.remove(position);
//            notifyItemRemoved(position);
//        }

    }

    public class MyItemDecoration extends RecyclerView.ItemDecoration {

        private final int[] ATTRS = new int[]{android.R.attr.listDivider};
        private Drawable mDivider;

        public MyItemDecoration(Context context) {
            final TypedArray array = context.obtainStyledAttributes(ATTRS);
            mDivider = array.getDrawable(0);
            array.recycle();
        }

        @Override
        public void onDraw(Canvas c, RecyclerView parent, RecyclerView.State state) {
            super.onDraw(c, parent, state);
            drawHorizontal(c, parent);
            drawVertical(c, parent);
        }

        // 水平线
        public void drawHorizontal(Canvas c, RecyclerView parent) {

            final int childCount = parent.getChildCount();

            // 在每一个子控件的底部画线
            for (int i = 0; i < childCount; i++) {
                final View child = parent.getChildAt(i);

                final int left = child.getLeft() + child.getPaddingLeft();
                final int right = child.getWidth() + child.getLeft() - child.getPaddingRight();
                final int top = child.getBottom() - mDivider.getIntrinsicHeight() - child.getPaddingBottom();
                final int bottom = top + mDivider.getIntrinsicHeight();
                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }

        // 竖直线
        public void drawVertical(Canvas c, RecyclerView parent) {

            final int childCount = parent.getChildCount();

            // 在每一个子控件的右侧画线
            for (int i = 0; i < childCount; i++) {
                final View child = parent.getChildAt(i);
                int right = child.getRight() - child.getPaddingRight();
                int left = right - mDivider.getIntrinsicWidth();
                final int top = child.getTop() + child.getPaddingTop();
                final int bottom = child.getTop() + child.getHeight() - child.getPaddingBottom();

                mDivider.setBounds(left, top, right, bottom);
                mDivider.draw(c);
            }
        }

        // Item之间的留白
        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
            outRect.set(0, 0, mDivider.getIntrinsicWidth(), mDivider.getIntrinsicHeight());
        }
    }

    private void initItemAnimator(RecyclerView recyclerView) {
        recyclerView.setItemAnimator(new DefaultItemAnimator()); // 默认动画
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        private TextView recycler_item_title,recycler_item_content,recycler_item_price,recycler_item_groupchase;
        private PhotoView recycler_item_photo;

        public MyViewHolder(View itemView) {
            super(itemView);
            recycler_item_title = (TextView) itemView.findViewById(R.id.recycler_item_title);
            recycler_item_content = (TextView) itemView.findViewById(R.id.recycler_item_content);
            recycler_item_price = (TextView) itemView.findViewById(R.id.recycler_item_price);
            recycler_item_groupchase = (TextView) itemView.findViewById(R.id.recycler_item_groupchase);
            recycler_item_photo = (PhotoView) itemView.findViewById(R.id.recycler_item_photo);
        }

        public TextView getRecycler_item_title() {
            return recycler_item_title;
        }

        public void setRecycler_item_title(TextView recycler_item_title) {
            this.recycler_item_title = recycler_item_title;
        }

        public TextView getRecycler_item_content() {
            return recycler_item_content;
        }

        public void setRecycler_item_content(TextView recycler_item_content) {
            this.recycler_item_content = recycler_item_content;
        }

        public TextView getRecycler_item_price() {
            return recycler_item_price;
        }

        public void setRecycler_item_price(TextView recycler_item_price) {
            this.recycler_item_price = recycler_item_price;
        }

        public TextView getRecycler_item_groupchase() {
            return recycler_item_groupchase;
        }

        public void setRecycler_item_groupchase(TextView recycler_item_groupchase) {
            this.recycler_item_groupchase = recycler_item_groupchase;
        }

        public PhotoView getRecycler_item_photo() {
            return recycler_item_photo;
        }

        public void setRecycler_item_photo(PhotoView recycler_item_photo) {
            this.recycler_item_photo = recycler_item_photo;
        }
    }

}


