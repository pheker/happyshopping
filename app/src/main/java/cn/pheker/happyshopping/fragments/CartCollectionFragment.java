package cn.pheker.happyshopping.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.activities.ProductDetailActivity;
import cn.pheker.happyshopping.adapter.HsProductAdapter;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.commons.UtilLogin;
import cn.pheker.happyshopping.entities.HsProduct;
import cn.pheker.happyshopping.entities.RetrieveDataByPage;
import cn.pheker.happyshopping.utils.DateUtil;
import okhttp3.Call;

/**
 * Created by cn.pheker on 2017/4/7.
 * Mail to hkbxoic@gmail.com
 * 好友公园,好友社区
 */

public class CartCollectionFragment extends Fragment {
    private static final String TAG = "CartCollectionFragment";

    public CartCollectionFragment() {
        super();
    }

    private FragmentActivity activity;

    private PullToRefreshListView mPullToRefreshListView;
    private HsProductAdapter hsProductAdapter;
    private List<HsProduct> hsProducts = new LinkedList<>();

    public List<HsProduct> getHsProducts() {
        return hsProducts;
    }

    public void setHsProducts(List<HsProduct> hsProducts) {
        this.hsProducts = hsProducts;
    }

    //页数
    public int getPage() {
        return CartFragment.pages_mycart_order_collection[2];
    }
    public void setPage(int page){
        CartFragment.pages_mycart_order_collection[2] = page;
    }

    String userId = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_cart_collection,container,false);

        initView(view);
        initEvent(view);

        return view;
    }

    public void initView(View view) {
        activity = getActivity();
        userId = UtilLogin.getLoginHsUserUuid(activity);

        // Set a listener to be invoked when the list should be refreshed.
        mPullToRefreshListView = (PullToRefreshListView) view.findViewById(R.id.pull_to_refresh_listview);

        /*刷新模式,下拉刷新+上拉加载*/
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);

        /*设置刷新显示的状态*/
        //设置下拉显示的日期和时间
        String label = DateUtil.getNowTimeString();

        /*设置下拉刷新状态*/
        ILoadingLayout refreshLabels = mPullToRefreshListView.getLoadingLayoutProxy(true, false);
        refreshLabels.setLastUpdatedLabel(label);
        refreshLabels.setPullLabel("快点下拉呀!");
        refreshLabels.setRefreshingLabel("正在刷新...");
        refreshLabels.setReleaseLabel("放开刷新");

        /*设置上拉加载状态*/
        ILoadingLayout loadingLabels = mPullToRefreshListView.getLoadingLayoutProxy(false, true);
        loadingLabels.setLastUpdatedLabel(label);
        loadingLabels.setPullLabel("快点上拉呀!");
        loadingLabels.setRefreshingLabel("正在加载...");
        loadingLabels.setReleaseLabel("放开加载");

        hsProducts.add(new HsProduct("uuid"));//防止为空时,pulltorefresh一直刷新/无法刷新的bug
        mPullToRefreshListView.setRefreshing();
//        getRemoteData(page++);

        /*初始化数据*/
        getRemoteData(1,new CartMycartFragment.RemoteDataCallback() {
            @Override
            public void dealData(List<HsProduct> hsProducts) {
                setHsProducts(hsProducts);
                  /*设置适配器*/
                hsProductAdapter = new HsProductAdapter(activity,hsProducts);
                mPullToRefreshListView.setAdapter(hsProductAdapter);
                hsProductAdapter.notifyDataSetChanged();
            }
        });

        /*点击事件,有个bug position 需要-1才是真正正确的 */
        mPullToRefreshListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("HsProduct", UtilGson.toJson(getHsProducts().get(position-1)));
                intent.setClass(activity, ProductDetailActivity.class);
                startActivity(intent);
            }
        });
    }

    public void initEvent(View view) {

        mPullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                getRemoteData(1);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                getRemoteData(1);
            }
        });
    }

    private void getRemoteData(final int page) {
        if("".equals(userId)){
            Toast.makeText(activity, "您还未登录", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("userId",userId);//最新
        params.put("type",1+"");
        params.put("page",page+"");

        OkHttpUtils.post()
                .params(params)
                .url(Constants.Url.HSCOLLECTION_RETRIEVEBYUSERIDTYPE)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onResponse(final String response, int id) {
                        RetrieveDataByPage<List<HsProduct>> data = UtilGson.fromJson(response,
                                new TypeToken<RetrieveDataByPage<List<HsProduct>>>() {}.getType());
                        int code = data.getCode();
                        if (code == 0) {
                            Toast.makeText(activity, data.getMsg(), Toast.LENGTH_SHORT).show();
                        } else if (code == 1) {//表明已获取到数据
//                                    Toast.makeText(activity, ""+data.getPage()+":"+data.getMaxPage(), Toast.LENGTH_SHORT).show();
                            int currPage = data.getPage();
                            if(currPage<=data.getMaxPage()) {
                                setPage(page+1);
                                if (currPage == 1) {
                                    hsProducts = data.getData();
                                }else if(currPage>1) {
                                    hsProducts.addAll(data.getData());
                                    Log.i(TAG, "onResponse: "+UtilGson.toJson(data.getData()));
                                    hsProductAdapter.notifyDataSetChanged();
                                }
                                if(data.getCount()==0){
                                    Toast.makeText(activity, "您还没有收藏过商品", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                ILoadingLayout loadingLabels = mPullToRefreshListView.getLoadingLayoutProxy(false, true);
                                loadingLabels.setLastUpdatedLabel(DateUtil.getNowTimeString());
                                String noMoreData = "没有更多数据了!";
                                loadingLabels.setPullLabel(noMoreData);
                                loadingLabels.setRefreshingLabel(noMoreData);
                                loadingLabels.setReleaseLabel("全部加载完毕");
                                Toast.makeText(activity, "没有更多数据了", Toast.LENGTH_SHORT).show();
                            }

                            // 加载完成后停止刷新
                            mPullToRefreshListView.onRefreshComplete();
                        }

                    }
                });//OkHttpUtils end
    }


    private void getRemoteData(final int page, final CartMycartFragment.RemoteDataCallback rdcb) {
        if("".equals(userId)){
            Toast.makeText(activity, "您还未登录", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("userId",userId);//最新
        params.put("type",1+"");
        params.put("page",page+"");

        OkHttpUtils.post()
                .params(params)
                .url(Constants.Url.HSCOLLECTION_RETRIEVEBYUSERIDTYPE)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onResponse(final String response, int id) {
                        RetrieveDataByPage<List<HsProduct>> data = UtilGson.fromJson(response,
                                new TypeToken<RetrieveDataByPage<List<HsProduct>>>() {}.getType());
                        int code = data.getCode();
                        if (code == 0) {
                            Toast.makeText(activity, data.getMsg(), Toast.LENGTH_SHORT).show();
                        } else if (code == 1) {
                            setPage(page+1);
                            hsProducts = data.getData();
                            Log.i(TAG, "onResponse: "+UtilGson.toJson(hsProducts));
                            rdcb.dealData(hsProducts);
                            // 通知数据改变了
                            hsProductAdapter.notifyDataSetChanged();
                            // 加载完成后停止刷新
                            mPullToRefreshListView.onRefreshComplete();
                            if(data.getCount()==0){
                                Toast.makeText(activity, "您还没有收藏过商品", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }
                });//OkHttpUtils end
    }

    interface RemoteDataCallback  {
        public void dealData(List<HsProduct> hsProducts);
    }



}
