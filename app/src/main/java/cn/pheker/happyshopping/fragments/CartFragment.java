package cn.pheker.happyshopping.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;

import java.util.ArrayList;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.adapter.SubFragmentAdapter;

public class CartFragment extends Fragment implements View.OnClickListener,
        ViewPager.OnPageChangeListener {

    public static int[] pages_mycart_order_collection = new int[]{1,1,1};

    public CartFragment() {}

    // 三个textview
    private TextView tab1Tv, tab2Tv, tab3Tv;
    // 指示器
    private ImageView cursorImg;
    // viewpager
    private ViewPager viewPager;
    // fragment对象集合
    private ArrayList<Fragment> fragmentsList;
    // 记录当前选中的tab的index
    private int currentIndex = 0;
    // 指示器的偏移量
    private int offset = 0;
    // 左margin
    private int leftMargin = 0;
    // 屏幕宽度
    private int screenWidth = 0;
    // 屏幕宽度的三分之一
    private int screen1_3;
    //
    private LinearLayout.LayoutParams lp;


    //
    private Activity mainActivity;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        AlexStatusBarUtils.setStatusColor(getActivity(),getResources().getColor(R.color.bg_login),0);
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        init(view);
        return view;
    }

    /**
     * 初始化操作
     */
    private void init(View view) {
        mainActivity = getActivity();

        DisplayMetrics dm = new DisplayMetrics();
        mainActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
        screenWidth = dm.widthPixels;
        screen1_3 = screenWidth / 3;

        cursorImg = (ImageView) view.findViewById(R.id.cursor);
        lp = (LinearLayout.LayoutParams) cursorImg.getLayoutParams();
        leftMargin = lp.leftMargin;

        tab1Tv = (TextView) view.findViewById(R.id.tab1_tv);
        tab2Tv = (TextView) view.findViewById(R.id.tab2_tv);
        tab3Tv = (TextView) view.findViewById(R.id.tab3_tv);
        //
        tab1Tv.setOnClickListener(this);
        tab2Tv.setOnClickListener(this);
        tab3Tv.setOnClickListener(this);

        initViewPager(view);
    }

    /**
     * 初始化viewpager
     */
    private void initViewPager(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.third_vp);
        fragmentsList = new ArrayList<>();
        Fragment fragment = new CartMycartFragment();
        fragmentsList.add(fragment);
        fragment = new CartOrderFragment();
        fragmentsList.add(fragment);
        fragment = new CartCollectionFragment();
        fragmentsList.add(fragment);

        // TODO: 2017/4/7 how to get getSupportFragmentManager
        viewPager.setAdapter(new SubFragmentAdapter(getChildFragmentManager(),fragmentsList));
        viewPager.setCurrentItem(0);
        viewPager.addOnPageChangeListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tab1_tv:
                if(pages_mycart_order_collection[0]>1) {
                    pages_mycart_order_collection[0] -= 1;
                }
                viewPager.setCurrentItem(0);
                break;
            case R.id.tab2_tv:
                if(pages_mycart_order_collection[1]>1) {
                    pages_mycart_order_collection[1] -= 1;
                }
                viewPager.setCurrentItem(1);
                break;
            case R.id.tab3_tv:
                if(pages_mycart_order_collection[2]>1) {
                    pages_mycart_order_collection[2] -= 1;
                }
                viewPager.setCurrentItem(2);
                break;
        }
    }

    @Override
    public void onPageScrollStateChanged(int arg0) {

    }

    @Override
    public void onPageScrolled(int position, float positionOffset,
                               int positionOffsetPixels) {
        if(pages_mycart_order_collection[position]>1) {
            pages_mycart_order_collection[position] -= 1;
        }
        offset = (screen1_3 - cursorImg.getLayoutParams().width) / 2;
//        Log.d("111", position + "--" + positionOffset + "--"
//                + positionOffsetPixels);
        final float scale = getResources().getDisplayMetrics().density;

//        if (position == 0) {// 0<->1
//            lp.leftMargin = (int) (positionOffsetPixels / 3) + offset;
//        } else if (position == 1) {// 1<->2
//            lp.leftMargin = (int) (positionOffsetPixels / 3) + screen1_3 +offset;
//        }
        lp.leftMargin = offset + screen1_3*position;
//        Log.d(TAG, "onPageScrolled: "+offset+"--"+lp.leftMargin+"--"+screen1_3+"--"+scale+"--"+positionOffsetPixels);
        cursorImg.setLayoutParams(lp);
        currentIndex = position;
    }

    @Override
    public void onPageSelected(int arg0) {
    }




}


