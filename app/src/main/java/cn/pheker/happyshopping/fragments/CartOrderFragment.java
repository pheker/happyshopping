package cn.pheker.happyshopping.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.adapter.BaseExpandableListAdapter;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.commons.UtilLogin;
import cn.pheker.happyshopping.entities.HsOrder;
import cn.pheker.happyshopping.entities.OrderGroup;
import cn.pheker.happyshopping.entities.RetrieveDataByPage;
import okhttp3.Call;

/**
 * Created by cn.pheker on 2017/4/7.
 * Mail to hkbxoic@gmail.com
 * 好友消息
 */

public class CartOrderFragment extends Fragment {
    private static final String TAG = "CartOrderFragment";
    public CartOrderFragment() {
        super();
    }
    
    private FragmentActivity activity;

    private ExpandableListView mExpandableListView;
    private ExpandableListAdapter mExpandableListAdapter;

    private List<HsOrder> mHsOrders = new ArrayList<>();
    private List<OrderGroup> mOrderGroups = new ArrayList<>();
    private List<List<HsOrder>> mOrderItems = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_cart_order,container,false);
        activity = getActivity();

        initView(view);
        initEvent(view);

        return view;
    }

    public void initView(View view) {
        activity = getActivity();
        // Set a listener to be invoked when the list should be refreshed.
        mExpandableListView = (ExpandableListView) view.findViewById(R.id.expandable_listview_orders);

        //获取数据

        getRemoteData(1, new RemoteDataCallback() {
            @Override
            public void dealData(List<HsOrder> hsOrders) {
                mHsOrders = hsOrders;
                Log.i(TAG, "dealData: "+UtilGson.toJson(mHsOrders));
                if (mHsOrders.size() == 0) {
                    Toast.makeText(activity, "没有订单信息", Toast.LENGTH_SHORT).show();
                }
                //处理数据
                HsOrderRawHolder hsOrderRawHolder = dealHsOrders();
                //适配器
                mExpandableListAdapter = new BaseExpandableListAdapter(activity,
                        hsOrderRawHolder.mOrderGroups, hsOrderRawHolder.mOrderItems);
                mExpandableListView.setAdapter(mExpandableListAdapter);
            }
        });

    }

    private class HsOrderRawHolder{

        private List<OrderGroup> mOrderGroups = new ArrayList<>();
        private List<List<HsOrder>> mOrderItems = new ArrayList<>();

        public HsOrderRawHolder(List<OrderGroup> mOrderGroups, List<List<HsOrder>> mOrderItems) {
            this.mOrderGroups = mOrderGroups;
            this.mOrderItems = mOrderItems;
        }
    }

    private HsOrderRawHolder dealHsOrders() {
        List<String> ordergroupstr = getOrderGroups();
        for (String orderGroup :
                ordergroupstr) {
            List<HsOrder> child = new ArrayList<>();
            float sumPrice = 0.0f;
            for (HsOrder hsOrder : mHsOrders) {
                if (orderGroup.equals(hsOrder.getOrderGroup())) {
                    sumPrice += hsOrder.getSumMoney();
                    child.add(hsOrder);
                }
            }
            OrderGroup mOrderGroup = new OrderGroup(orderGroup,sumPrice+"");
            mOrderGroups.add(mOrderGroup);
            mOrderItems.add(child);
        }
        return new HsOrderRawHolder(mOrderGroups, mOrderItems);

    }

    /**
     * 获取分组订单号
     * @return
     */
    private List<String> getOrderGroups() {
        List<String> ordergroupstr = new ArrayList<>();
        for (HsOrder hsOrder :mHsOrders) {
            String order_group = hsOrder.getOrderGroup();
            if (!ordergroupstr.contains(order_group)) {
                ordergroupstr.add(order_group);
            }
        }
        return ordergroupstr;
    }


    public void initEvent(View view) {

        mExpandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                return false;
            }
        });
        mExpandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
                return false;
            }
        });
    }



    private void getRemoteData(int page,final CartOrderFragment.RemoteDataCallback rdcb) {
        Map<String, String> params = new HashMap<>();
        params.put("userId", UtilLogin.getLoginHsUserUuid(activity));//最新
        params.put("page",page+"");

        OkHttpUtils.get()
                .params(params)
                .url(Constants.Url.HSORDER_RETRIEVEBYUSERID_PATH)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onResponse(final String response, int id) {
                        RetrieveDataByPage<List<HsOrder>> data = UtilGson.fromJson(response,
                                new TypeToken<RetrieveDataByPage<List<HsOrder>>>() {}.getType());
                        int code = data.getCode();
                        if (code == 0) {
                            Toast.makeText(activity, data.getMsg(), Toast.LENGTH_SHORT).show();
                        } else if (code == 1) {
                            rdcb.dealData(data.getData());
                        }

                    }
                });//OkHttpUtils end
    }

    interface RemoteDataCallback  {
        public void dealData(List<HsOrder> hsOrders);
    }



}
