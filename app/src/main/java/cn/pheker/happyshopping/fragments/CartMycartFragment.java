package cn.pheker.happyshopping.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.activities.ProductDetailActivity;
import cn.pheker.happyshopping.activities.ProductPayActivity;
import cn.pheker.happyshopping.adapter.HsProductCartAdapter;
import cn.pheker.happyshopping.adapter.HsProductCartAdapter.CheckboxClickCallback;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.commons.UtilLogin;
import cn.pheker.happyshopping.entities.HsOrder;
import cn.pheker.happyshopping.entities.HsProduct;
import cn.pheker.happyshopping.entities.RetrieveDataByPage;
import cn.pheker.happyshopping.utils.DateUtil;
import okhttp3.Call;

import static cn.pheker.happyshopping.adapter.HsProductCartAdapter.getIsSelected;

/**
 * Created by cn.pheker on 2017/4/7.
 * Mail to hkbxoic@gmail.com
 * 购物车 商品
 */

public class CartMycartFragment extends Fragment {

    private static final String TAG = "CartMycartFragment";
    private String userId;

    public CartMycartFragment() {
        super();
    }

    private FragmentActivity activity;

    private PullToRefreshListView mPullToRefreshListView;
    private HsProductCartAdapter hsProductCartAdapter;
    private List<HsProduct> hsProducts = new LinkedList<>();

    private TextView bt_selectall, bt_cancel,bt_deselectall;
    private int checkNum; // 记录选中的条目数量
    private TextView tv_show;// 用于显示选中的条目数量


    public List<HsProduct> getHsProducts() {
        return hsProducts;
    }

    public void setHsProducts(List<HsProduct> hsProducts) {
        this.hsProducts = hsProducts;
    }

    //页数
    public int getPage() {
        return CartFragment.pages_mycart_order_collection[0];
    }
    public void setPage(int page){
        CartFragment.pages_mycart_order_collection[0] = page;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_cart_mycart,container,false);

        initView(view);
        initEvent(view);

        initCheckbox(view);

        return view;
    }

    public void initView(View view) {
        userId = UtilLogin.getLoginHsUserUuid(activity);
        activity = getActivity();
        // Set a listener to be invoked when the list should be refreshed.
        mPullToRefreshListView = (PullToRefreshListView) view.findViewById(R.id.pull_to_refresh_listview);

        /*刷新模式,下拉刷新+上拉加载*/
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);

        /*设置刷新显示的状态*/
        //设置下拉显示的日期和时间
        String label = DateUtil.getNowTimeString();

        /*设置下拉刷新状态*/
        ILoadingLayout refreshLabels = mPullToRefreshListView.getLoadingLayoutProxy(true, false);
        refreshLabels.setLastUpdatedLabel(label);
        refreshLabels.setPullLabel("快点下拉呀!");
        refreshLabels.setRefreshingLabel("正在刷新...");
        refreshLabels.setReleaseLabel("放开刷新");

        /*设置上拉加载状态*/
        ILoadingLayout loadingLabels = mPullToRefreshListView.getLoadingLayoutProxy(false, true);
        loadingLabels.setLastUpdatedLabel(label);
        loadingLabels.setPullLabel("快点上拉呀!");
        loadingLabels.setRefreshingLabel("正在加载...");
        loadingLabels.setReleaseLabel("放开加载");

        hsProducts.add(new HsProduct("uuid"));//防止为空时,pulltorefresh一直刷新/无法刷新的bug
        mPullToRefreshListView.setRefreshing();
//        getRemoteData(page++);

        /*初始化数据*/
        getRemoteData(1,new RemoteDataCallback() {
            @Override
            public void dealData(List<HsProduct> hsProducts) {
                Log.i(TAG, "dealData: "+hsProducts);
                setHsProducts(hsProducts);
                  /*设置适配器*/
                hsProductCartAdapter = new HsProductCartAdapter(activity, hsProducts, mCheckboxClickback);
                mPullToRefreshListView.setAdapter(hsProductCartAdapter);
                hsProductCartAdapter.notifyDataSetChanged();
            }
        });

        /*点击事件,有个bug position 需要-1才是真正正确的 */
        mPullToRefreshListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("HsProduct", UtilGson.toJson(getHsProducts().get(position-1)));
                intent.setClass(activity, ProductDetailActivity.class);
                startActivity(intent);
            }
        });

        //立即购买
        TextView cart_purchase_now = (TextView) view.findViewById(R.id.cart_purchase_now);
        cart_purchase_now.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                List<HsOrder> hsOrders = new ArrayList<>();
                String orderGroupId = UUID.randomUUID().toString();
                String userId = UtilLogin.getLoginHsUserUuid(activity);
                int size = getIsSelected().size();
                for (int i = 0; i <size; i++) {
                    if(getIsSelected().get(i)==true){
                        hsOrders.add(
                            new HsOrder(UUID.randomUUID().toString(),
                                orderGroupId,"",0.0f,1,
                                userId,hsProducts.get(i).getUuid()
                            )
                        );
                    }
                }
                if (hsOrders.size() > 0) {
                    Intent intent = new Intent(activity,ProductPayActivity.class);
                    Bundle bundle = new Bundle();
                    bundle.putString("HsOrders",UtilGson.toJson(hsOrders));//订单entity
                    intent.putExtras(bundle);
                    startActivity(intent);
                }else{
                    Toast.makeText(activity, "您还未选择要购买的商品", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void initEvent(View view) {

        mPullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                getRemoteData(1);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                getRemoteData(getPage());
            }
        });
    }

    public void initCheckbox(View view) {

        bt_selectall = (TextView) view.findViewById(R.id.bt_selectall);
        bt_cancel = (TextView) view.findViewById(R.id.bt_cancleselectall);
        bt_deselectall = (TextView) view.findViewById(R.id.bt_deselectall);
        tv_show = (TextView) view.findViewById(R.id.tv);
        // 全选按钮的回调接口
        bt_selectall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 遍历list的长度，将HsProductCartAdapter中的map值全部设为true
                for (int i = 0; i < hsProducts.size(); i++) {
                    getIsSelected().put(i, true);
                }
                // 数量设为list的长度
                checkNum = hsProducts.size();
                // 刷新listview和TextView的显示
                dataChanged();
            }
        });
    
        // 反选按钮的回调接口
        bt_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 遍历list的长度，将已选的设为未选，未选的设为已选
                for (int i = 0; i < hsProducts.size(); i++) {
                    if (getIsSelected().get(i)) {
                        getIsSelected().put(i, false);
                        checkNum--;
                    } else {
                        getIsSelected().put(i, true);
                        checkNum++;
                    }
                }
                // 刷新listview和TextView的显示
                dataChanged();
            }
        });
    
        // 取消按钮的回调接口
        bt_deselectall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // 遍历list的长度，将已选的按钮设为未选
                for (int i = 0; i < hsProducts.size(); i++) {
                    if (getIsSelected().get(i)) {
                        getIsSelected().put(i, false);
                        checkNum--;// 数量减1
                    }
                }
                // 刷新listview和TextView的显示
                dataChanged();
            }
        });
    
//        // 绑定listView的监听器
//        lv.setOnItemClickListener(new OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
//            long arg3) {
//                // 取得ViewHolder对象，这样就省去了通过层层的findViewById去实例化我们需要的cb实例的步骤
//                ViewHolder holder = (ViewHolder) arg1.getTag();
//                // 改变CheckBox的状态
//                holder.cb.toggle();
//                // 将CheckBox的选中状况记录下来
//                HsProductCartAdapter.getIsSelected().put(arg2, holder.cb.isChecked());
//                // 调整选定条目
//                if (holder.cb.isChecked() == true) {
//                    checkNum++;
//                } else {
//                    checkNum--;
//                }
//                // 用TextView显示
//                tv_show.setText("已选中" + checkNum + "项");
//            }
//        });


    }


    // 刷新listview和TextView的显示
    private void dataChanged() {
        // 通知listView刷新
        hsProductCartAdapter.notifyDataSetChanged();
        // TextView显示最新的选中数目
        tv_show.setText("已选中" + checkNum + "项");
    };



    private void getRemoteData(final int page) {
        if("".equals(userId)){
            Toast.makeText(activity, "您还未登录", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("userId",userId);//最新
        params.put("type",2+"");
        params.put("page",page+"");

        OkHttpUtils.get()
                .params(params)
                .url(Constants.Url.HSCOLLECTION_RETRIEVEBYUSERIDTYPE)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onResponse(final String response, int id) {
                                RetrieveDataByPage<List<HsProduct>> data = UtilGson.fromJson(response,
                                        new TypeToken<RetrieveDataByPage<List<HsProduct>>>() {}.getType());
                                int code = data.getCode();
                                if (code == 0) {
                                    Toast.makeText(activity, data.getMsg(), Toast.LENGTH_SHORT).show();
                                } else if (code == 1) {//表明已获取到数据
//                                    Toast.makeText(activity, ""+data.getPage()+":"+data.getMaxPage(), Toast.LENGTH_SHORT).show();
                                    int currPage = data.getPage();
                                    if(currPage<=data.getMaxPage()) {
                                        setPage(page+1);
                                        if (currPage == 1) {
                                            hsProducts = data.getData();
                                        }else if(currPage>1) {
                                            hsProducts.addAll(data.getData());
                                        }
                                        hsProductCartAdapter.notifyDataSetChanged();
                                    }else{
                                        ILoadingLayout loadingLabels = mPullToRefreshListView.getLoadingLayoutProxy(false, true);
                                        loadingLabels.setLastUpdatedLabel(DateUtil.getNowTimeString());
                                        String noMoreData = "没有更多数据了!";
                                        loadingLabels.setPullLabel(noMoreData);
                                        loadingLabels.setRefreshingLabel(noMoreData);
                                        loadingLabels.setReleaseLabel("全部加载完毕");
                                        Toast.makeText(activity, "没有更多数据了", Toast.LENGTH_SHORT).show();
//                                        if(currPage>1){
//                                            setPage(currPage - 1);
//                                        }

                                    }


                                    // 加载完成后停止刷新
                                    mPullToRefreshListView.onRefreshComplete();
                                }

                            }
                });//OkHttpUtils end
    }


    private void getRemoteData(final int page, final RemoteDataCallback rdcb) {
        if("".equals(userId)){
            Toast.makeText(activity, "您还未登录", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("userId",userId);//最新
        params.put("type",2+"");
        params.put("page",page+"");

        OkHttpUtils.get()
            .params(params)
            .url(Constants.Url.HSCOLLECTION_RETRIEVEBYUSERIDTYPE)
            .build()
            .execute(new StringCallback() {
                @Override
                public void onError(Call call, Exception e, int id) {
                    Toast.makeText(activity, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                @Override
                public void onResponse(final String response, int id) {
                    RetrieveDataByPage<List<HsProduct>> data = UtilGson.fromJson(response,
                            new TypeToken<RetrieveDataByPage<List<HsProduct>>>() {}.getType());
                    int code = data.getCode();
                    if (code == 0) {
                        Toast.makeText(activity, data.getMsg(), Toast.LENGTH_SHORT).show();
                    } else if (code == 1) {
                        setPage(page+1);
                        hsProducts = data.getData();
                        rdcb.dealData(hsProducts);
                        // 通知数据改变了
                        hsProductCartAdapter.notifyDataSetChanged();
                        // 加载完成后停止刷新
                        mPullToRefreshListView.onRefreshComplete();
                    }

                }
            });//OkHttpUtils end
    }

    interface RemoteDataCallback  {
        public void dealData(List<HsProduct> hsProducts);
    }



    //复选框点击事件 监听回调函数
    private  CheckboxClickCallback mCheckboxClickback = new CheckboxClickCallback(){

        @Override
        public void onClick(View v) {
            int positionThis = (int)(v.getTag());
            boolean isSelectedThis = HsProductCartAdapter.getIsSelected().get(positionThis);
            if(isSelectedThis){
                HsProductCartAdapter.getIsSelected().put(positionThis, false);
                checkNum--;
            }else {
                HsProductCartAdapter.getIsSelected().put(positionThis, true);
                checkNum++;
            }
            dataChanged();
        }
    };



}
