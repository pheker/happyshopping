package cn.pheker.happyshopping.activities;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.adapter.ProductAdapter;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.UtilHsProducts;
import cn.pheker.happyshopping.entities.HsProduct;
import cn.pheker.happyshopping.entities.Product;
import cn.pheker.happyshopping.entities.RetrieveDataByPage;

public class SwipeRefreshBaseActivity extends BaseActivity implements AbsListView.OnScrollListener {


    private ImageView titlebar_back;
    private TextView titlebar_title;
    private ListView swipe_refresh_listview;
    private SwipeRefreshLayout swipe_refresh_layout;
    private List<Product> hsProducts;
    private ProductAdapter productAdapter;

    private int page;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_swipe_refresh_base);
        initView();
        initEvent();
    }

    @Override
    protected void initView() {
        titlebar_back = (ImageView) findViewById(R.id.titlebar_back);
        titlebar_title = (TextView) findViewById(R.id.titlebar_title);
        swipe_refresh_listview = (ListView) findViewById(R.id.swipe_refresh_listview);
        swipe_refresh_layout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
    }

    @Override
    protected void initEvent() {
        titlebar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        swipe_refresh_listview.setOnScrollListener(this);
        final ProductAdapter productAdapter = new ProductAdapter(SwipeRefreshBaseActivity.this, hsProducts);
        swipe_refresh_listview.setAdapter(productAdapter);

        /*下拉进度背景,下拉进度颜色*/
        swipe_refresh_layout.setProgressBackgroundColorSchemeResource(android.R.color.white);
        swipe_refresh_layout.setColorSchemeResources(android.R.color.holo_blue_bright,
                android.R.color.holo_green_light,android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        /*刷新监听*/
        final Map<String, String> params = new HashMap<>();
        params.put("page",page+"");
        params.put("orderby", "createtime");
        swipe_refresh_layout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                handler.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        //获取数据,通知数据改变,需要更新UI
                        UtilHsProducts.getRemoteData(SwipeRefreshBaseActivity.this, params, new UtilHsProducts.RemoteDataCallback() {
                            @Override
                            public void dealData(RetrieveDataByPage<List<HsProduct>> dataByPage) {
                                productAdapter.notifyDataSetChanged();

                                // 加载完数据设置为不刷新状态，将下拉进度收起来,不能写在外面
                                swipe_refresh_layout.setRefreshing(false);
                            }
                        });
                    }//--run end
                },1000);
            }//--onRefresh end
        });

    }

    @Override
    public void onScrollStateChanged(AbsListView view, int scrollState) {

    }

    @Override
    public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

    }


    Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what==1) {

            }
        }
    };
}
