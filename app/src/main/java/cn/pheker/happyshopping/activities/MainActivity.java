package cn.pheker.happyshopping.activities;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;

import java.util.ArrayList;
import java.util.List;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.UtilActivity;
import cn.pheker.happyshopping.commons.UtilLogin;
import cn.pheker.happyshopping.commons.UtilPermission;
import cn.pheker.happyshopping.fragments.CartFragment;
import cn.pheker.happyshopping.fragments.DiscountFragment;
import cn.pheker.happyshopping.fragments.GroupBuyingFragment;
import cn.pheker.happyshopping.fragments.MarketFragment;
import cn.pheker.happyshopping.fragments.PersonCenterFragment;

import static android.util.Log.d;


/**
 * Created by cn.pheker on 2017/4/5.
 * Mail to hkbxoic@gmail.com
 *  主页面
 * */
public class MainActivity extends BaseActivity implements View.OnClickListener{

    private static final String TAG = "MainActivity";

//tab
    int currentTabPosition = 0; //当前激活的tab索引
    int lastTabPosition = 0;    //上一个激活的tab索引

    private List<LinearLayout> tabList; //tab容器

    //父类LinearLayout id,子类ImageView id,子类TextView id
    private int[] parentResId = new int[]{
        R.id.person_market,R.id.person_cart,R.id.person_group_buying,R.id.person_discount,R.id.person_center
    };
    private int[] subImageResId = new int[]{
        R.id.person_market_image,R.id.person_cart_image,R.id.person_group_buying_image,
        R.id.person_discount_image,R.id.person_center_image
    };
    private int[] subTextResId = new int[]{
        R.id.person_market_text,R.id.person_cart_text,R.id.person_group_buying_text,
        R.id.person_discount_text,R.id.person_center_text
    };

    //tab 需要切换的图片
    private int[] pic = new int[]{
        R.drawable.market,R.drawable.cart,R.drawable.group_buying,R.drawable.discount,R.drawable.person_center
    };
    private int[] pic_selected = new int[]{
        R.drawable.market2,R.drawable.cart2,R.drawable.group_buying2,R.drawable.discount2,R.drawable.person_center2
    };

//fragment
    private List<Fragment> mFragments = new ArrayList<>();
    private FragmentManager mFragmentManager;
    private FragmentTransaction mFragmentTransaction;
    private String[] tags = new String[]{
            "MarketFragment","CartFragment","GroupBuyingFragment","DiscountFragment","PersonCenterFragment",
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Log.i(TAG, "onCreate: "+TAG);

        super.onCreate(savedInstanceState);
        //取消标题
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_main);

        AlexStatusBarUtils.setStatusColor(this,
                ActivityCompat.getColor(MainActivity.this,R.color.bg_login),0);

        initView();
        initEvent();

        int startFragmentIndext = 0;
        Bundle bundle = getIntent().getExtras();
        if (bundle!=null&&bundle.get("will_open")!=null) {
            startFragmentIndext = (Integer) bundle.get("will_open");
        }
        loadContentFragment(startFragmentIndext);//默认选中第一个tab
        UtilPermission.verifyStoragePermissions(MainActivity.this);//开启读写外部存储权限

        //关闭 引导,登录,注册界面
        UtilActivity.removeActivities(new String[]{"GuideActivity","LoginActivity","RegisterActiity"});
    }

    /**
     * 初始化视图
     */
    public void initView() {
        //tab
        if(tabList==null) tabList = new ArrayList<>();
        for (int i = 0; i < parentResId.length; i++) {
            tabList.add(0,(LinearLayout) findViewById(parentResId[i]));
        }

        mFragments.add(new MarketFragment());
        mFragments.add(new CartFragment());
        mFragments.add(new GroupBuyingFragment());
        mFragments.add(new DiscountFragment());
        mFragments.add(new PersonCenterFragment());

        mFragmentManager = getSupportFragmentManager();
    }

    /**
     * 初始化事件
     */
    public void initEvent() {
        for (LinearLayout ll : tabList) {
            ll.setOnClickListener(MainActivity.this);
        }
    }


    @Override
    public void onClick(View v) {
        d(TAG, "onClick() called with: v = [" + v + "]");
        switch (v.getId()) {
            case R.id.person_market:
                loadContentFragment(0);
                break;
            case R.id.person_cart:
                if(UtilLogin.hasLogin(MainActivity.this)) {
                    loadContentFragment(1);
                }else{
                    Toast.makeText(this, "您还未登录", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable(){
                        public void run() {
                            Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                            intent.putExtra("will_open", 1);
                            startActivity(intent);
                        }
                    }, 1000);
                }
                break;
            case R.id.person_group_buying:
                loadContentFragment(2);
                break;
            case R.id.person_discount:
                loadContentFragment(3);
                break;
            case R.id.person_center:
                if(UtilLogin.hasLogin(MainActivity.this)) {
                    loadContentFragment(4);
                }else{
                    Toast.makeText(this, "您还未登录", Toast.LENGTH_SHORT).show();
                    new Handler().postDelayed(new Runnable(){
                        public void run() {
                            Intent intent = new Intent(MainActivity.this,LoginActivity.class);
                            intent.putExtra("will_open", 4);
                            startActivity(intent);
                        }
                    }, 1000);
                }
                break;
            default:
                break;
        }
    }

    /**
     * 更改样式
     * @param curr   设置当前要激活的tab索引
     */
    private void changeStyle(int curr) {

        lastTabPosition = currentTabPosition;
        currentTabPosition = curr;

        //设置当前样式
        ((ImageView)findViewById(subImageResId[currentTabPosition]))
                .setImageResource(pic_selected[currentTabPosition]);
        ((TextView)findViewById(subTextResId[currentTabPosition]))
                .setTextColor(Color.parseColor("#0e656c"));

        if(currentTabPosition!=lastTabPosition){
            //设置上一个选中tab样式为默认
            ((ImageView)findViewById(subImageResId[lastTabPosition]))
                    .setImageResource(pic[lastTabPosition]);
            ((TextView)findViewById(subTextResId[lastTabPosition]))
                    .setTextColor(Color.parseColor("#999999"));
        }
    }

    private void loadContentFragment(int tabPosition) {

        if (tabPosition == mFragments.size() - 1) {
            AlexStatusBarUtils.setStatusColor(this, ActivityCompat.getColor(MainActivity.this,R.color.bg_light_greenblue),0);
        }else{
            AlexStatusBarUtils.setStatusColor(this, ActivityCompat.getColor(MainActivity.this,R.color.bg_login),0);
        }

        d(TAG, "loadContentFragment() called person center has been cliked ! " + tabPosition);
        changeStyle(tabPosition);

        mFragmentTransaction = mFragmentManager.beginTransaction();//每次都需要重新开启事务
        Fragment currFragment = mFragments.get(tabPosition);
        Fragment lastFragment = mFragments.get(lastTabPosition);
        if(tabPosition==lastTabPosition&&lastTabPosition==0) {
            if (!currFragment.isAdded()){
                mFragmentTransaction.add(R.id.fragments_container, currFragment, tags[tabPosition]).commit();
            }else {
                mFragmentTransaction.show(currFragment).commit();
            }
        }else {
            if (!currFragment.isAdded()) {
                mFragmentTransaction.hide(lastFragment)
                        .add(R.id.fragments_container, currFragment, tags[tabPosition]).commit();
            } else {
                mFragmentTransaction.hide(lastFragment).show(currFragment).commit();
            }
        }
    }

    /**
     * 状态检测 用于内存不足的时候保证fragment不会重叠
     * @param savedInstanceState
     */
    private void stateCheck(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            mFragmentManager = getSupportFragmentManager();
            FragmentTransaction fts = mFragmentManager.beginTransaction();
            MarketFragment marketFragment = new MarketFragment();
            fts.add(R.id.fragments_container,marketFragment,tags[0]);
            fts.commit();
        } else {
            mFragmentManager = getSupportFragmentManager();
            Fragment f0 = mFragmentManager.findFragmentByTag(tags[0]);
            Fragment f1 = mFragmentManager.findFragmentByTag(tags[1]);
            Fragment f2 = mFragmentManager.findFragmentByTag(tags[2]);
            Fragment f3 = mFragmentManager.findFragmentByTag(tags[3]);
            Fragment f4 = mFragmentManager.findFragmentByTag(tags[4]);
            mFragmentManager.beginTransaction().show(f0).hide(f1).hide(f2)
                    .hide(f3).hide(f4).commit();
        }
    }


    /**
     * 返回键 事件
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == event.KEYCODE_BACK&&event.getRepeatCount()==0){
            exit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }
}
