package cn.pheker.happyshopping.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.entities.Position;
import cn.pheker.happyshopping.utils.UtilLocation;

public class PositionActivity extends BaseActivity {

    TextView titlebar_title,position_show_position;
    ImageView titlebar_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_position);
        AlexStatusBarUtils.setStatusColor(PositionActivity.this,getResources().getColor(R.color.bg_login),0);
        initView();
        initEvent();
    }

    @Override
    protected void initView() {
        titlebar_title = (TextView) findViewById(R.id.titlebar_title);
        titlebar_title.setText("定位");
        titlebar_back = (ImageView) findViewById(R.id.titlebar_back);
        position_show_position = (TextView) findViewById(R.id.position_show_position);



    }

    @Override
    protected void initEvent() {
        titlebar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        /**
         * 获取定位信息
         */
        UtilLocation.showLocation(this, new UtilLocation.PositionCallback() {
            @Override
            public void showPosition(final Position position) {
                runOnUiThread(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        position_show_position.setText(position.getResult().getFormatted_address());
                    }

                });
            }
        });
    }





}
