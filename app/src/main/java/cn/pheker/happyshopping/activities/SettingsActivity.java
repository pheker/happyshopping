package cn.pheker.happyshopping.activities;

import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.UtilSP;

public class SettingsActivity extends BaseActivity implements View.OnClickListener {

    Switch settings_clear_cache;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
    }

    @Override
    protected void initView() {
        settings_clear_cache = (Switch) findViewById(R.id.settings_clear_cache);

    }


    @Override
    protected void initEvent() {
        settings_clear_cache.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    UtilSP.clearData(SettingsActivity.this);
                    Toast.makeText(SettingsActivity.this, "缓存已清空", Toast.LENGTH_SHORT).show();
                }else{

                }
            }
        });

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            default:
                break;
        }
    }
}
