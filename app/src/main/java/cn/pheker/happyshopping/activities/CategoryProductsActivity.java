package cn.pheker.happyshopping.activities;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.adapter.HsProductAdapter;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.entities.HsProduct;
import cn.pheker.happyshopping.entities.RetrieveDataByPage;
import cn.pheker.happyshopping.utils.DateUtil;
import okhttp3.Call;

public class CategoryProductsActivity extends BaseActivity {

    private ImageView titlebar_back;
    private TextView titlebar_title;

    private PullToRefreshListView mPullToRefreshListView;
    private HsProductAdapter hsProductAdapter;
    private List<HsProduct> hsProducts = new ArrayList<>();

    public List<HsProduct> getHsProducts() {
        return hsProducts;
    }

    public void setHsProducts(List<HsProduct> hsProducts) {
        this.hsProducts = hsProducts;
    }

    int page = 1;//页数

    public void setPage(int page) {
        this.page = page;
    }
    public int getPage(){
        return this.page;
    }


    //查询字段
    private String category;
    private String orderby;
    Map<String, String> params = new HashMap<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category_products);
        Bundle bundle = getIntent().getExtras();
        category = bundle.getString("category");
        orderby = bundle.getString("orderby");
        params.put("category", category==null?"":category); //分类
        params.put("orderby", orderby==null?"":category);//排序
        Log.i(TAG, "onCreate: "+category+"--"+orderby);

        AlexStatusBarUtils.setStatusColor(this,
                ActivityCompat.getColor(CategoryProductsActivity.this,R.color.bg_login),0);

        initView();

        initEvent();

    }


    @Override
    protected void initView() {
        titlebar_back = (ImageView) findViewById(R.id.titlebar_back);
        titlebar_title = (TextView) findViewById(R.id.titlebar_title);
        //动态设置标题
        if (category != null && !"".equals(category)) {
            titlebar_title.setText(category);
        }
        if (orderby != null && !"".equals(orderby)) {
            if (orderby.equals("createtime")) {
                titlebar_title.setText("最新商品");
            } else if (orderby.equals("paynum")) {
                titlebar_title.setText("最热商品");
            }
        }
        titlebar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // Set a listener to be invoked when the list should be refreshed.
        mPullToRefreshListView = (PullToRefreshListView) findViewById(R.id.product_ptr_listview);

        /*刷新模式,下拉刷新+上拉加载*/
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);

        /*设置刷新显示的状态*/
        //设置下拉显示的日期和时间
        String label = DateUtil.getNowTimeString();

        /*设置下拉刷新状态*/
        ILoadingLayout refreshLabels = mPullToRefreshListView.getLoadingLayoutProxy(true, false);
        refreshLabels.setLastUpdatedLabel(label);
        refreshLabels.setPullLabel("快点下拉呀!");
        refreshLabels.setRefreshingLabel("正在刷新...");
        refreshLabels.setReleaseLabel("放开刷新");

        /*设置上拉加载状态*/
        ILoadingLayout loadingLabels = mPullToRefreshListView.getLoadingLayoutProxy(false, true);
        loadingLabels.setLastUpdatedLabel(label);
        loadingLabels.setPullLabel("快点上拉呀!");
        loadingLabels.setRefreshingLabel("正在加载...");
        loadingLabels.setReleaseLabel("放开加载");

    }

    @Override
    protected void initEvent() {
        hsProducts.add(new HsProduct("uuid"));      //防止为空时,pulltorefresh一直刷新/无法刷新的bug
        mPullToRefreshListView.setRefreshing();     //进入页面刷新数据
        /*初始化数据*/
        getRemoteData(page++, new CategoryProductsActivity.RemoteDataCallback() {
            @Override
            public void dealData(List<HsProduct> hsProducts) {

                setHsProducts(hsProducts);
                  /*设置适配器*/
                hsProductAdapter = new HsProductAdapter(CategoryProductsActivity.this, hsProducts);
                mPullToRefreshListView.setAdapter(hsProductAdapter);
                hsProductAdapter.notifyDataSetChanged();
            }
        });

        /*点击事件,有个bug position 需要-1才是真正正确的 */
        mPullToRefreshListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("HsProduct", UtilGson.toJson(getHsProducts().get(position - 1)));
                intent.setClass(CategoryProductsActivity.this, ProductDetailActivity.class);
                startActivity(intent);
            }
        });

        mPullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                getRemoteData(1);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                getRemoteData(page);
            }
        });

    }

    private void getRemoteData(final int page) {
        params.put("page",page+"");
        OkHttpUtils.get()
            .params(params)
            .url(Constants.Url.HSPRODUCT_BYPAGE_CONDITION_PATH)
            .build()
            .execute(new StringCallback() {
                @Override
                public void onError(Call call, Exception e, int id) {
                    Toast.makeText(CategoryProductsActivity.this, Constants.I18n.zh_cn.NET_ERROR, Toast.LENGTH_SHORT).show();
                }
                @Override
                public void onResponse(final String response, int id) {
                    RetrieveDataByPage<List<HsProduct>> data = UtilGson.fromJson(response,
                            new TypeToken<RetrieveDataByPage<List<HsProduct>>>() {}.getType());
                    int code = data.getCode();
                    if (code == 0) {
                        Toast.makeText(CategoryProductsActivity.this, data.getMsg(), Toast.LENGTH_SHORT).show();
                    } else if (code == 1) {//表明已获取到数据
                        int currPage = data.getPage();
                        if (currPage <= data.getMaxPage()) {
                            Log.i(TAG, "onResponse: "+currPage);
                            if (currPage == 1) {
                                hsProducts = data.getData();
                                if (hsProducts.size()==0){
                                    Toast.makeText(CategoryProductsActivity.this, "没有查询到数据", Toast.LENGTH_SHORT).show();
                                }else{
                                    setPage(page+1);
                                }
                            }else {
                                hsProducts.addAll(data.getData());
                                setPage(getPage()+1);
                            }
                            hsProductAdapter.notifyDataSetChanged();
                        } else {
                            ILoadingLayout loadingLabels = mPullToRefreshListView.getLoadingLayoutProxy(false, true);
                            loadingLabels.setLastUpdatedLabel(DateUtil.getNowTimeString());
                            String noMoreData = "没有更多数据了!";
                            loadingLabels.setPullLabel(noMoreData);
                            loadingLabels.setRefreshingLabel(noMoreData);
                            loadingLabels.setReleaseLabel("全部加载完毕");
                            Toast.makeText(CategoryProductsActivity.this, "没有更多数据了", Toast.LENGTH_SHORT).show();
                            setPage(data.getPage() - 1);
                        }

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                // 加载完成后停止刷新
                                mPullToRefreshListView.onRefreshComplete();
                            }
                        },1000);

                    }

                }
            });//OkHttpUtils end
    }


    private void getRemoteData(int page, final CategoryProductsActivity.RemoteDataCallback rdcb) {
        params.put("page",page+"");
        OkHttpUtils.get()
            .params(params)
            .url(Constants.Url.HSPRODUCT_BYPAGE_CONDITION_PATH)
            .build()
            .execute(new StringCallback() {
                @Override
                public void onError(Call call, Exception e, int id) {
                    Toast.makeText(CategoryProductsActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                @Override
                public void onResponse(final String response, int id) {
                    RetrieveDataByPage<List<HsProduct>> data = UtilGson.fromJson(response,
                            new TypeToken<RetrieveDataByPage<List<HsProduct>>>() {}.getType());
                    int code = data.getCode();
                    if (code == 0) {
                        Toast.makeText(CategoryProductsActivity.this, data.getMsg(), Toast.LENGTH_SHORT).show();
                    } else if (code == 1) {
                        hsProducts = data.getData();
                        if (hsProducts.size()==0){
                            Toast.makeText(CategoryProductsActivity.this, "没有查询到数据", Toast.LENGTH_SHORT).show();
                        }
                        rdcb.dealData(hsProducts);
                        // 通知数据改变了
                        hsProductAdapter.notifyDataSetChanged();
                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                            // 加载完成后停止刷新
                            mPullToRefreshListView.onRefreshComplete();
                            }
                        },1000);
                    }

                }
            });//OkHttpUtils end
    }

    interface RemoteDataCallback {
        void dealData(List<HsProduct> hsProducts);
    }


}
