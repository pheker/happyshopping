package cn.pheker.happyshopping.activities;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.actionsheet.ActionSheet;
import com.bumptech.glide.Glide;
import com.google.gson.reflect.TypeToken;
import com.mylhyl.acp.Acp;
import com.mylhyl.acp.AcpListener;
import com.mylhyl.acp.AcpOptions;
import com.squareup.timessquare.CalendarPickerView;
import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;
import com.yanxing.dialog.PhotoParam;
import com.yanxing.dialog.SelectPhotoActivity;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.BasePopupWindow;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.EventBusUtils;
import cn.pheker.happyshopping.commons.UtilActivity;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.commons.UtilImage;
import cn.pheker.happyshopping.commons.UtilLogin;
import cn.pheker.happyshopping.commons.UtilReflect;
import cn.pheker.happyshopping.entities.HsUser;
import cn.pheker.happyshopping.entities.ReturnResponse;
import cn.pheker.happyshopping.entities.ReturnResponseWithData;
import cn.pheker.happyshopping.entities.UpdateHeadPhotoEvent;
import cn.pheker.happyshopping.utils.DateUtil;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Call;

import static cn.pheker.happyshopping.commons.Constants.TakePhoto.CROP_PHOTO;
import static cn.pheker.happyshopping.commons.UtilGson.fromJson;
import static cn.pheker.happyshopping.commons.UtilLogin.getHsUser;
import static com.zhy.http.okhttp.OkHttpUtils.post;

public class UserInfoActivity extends BaseActivity implements OnClickListener{

    /*工具栏*/
    ImageView titlebar_back;
    TextView titlebar_title, titlebar_settings;
    /*头部区域*/
    CircleImageView user_info_headphoto;
    TextView user_info_nick;
    /*内容区域*/
    TextView user_info_name,user_info_sex,user_info_birthday,user_info_phone,user_info_creditcard,
            user_info_email,user_info_degree,user_info_major,user_info_address;
    /*退出按钮*/
    Button exit;
    /*用户信息对象*/
    static HsUser hsUser;

    /*拍照获取到的图片要保存的路径*/
    private String imgSavePath;

    private String headPhotoName;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info);
        AlexStatusBarUtils.setStatusColor(this, ActivityCompat.getColor(UserInfoActivity.this,R.color.bg_light_greenblue),0);



        initView();
        initEvent();
    }

    @Override
    protected void initView() {
        titlebar_back = (ImageView) findViewById(R.id.titlebar_back);
        titlebar_title = (TextView) findViewById((R.id.titlebar_title));
        titlebar_settings = (TextView) findViewById((R.id.titlebar_settings));

        user_info_headphoto = ((CircleImageView) findViewById(R.id.user_info_headphoto));
        user_info_nick = (TextView) findViewById(R.id.user_info_nick);
        user_info_name = (TextView) findViewById(R.id.user_info_name);
        user_info_sex = (TextView) findViewById(R.id.user_info_sex);
        user_info_birthday = (TextView) findViewById(R.id.user_info_birthday);
        user_info_phone = (TextView) findViewById(R.id.user_info_phone);
        user_info_creditcard = (TextView) findViewById(R.id.user_info_creditcard);
        user_info_email = (TextView) findViewById(R.id.user_info_email);
        user_info_degree = (TextView) findViewById(R.id.user_info_degree);
        user_info_major = (TextView) findViewById(R.id.user_info_major);
        user_info_address = (TextView) findViewById(R.id.user_info_address);


        exit = (Button) findViewById(R.id.exit);
        hsUser = getHsUser(UserInfoActivity.this);
        Log.i(TAG, "initView: "+UtilGson.toJson(hsUser));
    }

    @Override
    protected void initEvent() {


        //加载,显示头像
        String headPhotoPath = hsUser.getHeadphotopath();
        headPhotoPath = UtilLogin.dealPicPath(headPhotoPath);
        if (headPhotoPath == null || "".equals(headPhotoPath)) {
            user_info_headphoto.setImageDrawable(ResourcesCompat.getDrawable(getResources(),R.drawable.ic_head_pic_default,null));
        }else{
            Glide.with(UserInfoActivity.this)
                .load(headPhotoPath)
//                .placeholder(R.drawable.ic_head_pic_default)
//                .error(R.drawable.ic_head_pic_default)
                .into(user_info_headphoto);
        }

        user_info_nick.setText(hsUser.getNick());
        user_info_name.setText(hsUser.getName());
        user_info_sex.setText(hsUser.getSex());
        user_info_birthday.setText(hsUser.getBirthday());
        user_info_phone.setText(hsUser.getPhone());
        user_info_creditcard.setText(hsUser.getCreditcard());
        user_info_degree.setText(hsUser.getDegree());
        user_info_major.setText(hsUser.getMajor());
        user_info_address.setText(hsUser.getAddress());


        titlebar_back.setOnClickListener(this);
        titlebar_settings.setOnClickListener(this);
        user_info_headphoto.setOnClickListener(this);
        user_info_nick.setOnClickListener(this);
        user_info_name.setOnClickListener(this);
        user_info_sex.setOnClickListener(this);
        user_info_birthday.setOnClickListener(this);
        user_info_phone.setOnClickListener(this);
        user_info_creditcard.setOnClickListener(this);
        user_info_email.setOnClickListener(this);
        user_info_degree.setOnClickListener(this);
        user_info_major.setOnClickListener(this);
        user_info_address.setOnClickListener(this);

        exit.setOnClickListener(this);

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.titlebar_back:
                finish();
                break;
            case R.id.titlebar_settings:
                startActivity(new Intent(UserInfoActivity.this,SettingsActivity.class));
                break;

            case R.id.user_info_headphoto:
                Acp.getInstance(UserInfoActivity.this)
                    .request(
                            new AcpOptions.Builder().setPermissions(
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.CAMERA)
                            /*以下为自定义提示语、按钮文字
                            .setDeniedMessage()
                            .setDeniedCloseBtn()
                            .setDeniedSettingBtn()
                            .setRationalMessage()
                            .setRationalBtn()*/
                            .build(),
                            new AcpListener() {
                                @Override
                                public void onGranted() {
                                    //从相册选照片还是拍照
                                    Intent intent=new Intent(getApplicationContext(), SelectPhotoActivity.class);
                                    headPhotoName = System.currentTimeMillis() + ".png";
                                    PhotoParam photoParam=new PhotoParam();
                                    photoParam.setName(headPhotoName);
                                    photoParam.setPath(Constants.TakePhoto.SAVED_IMAGE_DIR_PATH);
                                    photoParam.setCut(true);
                                    photoParam.setOutputX(480);
                                    photoParam.setOutputY(480);
                                    Bundle bundle=new Bundle();
                                    bundle.putParcelable("photoParam",photoParam);
                                    intent.putExtras(bundle);
                                    startActivityForResult(intent, 0xc1);
                                }

                                @Override
                                public void onDenied(List<String> permissions) {
                                    Toast.makeText(UserInfoActivity.this,permissions.toString() + "权限拒绝", Toast.LENGTH_SHORT).show();
                                }
                            }
                    );


                break;
            case R.id.user_info_nick:
                showInputPopupWindow(UserInfoActivity.this,user_info_nick,"昵称",".{2,18}",
                        "setNick");
                break;
            case R.id.user_info_name:
                showInputPopupWindow(UserInfoActivity.this,user_info_name,"登录名",
                        Constants.Pattern.LETTER_DIGIT,"setName");
                break;
            case R.id.user_info_sex:
                showInputPopupWindow(UserInfoActivity.this,user_info_sex,"性别","男|女","setSex");
                break;
            case R.id.user_info_birthday://出生日期
                showCalendarPopupWindow(UserInfoActivity.this,titlebar_back);
                break;
            case R.id.user_info_phone:
                showInputPopupWindow(UserInfoActivity.this,user_info_phone,"手机号",
                        Constants.Pattern.PHONE,"setPhone");
                break;
            case R.id.user_info_creditcard:
                showInputPopupWindow(UserInfoActivity.this,user_info_birthday,"身份证号",
                        ".{2,18}","setCreditcard");
                break;
            case R.id.user_info_email:
                showInputPopupWindow(UserInfoActivity.this,user_info_birthday,"邮箱",
                        Constants.Pattern.EMAIL,"setEmail");
                break;
            case R.id.user_info_degree:
                showInputPopupWindow(UserInfoActivity.this,user_info_birthday,"学位",".{2,18}",
                        "setDegree");
                break;
            case R.id.user_info_major:
                showInputPopupWindow(UserInfoActivity.this,user_info_birthday,"专业",".{2,18}",
                        "setMajor");
                break;
            case R.id.user_info_address:
                showInputPopupWindow(UserInfoActivity.this,user_info_birthday,"收货地址",".{2,18}",
                        "setAdress");
                break;

            case R.id.exit://退出
                UtilLogin.setUnLogin(UserInfoActivity.this);
                UtilActivity.finishAll();
                break;
        }
    }


    /**
     * 从相册或拍照获取图片
     */
    public void takePhoto() {
        //actionsheet http://www.cnblogs.com/tanghuian/p/4548685.html
        ActionSheet.createBuilder(this, getSupportFragmentManager())
            .setCancelButtonTitle("Cancel")
            .setOtherButtonTitles("拍照", "从相册中选取")
            .setCancelableOnTouchOutside(true)
            .setListener(new ActionSheet.ActionSheetListener() {
                @Override
                public void onDismiss(ActionSheet actionSheet, boolean isCancel) {
                    Toast.makeText(UserInfoActivity.this, "isCancal:"+isCancel,
                            Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onOtherButtonClick(ActionSheet actionSheet, int index) {
                    Toast.makeText(UserInfoActivity.this, "index:"+index, Toast.LENGTH_SHORT).show();
                    String imgsFolder = Constants.TakePhoto.SAVED_IMAGE_DIR_PATH;
                    File dir = new File(imgsFolder);
                    if (!dir.exists()) {
                        dir.mkdirs();
                    }
                    imgSavePath = imgsFolder +"/"+ System.currentTimeMillis() + ".jpg";
                    if (index == 0) {
                        //拍照
                        String state = Environment.getExternalStorageState();
                        if (state.equals(Environment.MEDIA_MOUNTED)) {//sdcard是否存在
                            File imgFile = new File(imgSavePath);
                            if (!imgFile.exists()) {
                                try {
                                    imgFile.createNewFile();
                                } catch (IOException e) {
                                    e.printStackTrace();
                                    Toast.makeText(UserInfoActivity.this, "创建图片失败", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                            }
                            //将拍的照片直接存到sd上,避免压缩或无法获取
                            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                            intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imgFile));
                            //裁剪
//                                        Intent intent2 = UtilImage.cropRawPhoto(Uri.fromFile(imgFile),500,500);
                            startActivityForResult(intent,Constants.TakePhoto.CAMERA);
                        }else{
                            Toast.makeText(UserInfoActivity.this, "SDcard不可用", Toast.LENGTH_SHORT).show();
                        }
                    } else if (index == 1) {
                        //从相册获取图片
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.addCategory(Intent.CATEGORY_OPENABLE);
                        intent.setType("image/*");
                        startActivityForResult(intent,Constants.TakePhoto.GALLERY);
                    }
                }
            }).show();
    }

    /**
     * 显示弹出层,通过用户输入更新用户信息
     * @param context   上下文
     * @param anchor    弹出层显示锚点
     * @param pattern   用于验证用户输入规则
     * @param methodName    使用反射执行指定的方法
     */
    public void showInputPopupWindow(final Context context,
                                            final View anchor, String titleName,
                                            final String pattern,
                                            final String methodName) {
        class InputPopupWindow extends BasePopupWindow {

            public InputPopupWindow(Context context) {
                super(context);
            }
        }

        final InputPopupWindow popupWindow = new InputPopupWindow(context);
        View view = LayoutInflater.from(context).inflate(R.layout.popupwindow_userinfo_input, null);
        popupWindow.setContentView(view);
        final TextView name = (TextView) view.findViewById(R.id.userinfo_popupwindow_name);//输入标题
        name.setText(titleName);
        final TextView text = (TextView) view.findViewById(R.id.userinfo_popupwindow_text);//输入的文本
        TextView ok = (TextView) view.findViewById(R.id.userinfo_popupwindow_ok);   //确定按钮

        ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String inputText = text.getText().toString();
//                Toast.makeText(context, "showInputPopupWindow: "+inputText, Toast.LENGTH_SHORT).show();
                if(inputText!=null&&!"".equals(inputText)&&inputText.matches(pattern)) {
                    UtilReflect.invokeMethod(hsUser,methodName,new String[]{inputText});
                    post()
                            .addParams("entity", UtilGson.toJson(hsUser))
                            .url(Constants.Url.UPDATE_PATH)
                            .build()
                            .execute(new StringCallback() {
                                @Override
                                public void onError(Call call, Exception e, int id) {
                                    Toast.makeText(context, Constants.I18n.zh_cn.NET_ERROR, Toast.LENGTH_SHORT).show();
                                }

                                @Override
                                public void onResponse(String response, int id) {
                                    ReturnResponse returnResponse = fromJson(response,
                                            new TypeToken<ReturnResponse>() {}.getType());
                                    if(returnResponse.getCode()==1){
                                        UtilLogin.saveHsUser(context,hsUser.getUuid(),UtilGson.toJson(hsUser));
                                        Toast.makeText(context, "更新信息成功", Toast.LENGTH_SHORT).show();
                                        UserInfoActivity.this.finish();
                                    } else if (returnResponse.getCode() == 0) {
                                        Toast.makeText(context, "更新信息出错", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });

                    popupWindow.dismiss();
                }else{
                    Toast.makeText(context, "输入格式不正确", Toast.LENGTH_SHORT).show();

                }
            }
        });

        popupWindow.showAsDropDown(anchor);//弹出框定位锚点

    }

    /**
     * 显示弹出层,通过用户输入更新用户日期
     * @param context   上下文
     * @param anchor    弹出层显示锚点
     */
    public void showCalendarPopupWindow(final Context context,final View anchor) {
        class InputPopupWindow extends BasePopupWindow {
            public InputPopupWindow(Context context) {
                super(context);
            }
        }

        final InputPopupWindow popupWindow = new InputPopupWindow(context);


        View view = LayoutInflater.from(context).inflate(R.layout.popupwindow_calendar, null);
        final CalendarPickerView calendarPickerView = (CalendarPickerView) view.findViewById(R.id.calendar_view);
        // init(Date selectedDate, Date minDate, Date maxDate) {...}
        // selectedDate 当前选中日期
        // minDate 对早可选日期 （包含）
        // maxDate 最晚可选日期（不包含）
        // calender.init
        Calendar minDate = Calendar.getInstance();
        minDate.set(1900,1,1);
        Calendar maxDate = Calendar.getInstance();
        maxDate.add(Calendar.DAY_OF_MONTH,1);
        String birthday = user_info_birthday.getText().toString();
        Date defaultDate = new Date();
        if(birthday!=null&&!"".equals(birthday)){
            defaultDate = DateUtil.getDateFromStr(birthday, "yyyy-MM-dd");
        }
        final CalendarPickerView.FluentInitializer init = calendarPickerView.init(minDate.getTime(), maxDate.getTime(), Locale.SIMPLIFIED_CHINESE);
        init.withSelectedDate(defaultDate);
        popupWindow.setContentView(view);

        final TextView calendar_toyear = (TextView)view.findViewById(R.id.calendar_toyear);
        final TextView calendar_tomonth = (TextView)view.findViewById(R.id.calendar_tomonth);
        final TextView calendar_select = (TextView)view.findViewById(R.id.calendar_select);

        calendar_select.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                final int curryear = Calendar.getInstance().get(Calendar.YEAR);
                String toyear = calendar_toyear.getText().toString();
                String tomonth = calendar_tomonth.getText().toString();
                if (toyear == null || "".equals(toyear)) {
                    toyear = curryear+"";
                }
                if (tomonth == null || "".equals(tomonth)) {
                    tomonth = Calendar.getInstance().get(Calendar.YEAR)+"";
                }
                if(!toyear.matches("\\d+")||!tomonth.matches("\\d+")){
//                    Toast.makeText(context, "请输入正确的年月", Toast.LENGTH_SHORT).show();
//                    return;
                }
                int year = Integer.parseInt(toyear);
                int month = Integer.parseInt(tomonth);
                if(year>=1900&&year<=curryear&&month-1<12){
                    Calendar queryCalendar = Calendar.getInstance();
                    queryCalendar.set(year,month-1,1);
                    init.withSelectedDate(queryCalendar.getTime());
                }else{
//                    Toast.makeText(context, "请输入正确的年月", Toast.LENGTH_SHORT).show();
                }
            }
        });


        Button calendar_ok = (Button) view.findViewById(R.id.calendar_ok);
        calendar_ok.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Date selectedDate = calendarPickerView.getSelectedDate();
                final String birthday = new SimpleDateFormat("yyyy-MM-dd").format(selectedDate);
                hsUser.setBirthday(birthday);
                OkHttpUtils.post()
                        .addParams("entity", UtilGson.toJson(hsUser))
                        .url(Constants.Url.UPDATE_PATH)
                        .build()
                        .execute(new StringCallback() {
                            @Override
                            public void onError(Call call, Exception e, int id) {
                                Toast.makeText(context, Constants.I18n.zh_cn.NET_ERROR, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onResponse(String response, int id) {
                                ReturnResponse returnResponse = fromJson(response,
                                        new TypeToken<ReturnResponse>() {}.getType());
                                if(returnResponse.getCode()==1){
                                    UtilLogin.saveHsUser(context,hsUser.getUuid(),UtilGson.toJson(hsUser));
                                    Toast.makeText(context, "更新信息成功", Toast.LENGTH_SHORT).show();
                                    user_info_birthday.setText(birthday);
                                    UserInfoActivity.this.finish();
                                } else if (returnResponse.getCode() == 0) {
                                    Toast.makeText(context, "更新信息出错", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                popupWindow.dismiss();
            }
        });

        popupWindow.showAsDropDown(anchor);//弹出框定位锚点

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case Constants.TakePhoto.GALLERY:
                if(requestCode == RESULT_OK){

                }
                dealImage(data);
                break;
            case Constants.TakePhoto.CAMERA:
                if(requestCode == RESULT_OK){
                    Intent intent = UtilImage.startCropPhotoActivity(data.getData());
                    startActivityForResult(intent, CROP_PHOTO);
                }
                break;
            case Constants.TakePhoto.CROP_PHOTO:
                if(requestCode == RESULT_OK){
                    dealImage(data);
                }
                break;
            case 0xc1:
                final String imgPath = Constants.TakePhoto.SAVED_IMAGE_DIR_PATH+File.separator+headPhotoName;
                Log.i(TAG, "onActivityResult: "+imgPath);
                File file = new File(imgPath);
                OkHttpUtils.post()
                    .url(Constants.Url.UPLOAD_PICS)
                    .addParams("uuid",UtilLogin.getLoginHsUserUuid(UserInfoActivity.this))
                    .addFile("head_photo",file.getName(),file)
                    .build()
                    .execute(new CustomFilUploadCallback(){
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            Toast.makeText(UserInfoActivity.this, "网络异常", Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            ReturnResponseWithData<String> data = UtilGson.fromJson(response,
                            new TypeToken<ReturnResponseWithData<String>>(){}.getType());

                            int code = data.getCode();
                            if(code==0){
                                Toast.makeText(UserInfoActivity.this, data.getMsg(), Toast.LENGTH_SHORT).show();
                            }else if(code==1){
                                String picsPath = data.getData();
                                picsPath = picsPath.startsWith("http://") ? picsPath : Constants.Url.SERVER_ADDRESS + picsPath;
    //                            user_info_headphoto.setImageBitmap(BitmapFactory.decodeFile(imgPath));
                                Log.i(TAG, "onResponse: "+picsPath);
                                Glide.with(UserInfoActivity.this)
                                        .load(picsPath)
//                                        .placeholder(R.drawable.ic_head_pic_default)
//                                        .error(R.drawable.ic_head_pic_default)
                                        .into(user_info_headphoto);
                                new UpdateThread();//更新头像
                                HsUser hsUser = UtilLogin.getHsUser(UserInfoActivity.this);
                                hsUser.setHeadphotopath(picsPath);
                                UtilLogin.saveHsUser(UserInfoActivity.this,hsUser.getUuid(),hsUser);

                                //EventBus
                                EventBusUtils.post(new UpdateHeadPhotoEvent(hsUser,picsPath));
//                                //
//                                Intent intent = new Intent(UserInfoActivity.this, PersonCenterFragment.class);
//                                Bundle bundle = new Bundle();
//                                bundle.putString("UserInfoActivity","update");
//                                intent.putExtras(bundle);
//                                setResult(Constants.ResultCode.USERINFO_ACTIVITY_UPDATE,intent);
//                                finish();
                            }
                        }

                    });

                break;
            default:
                break;
        }
    }

    /**
     * 从Intent中获取图片,上传到服务器,并显示
     * @param intent
     */
    private void dealImage(Intent intent) {
        Toast.makeText(this, "正在处理中...", Toast.LENGTH_SHORT).show();
        Bitmap bitmap = null;
        File file = null;
        Uri uri = intent.getData();
        if(uri!=null){
            bitmap = BitmapFactory.decodeFile(uri.getPath());
            try {
                file = new File(new URI(uri.toString()));
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }else{
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                bitmap =  bundle.getParcelable("data");
                UtilImage.saveImage(bitmap, imgSavePath);
                file = new File(imgSavePath);
            }else{
                Toast.makeText(this, "找不到图片", Toast.LENGTH_SHORT).show();
            }
        }
        Log.i(TAG, "dealImage: "+uri.getPath());

        final Bitmap finalBitmap = bitmap;
        OkHttpUtils.post()
            .url(Constants.Url.UPLOAD_PICS)
            .addParams("uuid",UtilLogin.getLoginHsUserUuid(UserInfoActivity.this))
            .addFile("head_photo",file.getName(),file)
            .build()
            .execute(new CustomFilUploadCallback(){
                @Override
                public void inProgress(float progress, long total, int id) {
                    super.inProgress(progress, total, id);
                    Toast.makeText(UserInfoActivity.this, "已上传:"+((int)(progress*100))+"%", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onError(Call call, Exception e, int id) {
                    Toast.makeText(UserInfoActivity.this, "网络异常", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onResponse(String response, int id) {
                    System.out.print(response);
                    ReturnResponseWithData<String> data = UtilGson.fromJson(response.toString(),
                            new TypeToken<ReturnResponseWithData<String>>(){}.getType());

                    int code = data.getCode();
                    if(code==0){
                        Toast.makeText(UserInfoActivity.this, data.getMsg(), Toast.LENGTH_SHORT).show();
                    }else if(code==1){
                        String picsPath = data.getData();
                        Toast.makeText(UserInfoActivity.this, picsPath, Toast.LENGTH_SHORT).show();
                        Log.i(TAG, "onResponse: "+picsPath);
                        user_info_headphoto.setImageBitmap(finalBitmap);
                    }
                }

            });
    }

    public abstract class CustomFilUploadCallback extends  StringCallback{
        @Override
        public void inProgress(float progress, long total, int id) {
            super.inProgress(progress, total, id);
//            Toast.makeText(UserInfoActivity.this, "已上传:"+Math.round(progress*100)+"%", Toast.LENGTH_SHORT).show();
        }
    }


    class UpdateThread implements Runnable{
        public void run(){
            while (!Thread.currentThread().isInterrupted()){
                try{
                    Thread.sleep(100);
                }
                catch (InterruptedException e){
                    Thread.currentThread().interrupt();
                }
                //使用postInvalidate可以直接在线程中更新界面
                user_info_headphoto.postInvalidate();
            }
        }
    }











}
