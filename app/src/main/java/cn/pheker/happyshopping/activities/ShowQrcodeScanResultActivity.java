package cn.pheker.happyshopping.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;

public class ShowQrcodeScanResultActivity extends BaseActivity {

    ImageView titlebar_back;
    TextView scanqqrcode_result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_qrcode_scan_result);

        String info = getIntent().getStringExtra("info");
        Log.i(TAG, "onCreate: "+info);
        initView();
        initEvent();

    }


    @Override
    protected void initView() {
        titlebar_back = (ImageView) findViewById(R.id.titlebar_back);
        scanqqrcode_result = (TextView) findViewById(R.id.scanqqrcode_result);

    }

    @Override
    protected void initEvent() {
        //返回
        titlebar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        Bundle bundle = getIntent().getExtras();
        String result = bundle.getString("result");
        Log.d(TAG, "initEvent: "+result);
        scanqqrcode_result.setText(result);
    }
}
