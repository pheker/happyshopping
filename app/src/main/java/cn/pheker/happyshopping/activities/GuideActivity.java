package cn.pheker.happyshopping.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.adapter.GuidePagerAdapter;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.utils.DensityUtil;

/**
 * Created by cn.pheker on 2017/4/5.
 * Mail to hkbxoic@gmail.com
 *  引导页
 * */
public class GuideActivity extends BaseActivity implements View.OnClickListener{

    private static final String TAG = "GuideActivity";
    private ViewPager viewPager;
    private GuidePagerAdapter guidePagerAdapter;
    private List<View> views ;

    RelativeLayout guide_indicator;
    LinearLayout guide_background_indicator,guide_foreground_indicator;
    TextView circle_orange;         //会移动的小圆点
    Button goMainBtn;               //开始体验按钮

    /* 圆点 TextView */
    int leftMargin;                 //左边距 容器LinearLayout
    int separationDistance = 40;    //间距
    int dip = 15;                   //大小


    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        //取消标题
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //取消状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN ,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_guide);

        initView();

        initEvent();
    }

    /**
     * 初始化视图和布局
     */
    @Override
    protected void initView() {
        viewPager = (ViewPager) findViewById(R.id.view_pager);
        guide_indicator = ((RelativeLayout) findViewById(R.id.guide_indicator));
        guide_background_indicator = ((LinearLayout) findViewById(R.id.guide_background_indicator));
        guide_foreground_indicator = ((LinearLayout) findViewById(R.id.guide_foreground_indicator));
        goMainBtn = ((Button) findViewById(R.id.go_main_btn));

        if (guidePagerAdapter==null){
            guidePagerAdapter = new GuidePagerAdapter();
        }
        int[] resIds = guidePagerAdapter.getTitles();
        //设置背景图片和不会移动的背景小圆点
        for (int i = 0;i<resIds.length;i++ ) {
            // 设置点的大小 单位为px 设置屏幕适配
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    DensityUtil.dip2px(GuideActivity.this, dip),DensityUtil.dip2px(GuideActivity.this, dip));
            View circle_white = new TextView(this);
            circle_white.setBackgroundResource(R.drawable.circle_white);
            // 设置间隙,过滤第一个点
            if(i>0) {
                params.leftMargin = DensityUtil.dip2px(GuideActivity.this,separationDistance);
            }
            circle_white.setLayoutParams(params);
            guide_background_indicator.addView(circle_white,i);

            ImageView imageView = new ImageView(this);
            Glide.with(this).load(resIds[i]).into(imageView);
//            imageView.setImageResource(resIds[i]);
            imageView.setScaleType(ImageView.ScaleType.FIT_XY);
            guidePagerAdapter.getViews().add(imageView);
        }
        //设置会移动的前景小圆点
        circle_orange = new TextView(this);
        circle_orange.setBackgroundResource(R.drawable.circle_orange);
        guide_foreground_indicator.addView(circle_orange);


        views = guidePagerAdapter.getViews();
        viewPager.setAdapter(guidePagerAdapter);


        leftMargin = guide_background_indicator.getLeft();
    }

    /**
     * 初始化化事件
     */
    @Override
    protected void initEvent() {
        goMainBtn.setOnClickListener(this);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {//第几个页面,滑动偏移百分比,滑动偏移像素
                Log.d(TAG, "onPageScrolled() called with: position = [" + position + "], positionOffset = [" + positionOffset + "], positionOffsetPixels = [" + positionOffsetPixels + "]");

                // 计算circle_orange_id的左边距
                float left = (separationDistance+dip) * (position + positionOffset);
                Log.d(TAG, "leftMargin--left:  "+leftMargin+"--"+left);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        DensityUtil.dip2px(GuideActivity.this, dip),DensityUtil.dip2px(GuideActivity.this, dip));
//                params.setMargins(DensityUtil.dip2px(GuideActivity.this,left),0,0,0);//setMargins(left, top, right, bottom);
                params.leftMargin = DensityUtil.dip2px(GuideActivity.this,left);
                circle_orange.setLayoutParams(params);
//                circle_orange.requestLayout();
            }
            @Override
            public void onPageSelected(int position) {
                Log.d(TAG, "onPageSelected() called with: position = [" + position + "]");

                // fade_in
                AlphaAnimation fade_in = new AlphaAnimation(0f, 1f);
                fade_in.setDuration(1500);
                // fade_out
                AlphaAnimation fade_out = new AlphaAnimation(1f, 0f);
                fade_out.setDuration(1500);
                //最后一页
                if (position==guidePagerAdapter.getViews().size()-1){
                    goMainBtn.setVisibility(View.VISIBLE);
                    goMainBtn.startAnimation(fade_in);
                    goMainBtn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                        Intent intent = new Intent(GuideActivity.this, MainActivity.class);
                        startActivity(intent);
                        finish();
                        }
                    });
                }else{
                    goMainBtn.setVisibility(View.GONE);
                    guide_indicator.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrollStateChanged(int state) {}

        });

    }


    @Override
    public void onClick(View v) {
        switch ((v.getId())){
            case R.id.go_main_btn:
                startActivity( new Intent(GuideActivity.this, MainActivity.class));
                overridePendingTransition(R.anim.activity_left_in,R.anim.activity_rignt_out);
                finish();

            break;

            default:
            break;
        }
    }
}
