package cn.pheker.happyshopping.activities;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.UtilGson;
import io.reactivex.Observable;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Retrofit 学习
 * 通过retrofit方位restful api 获取数据并显示
 */
public class ProfileActivity extends BaseActivity {

    private static final String TAG = "ProfileActivity";

    private static final String ENDPOINT = "http://ip.taobao.com";
    private static final String IP = "60.205.177.176";
    TextView mTextView;
    ProgressBar mProgressBar;
    Button retrofit_btn;
    ImageView glide_img;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        initView();
        initEvent();
        myGlideLoader();
    }

    public void initView() {
        mTextView = ((TextView) findViewById(R.id.retrofit_tv));
        mProgressBar = ((ProgressBar) findViewById(R.id.retrofit_pb));
        retrofit_btn = ((Button) findViewById(R.id.retrofit_btn));
        glide_img = ((ImageView) findViewById(R.id.glide_img));

        retrofit_rxjava_tv = ((TextView) findViewById(R.id.retrofit_rxjava_tv));
        retrofit_rxjava_btn = ((Button) findViewById(R.id.retrofit_rxjava_btn));
    }

    private void myGlideLoader() {
        Glide.with(ProfileActivity.this)
            .load("http://imglf0.nosdn.127.net/img/b2ZLVlBpMUtRcUNPNFpJU1duN3NsSVMrd3QyTHU2RlFWT2w2UHRzQzZyYVVEcjNxTEZ5RlZBPT0.jpg?imageView&thumbnail=1680x0&quality=96&stripmeta=0&type=jpg")
            .thumbnail(0.2f)
//            .crossFade()
            .animate(R.anim.activity_left_in)
            .into(glide_img);
    }

    public void initEvent() {
        retrofit_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                fetch();
            }
        });
        retrofit_rxjava_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getIpByRetrofitRxjava();
            }
        });
    }

    private void fetch() {

        //1.创建retrofit实例
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ENDPOINT)
                .client(new OkHttpClient())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        //2.创建服务
        RequestService service = retrofit.create(RequestService.class);

        mProgressBar.setVisibility(View.VISIBLE);//显示进度条

        //3.调用请求方法,并得到Call实例
        Call<IpInfoResponse> ipInfoResponseCall = service.getIpInfo(IP);

        //4.使用Call实例,并执行同步或一部请求
            //同步
//        try {
//            IpInfoResponse body = ipInfoResponseCall.execute().body();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        //异步
        ipInfoResponseCall.enqueue(new Callback<IpInfoResponse>() {
            @Override
            public void onResponse(Call<IpInfoResponse> call, Response<IpInfoResponse> response) {
                IpInfoResponse ipInfoResponse = response.body();
                Log.d(TAG, "onResponse: "+ UtilGson.toJson(response));
                mProgressBar.setVisibility(View.GONE);
//                mTextView.setText(ipInfoResponse.data.country);
                mTextView.setText(UtilGson.toJson(ipInfoResponse));
            }

            @Override
            public void onFailure(Call<IpInfoResponse> call, Throwable t) {
                Log.d(TAG, "onFailure: "+t.getMessage());
                mProgressBar.setVisibility(View.GONE);
            }
        });
    }

    /**
     * 1.定义服务接口
     * http://ip.taobao.com/ipSearch.php?ipAddr=60.205.177.176
     */
    public interface RequestService{
        /* 可以添加一些Header,如下: */
//        @Headers({
//            "Accept: application/json",
//            "Content-Type: application/json"
//        })
        @GET("service/getIpInfo.php")
        Call<IpInfoResponse> getIpInfo(@Query("ip") String ip);
    }

    /**
     * 2.定义要接收的数据实体类
     */
    public class IpInfoResponse {
        public IpInfo data;

        public class IpInfo {
            public String country;
            public String country_id;
            public String area;
            public String area_id;
            public String ip;
        }

    }

//    private static OkHttpClient getNewClient(){
//        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
//        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
//        return new OkHttpClient.Builder()
//                .addInterceptor(new CustomInterceptor())
//                .addInterceptor(logging)
//                .connectTimeout(DEFAULT_TIMEOUT, TimeUnit.SECONDS)
//                .build();
//    }


    //下面 我们使用rxjava来处理数据

    TextView retrofit_rxjava_tv;
    Button retrofit_rxjava_btn;

    public interface   iIpInfoService{
        @GET("service/getIpInfo.php")
        Observable<IpInfoResponse> getIpInfo(@Query("ip") String ip);
    }


    //单例模式 获取retrofit实例
    public static class IpInfoUtil{
        private static Retrofit retrofit;

        public static Observable<IpInfoResponse> getIpInfoResponseObservable(String baseUrl){
            if (retrofit == null) {
                retrofit = new Retrofit.Builder()
                        .baseUrl(baseUrl)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())  //使用RxJava
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
            return retrofit.create(iIpInfoService.class)
                        .getIpInfo(IP);
        }

    }

    public void getIpByRetrofitRxjava(){
        mProgressBar.setVisibility(View.VISIBLE);
        Observable<IpInfoResponse> ipInfoResponseObservable = IpInfoUtil.getIpInfoResponseObservable(ENDPOINT);
        ipInfoResponseObservable.subscribeOn(Schedulers.io())   //请求线程
                .unsubscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())      //回调线程
                .subscribe(                                     //数据回调
                        new Observer<IpInfoResponse>() {
                            @Override
                            public void onSubscribe(Disposable d) {

                            }

                            @Override
                            public void onNext(IpInfoResponse ipInfoResponse) {
                                Log.d(TAG, "onNext: "+ UtilGson.toJson(ipInfoResponse));
                                mProgressBar.setVisibility(View.GONE);
                                retrofit_rxjava_tv.setText(UtilGson.toJson(ipInfoResponse));
                            }

                            @Override
                            public void onError(Throwable t) {
                                Log.d(TAG, "onFailure: "+t.getMessage());

                            }

                            @Override
                            public void onComplete() {

                            }
                        }
                );
    }




















}

