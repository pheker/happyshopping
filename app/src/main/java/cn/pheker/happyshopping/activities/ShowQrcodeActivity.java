package cn.pheker.happyshopping.activities;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;
import com.xys.libzxing.zxing.encoding.EncodingUtils;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.UtilLogin;
import cn.pheker.happyshopping.entities.HsUser;

public class ShowQrcodeActivity extends BaseActivity {

    TextView qrcode_info;
    ImageView qrcode_img,titlebar_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_qrcode);

        AlexStatusBarUtils.setStatusColor(this, ActivityCompat.getColor(ShowQrcodeActivity.this,R.color.bg_light_greenblue),0);

        initView();
        initEvent();



    }

    @Override
    protected void initView() {
        qrcode_img = (ImageView) findViewById(R.id.qrcode_img);
        titlebar_back = (ImageView) findViewById(R.id.titlebar_back);
        qrcode_info = (TextView) findViewById(R.id.qrcode_info);

    }

    @Override
    protected void initEvent() {

        HsUser hsUser = UtilLogin.getHsUser(ShowQrcodeActivity.this);
        String nick = hsUser.getNick();
        qrcode_info.setText(nick==null||"".equals(nick)?hsUser.getName():nick);
        Bitmap bitmap = EncodingUtils.createQRCode(hsUser.getUuid(), 500, 500,null);
        // 设置图片
        qrcode_img.setImageBitmap(bitmap);
        qrcode_img.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
//                Toast.makeText(ShowQrcodeActivity.this, "保存或分享个二维码", Toast.LENGTH_SHORT).show();
                showToast(ShowQrcodeActivity.this, "保存或分享二维码");
                return true;
            }
        });

//        if(hsUser==null){
//            hsUser = new HsUser();
//            hsUser.setUuid("cn.pheker.uuid");
//            hsUser.setName("");
//        }
//        qrcode_info.setText(hsUser.getName());
//
//        final HsUser finalHsUser = hsUser;
//        final Handler myQrcodeHandler = new Handler(new Handler.Callback() {
//            @Override
//            public boolean handleMessage(Message msg) {
//                if (0 == msg.what) {
//                    /**
//                     * 参数：1.文本 2 3.二维码的宽高 4.二维码中间的那个logo
//                     */
//                    Bitmap bitmap = EncodingUtils.createQRCode(finalHsUser.getUuid(), 500, 500, (Bitmap) msg.obj);
//                    // 设置图片
//                    qrcode_img.setImageBitmap(bitmap);
//                }
//                return true;
//            }
//        });
//
//        //子线程获取图片
//        new Thread(new Runnable() {
//            @Override
//            public void run() {
//                String headPhotoPaht = finalHsUser.getHeadphotopath();
//                Log.i(TAG, "run: "+headPhotoPaht);
//                OkHttpUtils
//                    .get()
//                    .url(finalHsUser.getHeadphotopath())
//                    .build()
//                    .execute(new BitmapCallback() {
//                        @Override
//                        public void onError(Call call, Exception e, int id) {
//
//                        }
//
//                        @Override
//                        public void onResponse(Bitmap response, int id) {
//                            Bitmap bitmap = UtilBitmap.scaleBitmap(response, 50, 50);
//                            Message message = myQrcodeHandler.obtainMessage();
//                            message.what = 1;
//                            message.obj = bitmap;
//                            myQrcodeHandler.sendMessage(message);
//                        }
//                    });
//            }
//        }).start();


        //标题栏 返回按钮
        titlebar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }


}
