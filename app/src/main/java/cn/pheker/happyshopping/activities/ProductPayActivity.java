package cn.pheker.happyshopping.activities;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.entities.HsOrder;
import cn.pheker.happyshopping.entities.HsReceiveAddress;
import cn.pheker.happyshopping.entities.RetrieveDataByPage;
import cn.pheker.happyshopping.entities.ReturnResponse;
import okhttp3.Call;

public class ProductPayActivity extends BaseActivity implements View.OnClickListener {

    ImageView titlebar_back;


    private List<HsOrder> hsOrders = new ArrayList<>();
    private TextView HsOrder_ordergroupid;
    private TextView HsOrder_sumprice;
    private TextView HsOrder_paytype;
    private TextView paytye_zfb;
    private TextView paytype_wx;
    private TextView paytye_yhk;
    private TextView paytype_bitcoin;
    private Spinner spinner_hsorder_address;
    private TextView start_pay;
    private List<HsReceiveAddress> hsReceiveAddresss;
    private ArrayAdapter spinnerAddressAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_pay);

        AlexStatusBarUtils.setStatusColor(this,
                ActivityCompat.getColor(this, R.color.bg_light_greenblue), 0);
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            Log.i(TAG, "onCreate: "+bundle.getString("HsOrders"));
            hsOrders = UtilGson.fromJson(bundle.getString("HsOrders"), new TypeToken<List<HsOrder>>() {
            }.getType());
        }
        initView();
        initEvent();
    }

    @Override
    protected void initView() {
        titlebar_back = (ImageView) findViewById(R.id.titlebar_back);
        titlebar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        HsOrder_ordergroupid = (TextView) findViewById(R.id.HsOrder_ordergroupid);
        HsOrder_ordergroupid.setOnClickListener(this);
        HsOrder_sumprice = (TextView) findViewById(R.id.HsOrder_sumprice);
        HsOrder_sumprice.setOnClickListener(this);
        HsOrder_paytype = (TextView) findViewById(R.id.HsOrder_paytype);
        HsOrder_paytype.setOnClickListener(this);
        paytye_zfb = (TextView) findViewById(R.id.paytype_zfb);
        paytye_zfb.setOnClickListener(this);
        paytype_wx = (TextView) findViewById(R.id.paytype_wx);
        paytype_wx.setOnClickListener(this);
        paytye_yhk = (TextView) findViewById(R.id.paytype_yhk);
        paytye_yhk.setOnClickListener(this);
        paytype_bitcoin = (TextView) findViewById(R.id.paytype_bitcoin);
        paytype_bitcoin.setOnClickListener(this);
        spinner_hsorder_address = (Spinner) findViewById(R.id.spinner_hsorder_address);
        OkHttpUtils.get()
                .url(Constants.Url.HSRECEIVEADDRESS_RETRIEVE)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Toast.makeText(ProductPayActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onResponse(final String response, int id) {
                        RetrieveDataByPage<List<HsReceiveAddress>> data = UtilGson.fromJson(response,
                                new TypeToken<RetrieveDataByPage<List<HsReceiveAddress>>>() {
                                }.getType());
                        int code = data.getCode();
                        if (code == 0) {
                            Toast.makeText(ProductPayActivity.this, data.getMsg(), Toast.LENGTH_SHORT).show();
                        } else if (code == 1) {//表明已获取到数据
                            Log.i(TAG, "onResponse: " + UtilGson.toJson(data.getData()));
                            int currPage = data.getPage();
                            if (currPage <= data.getMaxPage()) {
                                hsReceiveAddresss = data.getData();
                                if (data.getCount() == 0) {
                                    Toast.makeText(ProductPayActivity.this, "您还没有收货地址", Toast.LENGTH_SHORT).show();
                                    return;
                                }
                                List<String> addressAliases = new ArrayList<>();
                                for (HsReceiveAddress hsReceiveAddress:
                                        hsReceiveAddresss) {
                                    addressAliases.add(hsReceiveAddress.getReceiveAddressAlias());
                                }
                                spinnerAddressAdapter = new ArrayAdapter(ProductPayActivity.this, R.layout.item_spinner_string, addressAliases);
                                spinner_hsorder_address.setAdapter(spinnerAddressAdapter);

                            } else {

                            }
                        }
                    }
                });
        start_pay = (TextView) findViewById(R.id.start_pay);
        start_pay.setOnClickListener(this);

        //订单号,总价格
        float sumPrice = 0.0f;
        int size = hsOrders.size();
        for (int i = 0; i < size; i++) {
            HsOrder hsOrder = hsOrders.get(i);
            if(i==0){
                HsOrder_ordergroupid.setText(hsOrder.getOrderGroup());
            }
            sumPrice += hsOrder.getSumMoney();
        }
        HsOrder_sumprice.setText(sumPrice+"");

    }

    @Override
    protected void initEvent() {

    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.start_pay:
                if (!start_pay.getText().toString().equals("支付成功")) {
                    //设置支付方式
                    int len = hsOrders.size();
                    String paytype = HsOrder_paytype.getText().toString();
                    for (int i = 0; i < len; i++) {
                        HsOrder hsOrder = hsOrders.get(i);
                        hsOrder.setPayType(paytype);
                        hsOrders.set(i, hsOrder);
                    }
                    OkHttpUtils.post()
                        .addParams("entities",UtilGson.toJson(hsOrders))
                        .url(Constants.Url.HSORDER_CREATEBATCH)
                        .build()
                        .execute(new StringCallback() {
                            @Override
                            public void onError(Call call, Exception e, int id) {
                                Toast.makeText(ProductPayActivity.this, "网络异常,支付失败", Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onResponse(String response, int id) {
                                ReturnResponse data = UtilGson.fromJson(response,
                                        new TypeToken<ReturnResponse>() {}.getType());
                                int code = data.getCode();
                                if (code == 0) {
                                    Toast.makeText(ProductPayActivity.this, data.getMsg(), Toast.LENGTH_SHORT).show();
                                } else if (code == 1) {//表明已获取到数据
                                    Log.i(TAG, "onResponse: " + UtilGson.toJson(data.getMsg()));
                                    Toast.makeText(ProductPayActivity.this, "支付成功", Toast.LENGTH_SHORT).show();
                                    start_pay.setText("支付成功");
                                }
                            }
                        });
                }else{
                    Toast.makeText(this, "您已经支付过了", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.paytype_zfb:
                HsOrder_paytype.setText("支付宝");
                break;
            case R.id.paytype_wx:
                HsOrder_paytype.setText("微信");
                break;
            case R.id.paytype_yhk:
                HsOrder_paytype.setText("银行卡");
                break;
            case R.id.paytype_bitcoin:
                HsOrder_paytype.setText("比特币");
                break;
            default:
                break;
        }
    }
}
