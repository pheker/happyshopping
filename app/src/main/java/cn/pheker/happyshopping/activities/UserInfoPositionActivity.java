package cn.pheker.happyshopping.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.handmark.pulltorefresh.library.ILoadingLayout;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.adapter.HsReceiveAddressAdapter;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.commons.UtilLogin;
import cn.pheker.happyshopping.entities.HsReceiveAddress;
import cn.pheker.happyshopping.entities.RetrieveDataByPage;
import cn.pheker.happyshopping.utils.DateUtil;
import okhttp3.Call;

public class UserInfoPositionActivity extends BaseActivity implements View.OnClickListener {

    private ImageView titlebar_back;
    private TextView titlebar_title;
    private TextView titlebar_settings;
    private LinearLayout activity_user_info_position;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_position);
        AlexStatusBarUtils.setStatusColor(this,
                ActivityCompat.getColor(this, R.color.bg_light_greenblue), 0);
        initView();
        initEvent();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.titlebar_settings://添加编辑页面
                startActivity(new Intent(this,UserInfoPositionEditActivity.class));
        }
    }

    private PullToRefreshListView mPullToRefreshListView;
    private HsReceiveAddressAdapter hsReceiveAddressAdapter;
    private List<HsReceiveAddress> hsReceiveAddresss = new LinkedList<>();

    public List<HsReceiveAddress> getHsReceiveAddresss() {
        return hsReceiveAddresss;
    }

    public void setHsReceiveAddresss(List<HsReceiveAddress> hsReceiveAddresss) {
        this.hsReceiveAddresss = hsReceiveAddresss;
    }

    int page = 1;//页数


    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    String userId = "";

    @Override
    protected void initView() {

        titlebar_back = (ImageView) findViewById(R.id.titlebar_back);
        titlebar_back.setOnClickListener(this);
        titlebar_title = (TextView) findViewById(R.id.titlebar_title);
        titlebar_title.setOnClickListener(this);
        titlebar_settings = (TextView) findViewById(R.id.titlebar_settings);
        titlebar_settings.setOnClickListener(this);
        activity_user_info_position = (LinearLayout) findViewById(R.id.activity_user_info_position);
        activity_user_info_position.setOnClickListener(this);
        ImageView titlebar_back = (ImageView) findViewById(R.id.titlebar_back);
        titlebar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        userId = UtilLogin.getLoginHsUserUuid(UserInfoPositionActivity.this);

        // Set a listener to be invoked when the list should be refreshed.
        mPullToRefreshListView = (PullToRefreshListView) findViewById(R.id.position_prt);

        /*刷新模式,下拉刷新+上拉加载*/
        mPullToRefreshListView.setMode(PullToRefreshBase.Mode.BOTH);

        /*设置刷新显示的状态*/
        //设置下拉显示的日期和时间
        String label = DateUtil.getNowTimeString();

        /*设置下拉刷新状态*/
        ILoadingLayout refreshLabels = mPullToRefreshListView.getLoadingLayoutProxy(true, false);
        refreshLabels.setLastUpdatedLabel(label);
        refreshLabels.setPullLabel("快点下拉呀!");
        refreshLabels.setRefreshingLabel("正在刷新...");
        refreshLabels.setReleaseLabel("放开刷新");

        /*设置上拉加载状态*/
        ILoadingLayout loadingLabels = mPullToRefreshListView.getLoadingLayoutProxy(false, true);
        loadingLabels.setLastUpdatedLabel(label);
        loadingLabels.setPullLabel("快点上拉呀!");
        loadingLabels.setRefreshingLabel("正在加载...");
        loadingLabels.setReleaseLabel("放开加载");

        hsReceiveAddresss.add(new HsReceiveAddress());//防止为空时,pulltorefresh一直刷新/无法刷新的bug
        mPullToRefreshListView.setRefreshing();
        /*初始化数据*/
        getRemoteData(page++,new UserInfoPositionActivity.RemoteDataCallback() {
            @Override
            public void dealData(List<HsReceiveAddress> hsReceiveAddresss) {
                setHsReceiveAddresss(hsReceiveAddresss);
                  /*设置适配器*/
                hsReceiveAddressAdapter = new HsReceiveAddressAdapter(UserInfoPositionActivity.this,hsReceiveAddresss);
                mPullToRefreshListView.setAdapter(hsReceiveAddressAdapter);
                hsReceiveAddressAdapter.notifyDataSetChanged();
            }
        });

        /*点击事件,有个bug position 需要-1才是真正正确的 */
        mPullToRefreshListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent();
                intent.putExtra("HsReceiveAddress", UtilGson.toJson(getHsReceiveAddresss().get(position-1)));
                intent.setClass(UserInfoPositionActivity.this, UserInfoPositionEditActivity.class);
                startActivity(intent);
            }
        });
    }

    @Override
    public void initEvent() {
        mPullToRefreshListView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<ListView>() {
            @Override
            public void onPullDownToRefresh(PullToRefreshBase<ListView> refreshView) {
                page = 1;
                getRemoteData(page++);
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<ListView> refreshView) {
                getRemoteData(page++);
            }
        });
    }

    private void getRemoteData(final int page) {
        if("".equals(userId)){
            Toast.makeText(UserInfoPositionActivity.this, "您还未登录", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("userId",userId);//最新
        params.put("type",1+"");
        params.put("page",page+"");

        OkHttpUtils.post()
                .params(params)
                .url(Constants.Url.HSRECEIVEADDRESS_RETRIEVE)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Toast.makeText(UserInfoPositionActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onResponse(final String response, int id) {
                        RetrieveDataByPage<List<HsReceiveAddress>> data = UtilGson.fromJson(response,
                                new TypeToken<RetrieveDataByPage<List<HsReceiveAddress>>>() {}.getType());
                        int code = data.getCode();
                        if (code == 0) {
                            Toast.makeText(UserInfoPositionActivity.this, data.getMsg(), Toast.LENGTH_SHORT).show();
                        } else if (code == 1) {//表明已获取到数据
                            Log.i(TAG, "onResponse: "+UtilGson.toJson(data.getData()));
//                                    Toast.makeText(activity, ""+data.getPage()+":"+data.getMaxPage(), Toast.LENGTH_SHORT).show();
                            int currPage = data.getPage();
                            if(currPage<=data.getMaxPage()) {
                                if (currPage == 1) {
                                    hsReceiveAddresss = data.getData();
                                }else if(currPage>1) {
                                    hsReceiveAddresss.addAll(data.getData());
                                    Log.i(TAG, "onResponse: "+UtilGson.toJson(data.getData()));
                                    hsReceiveAddressAdapter.notifyDataSetChanged();
                                }
                                if(data.getCount()==0){
                                    Toast.makeText(UserInfoPositionActivity.this, "您还没有收货地址", Toast.LENGTH_SHORT).show();
                                }
                            }else{
                                ILoadingLayout loadingLabels = mPullToRefreshListView.getLoadingLayoutProxy(false, true);
                                loadingLabels.setLastUpdatedLabel(DateUtil.getNowTimeString());
                                String noMoreData = "没有更多数据了!";
                                loadingLabels.setPullLabel(noMoreData);
                                loadingLabels.setRefreshingLabel(noMoreData);
                                loadingLabels.setReleaseLabel("全部加载完毕");
                                Toast.makeText(UserInfoPositionActivity.this, "没有更多数据了", Toast.LENGTH_SHORT).show();
                                setPage(data.getPage() - 1);
                            }

                            // 加载完成后停止刷新
                            mPullToRefreshListView.onRefreshComplete();
                        }

                    }
                });//OkHttpUtils end
    }


    private void getRemoteData(int page,final UserInfoPositionActivity.RemoteDataCallback rdcb) {
        if("".equals(userId)){
            Toast.makeText(UserInfoPositionActivity.this, "您还未登录", Toast.LENGTH_SHORT).show();
            return;
        }
        Map<String, String> params = new HashMap<>();
        params.put("userId",userId);//最新
        params.put("type",1+"");
        params.put("page",page+"");

        OkHttpUtils.post()
                .params(params)
                .url(Constants.Url.HSRECEIVEADDRESS_RETRIEVE)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Toast.makeText(UserInfoPositionActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onResponse(final String response, int id) {
                        RetrieveDataByPage<List<HsReceiveAddress>> data = UtilGson.fromJson(response,
                                new TypeToken<RetrieveDataByPage<List<HsReceiveAddress>>>() {}.getType());
                        int code = data.getCode();
                        if (code == 0) {
                            Toast.makeText(UserInfoPositionActivity.this, data.getMsg(), Toast.LENGTH_SHORT).show();
                        } else if (code == 1) {
                            hsReceiveAddresss = data.getData();
                            Log.i(TAG, "onResponse: "+UtilGson.toJson(hsReceiveAddresss));
                            rdcb.dealData(hsReceiveAddresss);
                            // 通知数据改变了
                            hsReceiveAddressAdapter.notifyDataSetChanged();
                            // 加载完成后停止刷新
                            mPullToRefreshListView.onRefreshComplete();
                            if(data.getCount()==0){
                                Toast.makeText(UserInfoPositionActivity.this, "您还没有收货地址", Toast.LENGTH_SHORT).show();
                            }
                            setPage(data.getPage() - 1);
                        }

                    }
                });//OkHttpUtils end
    }

    interface RemoteDataCallback  {
        void dealData(List<HsReceiveAddress> hsReceiveAddresss);
    }





}
