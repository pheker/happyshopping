package cn.pheker.happyshopping.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bm.library.PhotoView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.commons.UtilLogin;
import cn.pheker.happyshopping.entities.HsCollection;
import cn.pheker.happyshopping.entities.HsOrder;
import cn.pheker.happyshopping.entities.HsProduct;
import cn.pheker.happyshopping.entities.ReturnResponse;
import okhttp3.Call;

public class ProductDetailActivity extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "ProductDetailActivity";

    private PhotoView mThumnail;
    private TextView titlebar_title,titlebar_collection, mTitle, mContent,
            detail_num_digital_reduce, detail_num_digital_plus,
            hsproduct_num, hsproduct_paynum, hsproduct_discount, hsproduct_groupchase,
            hsproduct_coupon, detail_price_num, detail_num_joincarket, detail_num_purchase,
            detail_price_sum;
    private EditText detail_num_digital;
    private ImageView mTitlebarBack;

    private HsProduct hsProduct;
    private RatingBar product_ratingbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_detail);
        AlexStatusBarUtils.setStatusColor(this, ActivityCompat.getColor(ProductDetailActivity.this, R.color.bg_login), 0);

        initView();
        initEvent();

        Bundle bundle = getIntent().getExtras();
        String data = (String) bundle.get("HsProduct");
        hsProduct = UtilGson.fromJson(data, HsProduct.class);

        showHsProduct();

    }


    @Override
    protected void initView() {
        mThumnail = ((PhotoView) findViewById(R.id.detail_thumnail));
        mTitle = ((TextView) findViewById(R.id.detail_title));
        titlebar_collection = ((TextView) findViewById(R.id.titlebar_collection));
        titlebar_title = (TextView) findViewById(R.id.titlebar_title);
        titlebar_title.setText("商品详情");
        mContent = ((TextView) findViewById(R.id.detail_content));
        mTitlebarBack = (ImageView) findViewById(R.id.titlebar_back);
        detail_num_digital_reduce = ((TextView) findViewById(R.id.detail_num_digital_reduce));
        detail_num_digital = ((EditText) findViewById(R.id.detail_num_digital));
        detail_num_digital_plus = ((TextView) findViewById(R.id.detail_num_digital_plus));

    }

    @Override
    protected void initEvent() {
        mTitlebarBack.setOnClickListener(this);
        detail_num_digital_reduce.setOnClickListener(this);
        detail_num_digital_plus.setOnClickListener(this);

        titlebar_collection.setOnClickListener(this);
        product_ratingbar = (RatingBar) findViewById(R.id.product_ratingbar);
        product_ratingbar.setOnClickListener(this);
        hsproduct_num = (TextView) findViewById(R.id.hsproduct_num);
        hsproduct_num.setOnClickListener(this);
        hsproduct_paynum = (TextView) findViewById(R.id.hsproduct_paynum);
        hsproduct_paynum.setOnClickListener(this);
        hsproduct_discount = (TextView) findViewById(R.id.hsproduct_discount);
        hsproduct_discount.setOnClickListener(this);
        hsproduct_groupchase = (TextView) findViewById(R.id.hsproduct_groupchase);
        hsproduct_groupchase.setOnClickListener(this);
        hsproduct_coupon = (TextView) findViewById(R.id.hsproduct_coupon);
        hsproduct_coupon.setOnClickListener(this);
        detail_price_num = (TextView) findViewById(R.id.detail_price_num);
        detail_price_num.setOnClickListener(this);
        detail_num_joincarket = (TextView) findViewById(R.id.detail_num_joincarket);
        detail_num_joincarket.setOnClickListener(this);
        detail_num_purchase = (TextView) findViewById(R.id.detail_num_purchase);
        detail_num_purchase.setOnClickListener(this);
        detail_price_sum = (TextView) findViewById(R.id.detail_price_sum);
        detail_price_sum.setOnClickListener(this);
    }

    private void showHsProduct() {
        mThumnail.enable();//启用预览
        if (hsProduct == null) {
            return;
        }
        if (!"".equals(hsProduct.getPicspath())) {
            /*使用Glide加载图片*/
            Glide.with(ProductDetailActivity.this)
                    .load(hsProduct.getPicspath())
                    .crossFade()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .placeholder(R.drawable.img_loading)
                    .error(R.drawable.loading_fail)
                    .into(mThumnail);
        }

        mTitle.setText(hsProduct.getName());
        mContent.setText(hsProduct.getDescription());
        //评价星级
        product_ratingbar.setRating(hsProduct.getEvaluation());
        //存货,已售数量
        hsproduct_num.setText(hsProduct.getNum() + "");
        hsproduct_paynum.setText(hsProduct.getPaynum() + "");
        //打折,团购,抵扣券
        hsproduct_discount.setText(hsProduct.getDiscount() + "");
        hsproduct_groupchase.setText(hsProduct.getGroupPurchase() + "");
        hsproduct_coupon.setText(hsProduct.getCoupon() + "");
        //价格,总价格
        detail_price_num.setText(hsProduct.getPrice() + "");
        detail_price_sum.setText(getSumPrice() + "");

    }

    @Override
    public void onClick(View v) {
        int digital;
        switch (v.getId()) {
//返回按钮
            case R.id.titlebar_back:
                ProductDetailActivity.this.finish();
                break;
//收藏
            case R.id.titlebar_collection:
                String hsUserUUID = UtilLogin.getLoginHsUserUuid(ProductDetailActivity.this);
                if ("".equals(hsUserUUID)) {
                    Toast.makeText(this, "您还未登录", Toast.LENGTH_SHORT).show();
                    return;
                }
                HsCollection hsCollection = new HsCollection(UUID.randomUUID().toString(),
                        hsUserUUID,hsProduct.getUuid(),1);
                Log.i(TAG, UtilGson.toJson(hsCollection));
                OkHttpUtils.post()
                        .url(Constants.Url.HSCOLLECTION_CREATE)
                        .addParams("entity", UtilGson.toJson(hsCollection))
                        .build()
                        .execute(new StringCallback() {
                            @Override
                            public void onError(Call call, Exception e, int id) {
                                Toast.makeText(ProductDetailActivity.this, Constants.I18n.zh_cn.NET_ERROR, Toast.LENGTH_SHORT).show();
                            }

                            @Override
                            public void onResponse(String response, int id) {
                                ReturnResponse returnResponse = UtilGson.fromJson(response, ReturnResponse.class);
                                int code = returnResponse.getCode();
                                if (code == 1) {
                                    Toast.makeText(ProductDetailActivity.this, "收藏成功", Toast.LENGTH_SHORT).show();
                                }else{
                                    Toast.makeText(ProductDetailActivity.this, returnResponse.getMsg(), Toast.LENGTH_SHORT).show();
                                    Log.i(TAG, "收藏失败");
                                }
                            }
                        });

                break;
//商品评价0-5分
            case R.id.product_ratingbar:

                break;
//商品数量-/+
            case R.id.detail_num_digital_reduce:
                digital = Integer.parseInt(detail_num_digital.getText().toString());
                if (digital > 1) {
                    detail_num_digital.setText(--digital + "");
                } else {//最小为1
                    detail_num_digital.setText("1");
                }
                detail_price_sum.setText(getSumPrice()+"");
                break;
            case R.id.detail_num_digital_plus:
                digital = Integer.parseInt(detail_num_digital.getText().toString());
                if (digital < hsProduct.getNum()) {//应从后台获取 商品存货数量
                    detail_num_digital.setText(++digital + "");
                }
                detail_price_sum.setText(getSumPrice()+"");
                break;
//加入购物车
            case R.id.detail_num_joincarket:
                String hsUserUUID2 = UtilLogin.getLoginHsUserUuid(ProductDetailActivity.this);
                if ("".equals(hsUserUUID2)) {
                    Toast.makeText(this, "您还未登录", Toast.LENGTH_SHORT).show();
                    return;
                }
                HsCollection hsCollection_to_cart = new HsCollection(UUID.randomUUID().toString(),
                        hsUserUUID2,hsProduct.getUuid(),2);
                OkHttpUtils.post()
                    .url(Constants.Url.HSCOLLECTION_CREATE)
                    .addParams("entity", UtilGson.toJson(hsCollection_to_cart))
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            Toast.makeText(ProductDetailActivity.this, Constants.I18n.zh_cn.NET_ERROR, Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onResponse(String response, int id) {
                            ReturnResponse returnResponse = UtilGson.fromJson(response, ReturnResponse.class);
                            int code = returnResponse.getCode();
                            if (code == 1) {
                                Toast.makeText(ProductDetailActivity.this, "添加到购物车成功", Toast.LENGTH_SHORT).show();
                            }else{
                                Log.i(TAG, "onResponse: 添加到购物车失败");
                            }
                        }
                    });

                break;
//立即购买
            case R.id.detail_num_purchase:
                Intent intent = new Intent(ProductDetailActivity.this,ProductPayActivity.class);
                Bundle bundle = new Bundle();
                List<HsOrder> hsOrders = new ArrayList<>();
                hsOrders.add(new HsOrder(UUID.randomUUID().toString(),
                        UUID.randomUUID().toString(),
                        "",hsProduct.getPrice(),1,
                        UtilLogin.getLoginHsUserUuid(ProductDetailActivity.this),
                        hsProduct.getUuid()));
                bundle.putString("HsOrders",UtilGson.toJson(hsOrders));//订单entity
                intent.putExtras(bundle);
                startActivity(intent);
                break;

            default:
                break;
        }
    }


    /**
     * 计算总价
     * @return
     */
    public float getSumPrice() {
        //优惠券,抵扣券
        String hsproduct_coupon_text = hsproduct_coupon.getText().toString();
        float coupon = 0.0f;
        if (hsproduct_coupon_text != null && !"".equals(hsproduct_coupon_text)) {
            coupon = Float.parseFloat(hsproduct_coupon_text);
        }

        //团购
        String hsproduct_groupchase_text = hsproduct_groupchase.getText().toString();
        float groupchase = 0.0f;
        if (hsproduct_groupchase_text != null && !"".equals(hsproduct_groupchase_text)) {
            groupchase = Float.parseFloat(hsproduct_groupchase_text);
        }

        //打折
        String hsproduct_discount_text = hsproduct_discount.getText().toString();
        float discount = 0.0f;
        if (hsproduct_discount_text != null && !"".equals(hsproduct_discount_text)) {
            discount = Float.parseFloat(hsproduct_discount_text);
        }

        //单价
        float price = Float.parseFloat(detail_price_num.getText().toString());
        float num = Integer.parseInt(detail_num_digital.getText().toString());
        if(groupchase!=0.0){
            price *= groupchase;
        }else if(discount!=0.0){
            price *= discount;
        }else if(coupon!=0.0){
            price -= coupon;
        }
        return Float.parseFloat(String.format("%.2f",price * num));
    }




}
