package cn.pheker.happyshopping.activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.Constants;


/**
 * Created by cn.pheker on 2017/4/1.
 * Mail to hkbxoic@gmail.com
 * 欢迎页面
 */
public class WelcomeActivity extends BaseActivity {


    private static final int DELAY_TIME = 2000;

    //通过参数决定跳转到哪一个页面
    private static final int GO_GUIDE = 1001;       //新手导航页面
    private static final int GO_MAIN = 1002;        //主页面
    private TextView mTimer;
    private LinearLayout guide_timer_wrapper;

    private MyCount myCount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_welcome);
        AlexStatusBarUtils.setStatusColor(this, ActivityCompat.getColor(WelcomeActivity.this,R.color.welcome_statusbar_gray),0);//显示状态栏,设置状态栏  颜色 透明度等参数,默认为黑色

        initView();
        initEvent();
    }

    @Override
    public void initView() {
        guide_timer_wrapper = (LinearLayout) findViewById(R.id.guide_timer_wrapper);
        mTimer = (TextView) findViewById(R.id.guide_timer);
        final SharedPreferences sp = getSharedPreferences(Constants.SP_CONFIG,MODE_PRIVATE);
        int openCounter = sp.getInt("openCounter",1);   //打开次数统计
        SharedPreferences.Editor edit = sp.edit();
        if(openCounter%Constants.GUIDE_FREQUENCY==1){   //指定频率进入导航页GuideActivity
            edit.putInt("openCounter",++openCounter);
            edit.commit();
            handler.sendEmptyMessageDelayed(GO_GUIDE,0);
        }else {         //否,首页倒计时maxSecond后进入主页面
            edit.putInt("openCounter",++openCounter);
            edit.commit();
            Message message = handler.obtainMessage();
            message.what = GO_MAIN;
            message.arg1 = 1;
            handler.sendMessage(message);
        }
    }

    @Override
    public void initEvent() {
        guide_timer_wrapper.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(myCount!=null) {
                    myCount.cancel();// 点击之后跳过
                    goMain();
                }
                }
        });
    }

    private Handler handler = new Handler(){
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what){
                case GO_GUIDE:
                    goGuide();
                    break;
                case GO_MAIN:
                    if(msg.arg1==1){
                        guide_timer_wrapper.setVisibility(View.VISIBLE);
                        int maxSecond = Constants.GUIDE_TIMER_MAXSECOND;
                        myCount = new MyCount(maxSecond*1000, 1000);// 创建一个倒计时 总时长10秒 间隔1秒
                        myCount.start();// 开启倒计时
                    }else {
                        goMain();
                    }
                    break;
                default:
                    break;
            }
        }

    };

    private void goMain() {
        Intent intent = new Intent(WelcomeActivity.this, MainActivity.class);
        startActivity(intent);
        Log.i(TAG, "goMain: ");
        finish();
    }


    private void goGuide(){
        Intent intent = new Intent(WelcomeActivity.this, GuideActivity.class);
        startActivity(intent);
//        overridePendingTransition(R.anim.activity_right_in,R.anim.activity_left_out);
        WelcomeActivity.this.finish();
        Log.i(TAG, "goGuide: ");
        finish();
    }




    /**
     * 返回键 事件
     * @param keyCode
     * @param event
     * @return
     */
    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if(keyCode == event.KEYCODE_BACK&&event.getRepeatCount()==0){
            exit();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }




    /* 定义一个倒计时的内部类 */
    class MyCount extends CountDownTimer {
        /**
         *
         * @param millisInFuture        持续时长
         * @param countDownInterval     间隔时长
         */
        public MyCount(long millisInFuture, long countDownInterval) {
            super(millisInFuture, countDownInterval);
        }

        /**
         * 在倒计时结束时调用
         */
        @Override
        public void onFinish() {
            mTimer.setText("0");
            goMain();
        }

        /**
         * 每间隔countDownInterval会调用一次
         * @param millisUntilFinished   已经过去了多长时间
         */
        @Override
        public void onTick(long millisUntilFinished) {
            mTimer.setText(millisUntilFinished/1000+"");
            Log.i(TAG, "onTick: "+millisUntilFinished/1000);
        }
    }
}
