package cn.pheker.happyshopping.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import cn.pheker.happyshopping.Dialog.LoadingDialog;
import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.commons.UtilLogin;
import cn.pheker.happyshopping.commons.UtilMD5;
import cn.pheker.happyshopping.entities.HsUser;
import cn.pheker.happyshopping.entities.ReturnResponseWithData;
import okhttp3.Call;

/**
 * Created by cn.pheker on 2017/4/5.
 * Mail to hkbxoic@gmail.com
 * 登录
 */
public class LoginActivity extends BaseActivity implements View.OnClickListener{


    private EditText mNameView;
    private EditText mPasswordView;
    private View mLoginBtn;
    private View mSkipToRegisterView;
    int will_open = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //取消标题
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //取消状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN ,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_login);

        will_open = getIntent().getIntExtra("will_open", 0);

        initView();

        initEvent();
    }

    public void initView() {
        mNameView = (EditText) findViewById(R.id.user_name);
        mPasswordView = (EditText) findViewById(R.id.user_password);
        mLoginBtn = findViewById(R.id.user_login);
        mSkipToRegisterView = findViewById(R.id.skip_to_register);
    }

    public void initEvent() {

        mNameView.setOnClickListener(this);
        mPasswordView.setOnClickListener(this);
        mLoginBtn.setOnClickListener(this);
        mSkipToRegisterView.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.user_login:
                doLogin();
                break;
            case R.id.skip_to_register:
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
//                overridePendingTransition(R.anim.activity_right_in,R.anim.activity_left_out);
                finish();
                break;
            default:
                break;
        }
    }



    private void doLogin() {

        boolean canLogin = true;

        final HsUser hsUser = new HsUser();
        //用户名(也可以是手机号)
        String name = mNameView.getText().toString();
        //密码
        String pwd = mPasswordView.getText().toString();
        if (name==null||"".equals(name.trim())){
            Toast.makeText(this, "用户名不能空", Toast.LENGTH_SHORT).show();
            canLogin = false;
        }else if(!name.trim().matches(Constants.Pattern.LOGIN_NAME)){
            Toast.makeText(this, "用户名4-10字母数字或者手机号,qq,邮箱", Toast.LENGTH_SHORT).show();
            canLogin = false;
        }else{
            hsUser.setName(name.trim());

            if (pwd == null || "".equals(pwd)) {
                Toast.makeText(this, "密码不能空", Toast.LENGTH_SHORT).show();
                canLogin = false;
            }else if(!pwd.trim().matches(Constants.Pattern.PASSWORD)){
                Toast.makeText(this, "密码必须包含字母和数字或!@#$%^&*()_+-=等特殊字符,长度6-16位", Toast.LENGTH_SHORT).show();
                canLogin = false;
            }else{
                hsUser.setPassword(UtilMD5.encryptToSHA(UtilMD5.getMD5(pwd.trim())));
            }

        }


        if(canLogin) {
            //显示等待框
            final LoadingDialog loadingDialog = new LoadingDialog(LoginActivity.this);
            loadingDialog.setCancelable(false);
            loadingDialog.setTitle("正在登录...");
            loadingDialog.show();

            String entityString = UtilGson.toJson(hsUser);
            Log.i(TAG, "doLogin: "+entityString);

            //登录
            OkHttpUtils
                    .post()
                    .url(Constants.Url.LOGIN_PATH)
                    .addParams("entity",entityString)
                    .build()
                    .execute(new StringCallback() {
                        @Override
                        public void onError(Call call, Exception e, int id) {
                            Toast.makeText(LoginActivity.this, "登录失败!", Toast.LENGTH_SHORT).show();
                            loadingDialog.dismiss();
                            e.printStackTrace();
                        }

                        @Override
                        public void onResponse(final String response, int id) {
                            final String str = response.toString();
                            final ReturnResponseWithData<HsUser> returnResponse = UtilGson.fromJson(response, new TypeToken<ReturnResponseWithData<HsUser>>(){}.getType());
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    loadingDialog.dismiss();
                                    int code = returnResponse.getCode();
                                    if(code==1) {
                                        Toast.makeText(LoginActivity.this, "登录成功!", Toast.LENGTH_SHORT).show();
                                        Log.i(TAG, "onResponse: " + str);
                                        HsUser retHsUser = returnResponse.getData();
                                        UtilLogin.setLogin(LoginActivity.this);//保存登录状态
                                        UtilLogin.saveHsUser(LoginActivity.this,retHsUser.getUuid(),UtilGson.toJson(retHsUser));

                                        //注册成功跳转到主页面
                                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                                        intent.putExtra("will_open",will_open);
                                        startActivity(intent);

//                                        overridePendingTransition(R.anim.activity_right_in, R.anim.activity_left_out);

                                        finish();
                                    }else if(code == 2){//未注册
                                        Toast.makeText(LoginActivity.this, returnResponse.getMsg(), Toast.LENGTH_SHORT).show();
                                        startActivity(new Intent(LoginActivity.this, RegisterActivity.class));
                                        finish();
                                    }else if(code == 3){//用户名或密码错误
                                        Toast.makeText(LoginActivity.this, returnResponse.getMsg(), Toast.LENGTH_SHORT).show();
                                    }else if(returnResponse.getCode()==0){
                                        Toast.makeText(LoginActivity.this, returnResponse.getMsg(), Toast.LENGTH_SHORT).show();
                                    }

                                }
                            });
                        }
                    } );
        }

    }

    
}

