package cn.pheker.happyshopping.activities;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.UUID;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.commons.UtilLogin;
import cn.pheker.happyshopping.entities.HsReceiveAddress;
import cn.pheker.happyshopping.entities.ReturnResponse;
import okhttp3.Call;

public class UserInfoPositionEditActivity extends BaseActivity implements View.OnClickListener{

    private EditText receive_address_alias;
    private EditText delivery_address;
    private EditText receive_person_name;
    private EditText receive_person_phone;
    private TextView receive_address_save;
    private LinearLayout activity_user_info_position_edit;
    private HsReceiveAddress hsReceiveAddress;
    private boolean hasSaved = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_position_edit);
        AlexStatusBarUtils.setStatusColor(this,
                ActivityCompat.getColor(this, R.color.bg_light_greenblue), 0);
        String hsReceiveAddressStr = getIntent().getStringExtra("HsReceiveAddress");
        if (!"".equals(hsReceiveAddressStr)) {
            hsReceiveAddress = UtilGson.fromJson(hsReceiveAddressStr, HsReceiveAddress.class);
        }
        initView();
        initEvent();
    }

    @Override
    protected void initView() {

        receive_address_alias = (EditText) findViewById(R.id.receive_address_alias);
        receive_address_alias.setOnClickListener(this);
        delivery_address = (EditText) findViewById(R.id.delivery_address);
        delivery_address.setOnClickListener(this);
        receive_person_name = (EditText) findViewById(R.id.receive_person_name);
        receive_person_name.setOnClickListener(this);
        receive_person_phone = (EditText) findViewById(R.id.receive_person_phone);
        receive_person_phone.setOnClickListener(this);
        receive_address_save = (TextView) findViewById(R.id.receive_address_save);
        receive_address_save.setOnClickListener(this);
        activity_user_info_position_edit = (LinearLayout) findViewById(R.id.activity_user_info_position_edit);
        activity_user_info_position_edit.setOnClickListener(this);
        ImageView titlebar_back = (ImageView) findViewById(R.id.titlebar_back);
        titlebar_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    @Override
    protected void initEvent() {
        if(hsReceiveAddress==null) return;
        receive_address_alias.setText(hsReceiveAddress.getReceiveAddressAlias());
        delivery_address.setText(hsReceiveAddress.getDeliveryAddress());
        receive_person_name.setText(hsReceiveAddress.getReceivePersonName());
        receive_person_phone.setText(hsReceiveAddress.getReceivePersonPhone());
    }

    private void submit() {
        if (hasSaved) {
            Toast.makeText(this, "请勿重复保存", Toast.LENGTH_SHORT).show();
            return;
        }
        // validate
        String alias = receive_address_alias.getText().toString().trim();
        if (TextUtils.isEmpty(alias)) {
            Toast.makeText(this, "alias不能为空", Toast.LENGTH_SHORT).show();
            return;
        } else if (!alias.matches(".{1,50}")) {
            Toast.makeText(this, "10个字符以内", Toast.LENGTH_SHORT).show();
            return;
        }


        String address = delivery_address.getText().toString().trim();
        if (TextUtils.isEmpty(address)) {
            Toast.makeText(this, "address不能为空", Toast.LENGTH_SHORT).show();
            return;
        } else if (!address.matches(".{1,50}")) {
            Toast.makeText(this, "50个字符以内", Toast.LENGTH_SHORT).show();
            return;
        }

        String name = receive_person_name.getText().toString().trim();
        if (TextUtils.isEmpty(name)) {
            Toast.makeText(this, "name不能为空", Toast.LENGTH_SHORT).show();
            return;
        } else if (!name.matches(".{1,20}")) {
            Toast.makeText(this, "20个字符以内", Toast.LENGTH_SHORT).show();
            return;
        }

        String phone = receive_person_phone.getText().toString().trim();
        if (TextUtils.isEmpty(phone)) {
            Toast.makeText(this, "phone不能为空", Toast.LENGTH_SHORT).show();
            return;
        } else if (!phone.matches(Constants.Pattern.PHONE)) {
            Toast.makeText(this, "手机号格式不正确", Toast.LENGTH_SHORT).show();
            return;
        }

        // TODO validate success, do something
        if(hsReceiveAddress==null) {
            hsReceiveAddress = new HsReceiveAddress(UUID.randomUUID().toString(),
                    UtilLogin.getLoginHsUserUuid(this),
                    address, name, phone, alias);
        }
        OkHttpUtils.post()
            .addParams("entity", UtilGson.toJson(hsReceiveAddress))
            .url(Constants.Url.HSRECEIVEADDRESS_CREATE)
            .build()
            .execute(new StringCallback() {
                @Override
                public void onError(Call call, Exception e, int id) {
                    Toast.makeText(UserInfoPositionEditActivity.this, "网络异常",
                            Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onResponse(String response, int id) {
                    ReturnResponse returnResponse = UtilGson.fromJson(response,
                            new TypeToken<ReturnResponse>() {}.getType());
                    int code = returnResponse.getCode();
                    if (code == 0) {
                        Toast.makeText(UserInfoPositionEditActivity.this, returnResponse.getMsg(), Toast.LENGTH_SHORT).show();
                    }else{
                        Toast.makeText(UserInfoPositionEditActivity.this, "保存成功", Toast.LENGTH_SHORT).show();
                        hasSaved = true;
                    }
                }
            });
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.receive_address_save:
                submit();

                break;
        }
    }
}
