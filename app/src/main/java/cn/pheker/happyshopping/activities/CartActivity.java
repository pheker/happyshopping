package cn.pheker.happyshopping.activities;

import android.os.Bundle;
import android.widget.Adapter;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;

public class CartActivity extends BaseActivity {


    private Adapter productAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);

        initView();
        initEvent();
    }


    @Override
    protected void initView() {


    }

    @Override
    protected void initEvent() {


    }
}
