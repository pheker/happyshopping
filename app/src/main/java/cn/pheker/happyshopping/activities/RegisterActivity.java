package cn.pheker.happyshopping.activities;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.UUID;

import cn.pheker.happyshopping.Dialog.LoadingDialog;
import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;
import cn.pheker.happyshopping.commons.Constants;
import cn.pheker.happyshopping.commons.UtilGson;
import cn.pheker.happyshopping.commons.UtilLogin;
import cn.pheker.happyshopping.commons.UtilMD5;
import cn.pheker.happyshopping.commons.ui_usetime_check.BlockDetectByPrinter;
import cn.pheker.happyshopping.entities.HsUser;
import cn.pheker.happyshopping.entities.ReturnResponse;
import okhttp3.Call;

/**
 * Created by cn.pheker on 2017/4/5.
 * Mail to hkbxoic@gmail.com
 */
public class RegisterActivity extends BaseActivity implements View.OnClickListener{

    // UI references.
    private View mLoginForm;
    private ProgressBar mProgressView;
    private EditText mNameView;
    private EditText mPasswordView;
    private EditText mPasswordConfirmView;
    private View mRegisterBtn;
    private View mSkipToLoginView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //取消标题
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //取消状态栏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN ,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_register);

        BlockDetectByPrinter.start();//检测卡顿

        initView();
        initEvent();


    }

    public void initView() {
        mNameView = (EditText) findViewById(R.id.user_name);
        mPasswordView = (EditText) findViewById(R.id.user_password);
        mPasswordConfirmView = (EditText) findViewById(R.id.user_password_confirm);
        mRegisterBtn = findViewById(R.id.user_register);
        mSkipToLoginView = findViewById(R.id.skip_to_login);
    }

    public void initEvent() {

        mNameView.setOnClickListener(this);
        mPasswordView.setOnClickListener(this);
        mPasswordConfirmView.setOnClickListener(this);
        mRegisterBtn.setOnClickListener(this);
        mSkipToLoginView.setOnClickListener(this);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.user_register:
                doRegister();
                break;
            case R.id.skip_to_login:
                //跳转到登页面
                startActivity(new Intent(RegisterActivity.this,LoginActivity.class));
                overridePendingTransition(R.anim.activity_left_out,R.anim.activity_right_in);
                finish();
                break;
            default:
                break;
        }
    }

    private void doRegister() {
        boolean canReg = true;

        final HsUser hsUser = new HsUser();
        hsUser.setUuid(UUID.randomUUID().toString());
        //用户名(也可以是手机号)
        String name = mNameView.getText().toString();
        //密码
        String pwd = mPasswordView.getText().toString();
        String pwd2 = mPasswordConfirmView.getText().toString();
        if (name==null||"".equals(name.trim())){
            Toast.makeText(this, "用户名不能空", Toast.LENGTH_SHORT).show();
            canReg = false;
        }else if(!name.trim().matches(Constants.Pattern.REGISTER_NAME)){
            Toast.makeText(this, "用户名4-10字母数字,手机号,邮箱", Toast.LENGTH_SHORT).show();
            canReg = false;
        }else{
            if(name.matches(Constants.Pattern.PHONE)){
                hsUser.setPhone(name.trim());
            }else if(name.matches(Constants.Pattern.EMAIL)) {
                hsUser.setEmail(name.trim());
            }else{
                hsUser.setName(name.trim());
            }

            if (pwd == null || "".equals(pwd.trim())) {
                Toast.makeText(this, "密码不能空", Toast.LENGTH_SHORT).show();
                canReg = false;
            }else if(!pwd.trim().matches(Constants.Pattern.PASSWORD)){
                Toast.makeText(this, "密码必须包含字母和数字或!@#$%^&*()_+-=等特殊字符,长度6-16位", Toast.LENGTH_SHORT).show();
                canReg = false;
            }else{
                if(pwd2==null||"".equals(pwd2.trim())||!pwd.equals(pwd2.trim())){
                    Toast.makeText(this, "两次输入密码不一致", Toast.LENGTH_SHORT).show();
                    canReg = false;
                }else{
                    hsUser.setPassword(UtilMD5.encryptToSHA(UtilMD5.getMD5(pwd.trim())));
                }

            }

        }


        if(canReg) {
            //显示等待框
            final LoadingDialog loadingDialog = new LoadingDialog(RegisterActivity.this);
            loadingDialog.setCancelable(false);
            loadingDialog.setTitle("正在登录...");
            loadingDialog.show();

            //注册
            OkHttpUtils
                .post()
                .url(Constants.Url.REGISTER_PATH)
                .addParams("entity",UtilGson.toJson(hsUser))
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Toast.makeText(RegisterActivity.this, "登录失败!", Toast.LENGTH_SHORT).show();
                        loadingDialog.dismiss();
                        e.printStackTrace();
                    }

                    @Override
                    public void onResponse(String response, int id) {
                        final String str = response.toString();
                        final ReturnResponse returnResponse = UtilGson.fromJson(response, ReturnResponse.class);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                loadingDialog.dismiss();
                                if(returnResponse.getCode()==1) {
                                    Toast.makeText(RegisterActivity.this, "登录成功!", Toast.LENGTH_SHORT).show();
                                    Log.i(TAG, "onResponse: " + str);

                                    //注册成功跳转到主页面
                                    Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
                                    intent.putExtra("open_person_center",true);
                                    startActivity(intent);
                                    UtilLogin.setLogin(RegisterActivity.this);//保存登录状态
                                    UtilLogin.saveHsUser(RegisterActivity.this,hsUser.getUuid(),UtilGson.toJson(hsUser));
                                    finish();
                                }else if(returnResponse.getCode()==0){
                                    Toast.makeText(RegisterActivity.this, returnResponse.getMsg(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        });
                    }
                } );
        }
    }


}
