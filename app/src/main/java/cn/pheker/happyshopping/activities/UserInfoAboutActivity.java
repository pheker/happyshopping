package cn.pheker.happyshopping.activities;

import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.statusbar_alexleo.alexstatusbarutilslib.AlexStatusBarUtils;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.BaseActivity;

public class UserInfoAboutActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_info_about);
        AlexStatusBarUtils.setStatusColor(this, ActivityCompat.getColor(UserInfoAboutActivity.this,R.color.bg_light_greenblue),0);

    }

    @Override
    protected void initView() {

    }

    @Override
    protected void initEvent() {

    }
}
