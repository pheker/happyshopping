package cn.pheker.happyshopping.commons;

import android.os.Environment;

import java.io.File;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/4/28
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description
 *
 * </pre>
 */

public class Constants {
    /* SharePreferences 配置文件名称 */
    public static final String SP_CONFIG = "happyshopping_sp_config.xml";

    /* 导航页显示频率 建议2-5 */
    public static final int GUIDE_FREQUENCY = 2;
    /* 倒计时 */
    public static final int GUIDE_TIMER_MAXSECOND = 5;
    /* 百度AK */
    public static final String BD_AK = "0h5NcNfzwdw5Ykn6QSbx6uoUsOGoPkhF";

    /* 请求地址 */
    public static class Url {
        /* 项目跟路径 */
        public static final String SERVER_ADDRESS = "http://pheker.cn/ssm";
        public static final String SERVER_ADDRESS1 = "http://192.168.1.3:8880/ssm";
        /* 注册 */
        public static final String REGISTER_PATH = SERVER_ADDRESS + "/HsUser/create";
        /* 登录 */
        public static final String LOGIN_PATH = SERVER_ADDRESS + "/HsUser/login";
        /* 获取用户信息 */
        public static final String RETRIEVE_HsUser_byUUID = SERVER_ADDRESS + "/HsUser/retrieveByUuidOrLoginName";
        /*更新用户信息*/
        public static final String UPDATE_PATH = SERVER_ADDRESS + "/HsUser/update";
        /*图片上传--头像上传*/
        public static final String UPLOAD_PICS = SERVER_ADDRESS + "/HsUser/uploadHeadPhoto";

        /*商品*/
        public static final String HSPRODUCT_BYPAGE_PATH = SERVER_ADDRESS + "/HsProduct/retrieve";

        /*商品条件查询:根据分类,排序字段 */
        public static final String HSPRODUCT_BYPAGE_CONDITION_PATH = SERVER_ADDRESS + "/HsProduct/retrieveCondition";
        /*添加商品到收藏,购物车,关注商家*/
        public static final String HSCOLLECTION_CREATE = SERVER_ADDRESS + "/HsCollection/create";
        /*查询收藏,购物车,关注商家*/
        public static final String HSCOLLECTION_RETRIEVEBYUSERIDTYPE = SERVER_ADDRESS + "/HsCollection/retrieveByUserIdType";

        /*订单*/
        public static final String HSORDER_RETRIEVEBYUSERID_PATH = SERVER_ADDRESS + "/HsOrder/retrieveByUserId";
        public static final String HSORDER_CREATEBATCH = SERVER_ADDRESS + "/HsOrder/createBatch";

        /* 收货地址,crud */
        public static final String HSRECEIVEADDRESS_CREATE = SERVER_ADDRESS + "/HsReceiveAddress/create";
        public static final String HSRECEIVEADDRESS_RETRIEVE = SERVER_ADDRESS + "/HsReceiveAddress/retrieve";
    }


    /* 正则 */
    public static class Pattern {
        /*邮箱*/
        public static final String EMAIL = "[A-Za-z0-9\\u4e00-\\u9fa5]+@[a-zA-Z0-9_-]+(\\.[a-zA-Z0-9_-]+)+";
        /*密码 密码必须包含字母和数字或!@#$%^&*()_+-=等特殊字符,长度6-16位 */
        public static final String PASSWORD = "(?=.*[a-zA-Z])(?=.*[0-9])[a-zA-Z0-9!@#$%^&*()_+-=]{6,16}";
        /*字母数字4-16*/
        public static final String LETTER_DIGIT = "[a-zA-Z0-9]{4,16}";
        /*QQ号*/
        public static final String QQ = "\\d{4,12}";
        /*手机号*/
        public static final String PHONE = "1[358][0-9]{9}";

        /* 登录名,字母+数字 QQ号/手机号 邮箱 */
        public static final String LOGIN_NAME = LETTER_DIGIT + "|" + QQ + "|" + EMAIL;
        /* 注册时的用户名,字母+数字 手机号 邮箱 */
        public static final String REGISTER_NAME = LETTER_DIGIT + "|" + QQ + "|" + EMAIL;
    }

    /*获取图片*/
    public static class TakePhoto {
        /*拍照图片的存储路径*/
        public static final String SAVED_IMAGE_DIR_PATH = Environment.getExternalStorageDirectory().getAbsolutePath() + File.separator + "hs_imgs";

        /* 请求识别码 */
        /*从相册获取*/
        public static final int GALLERY = 0xa0;
        /*拍照获取*/
        public static final int CAMERA = 0xa1;
        /*裁剪页面*/
        public static final int CROP_PHOTO = 0xa2;
    }

    /*result code*/
    public static class ResultCode{
        /*UserInfoActivity用户信息更新*/
        public static final int USERINFO_ACTIVITY_UPDATE = 0xd001;
        /*二维码扫描返回码*/
        public static final int QRCODE_SCAN_RESULT = 0xd001;
    }


    /* I18n */
    public static class I18n {

        /*中文*/
        public static class zh_cn {
            public static final String NET_ERROR = "网络错误";

        }
    }

    /* 测试 */
    public static class Test {
        public static String json = "[" +
                "{\"thumnail\":\"http://ubmcmm.baidustatic.com/media/v1/0f000KLR1X4t_P41d9V_20.jpg\",\"title\":\"2014-07-02 22:05 工具上线\",\"content\":\"2016-11-16 14:13 增加php, go类生成.2016-11-19 01:17 增加java类生成\"}," +
                "{\"thumnail\":\"http://img13.360buyimg.com/n1/jfs/t3133/159/9683831508/112116/f7de18ac/58d70e39N7d687a9d.jpg\",\"title\":\"2014-07-02 22:05 工具上线\",\"content\":\"2016-11-16 14:13 增加php, go类生成.2016-11-19 01:17 增加java类生成\"}," +
                "{\"thumnail\":\"https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1490868724807&di=71625a1457d20cc4b964a6663e42fdbd&imgtype=jpg&src=http%3A%2F%2Fimg4.imgtn.bdimg.com%2Fit%2Fu%3D1277493845%2C86945667%26fm%3D214%26gp%3D0.jpg\",\"title\":\"2014-07-02 22:05 工具上线\",\"content\":\"2016-11-16 14:13 增加php, go类生成.2016-11-19 01:17 增加java类生成\"}," +
                "{\"thumnail\":\"https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=928861582,28679776&fm=23&gp=0.jpg\",\"title\":\"2014-07-02 22:05 工具上线\",\"content\":\"2016-11-16 14:13 增加php, go类生成.2016-11-19 01:17 增加java类生成\"}," +
                "{\"thumnail\":\"https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=3001557659,2023085306&fm=11&gp=0.jpg\",\"title\":\"2014-07-02 22:05 工具上线\",\"content\":\"2016-11-16 14:13 增加php, go类生成.2016-11-19 01:17 增加java类生成\"}," +
                "{\"thumnail\":\"https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=2044437122,3308405600&fm=23&gp=0.jpg\",\"title\":\"2014-07-02 22:05 工具上线\",\"content\":\"2016-11-16 14:13 增加php, go类生成.2016-11-19 01:17 增加java类生成\"}," +
                "{\"thumnail\":\"https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2506235241,3279966129&fm=23&gp=0.jpg\",\"title\":\"2014-07-02 22:05 工具上线\",\"content\":\"2016-11-16 14:13 增加php, go类生成.2016-11-19 01:17 增加java类生成\"}," +
                "{\"thumnail\":\"https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=3442436014,3926526038&fm=11&gp=0.jpg\",\"title\":\"2014-07-02 22:05 工具上线\",\"content\":\"2016-11-16 14:13 增加php, go类生成.2016-11-19 01:17 增加java类生成\"}," +
                "{\"thumnail\":\"http://pheker.cn/ssm/static_this_is_error_picture_address_usedtotest.jpg\",\"title\":\"2014-07-02 22:05 工具上线\",\"content\":\"2016-11-16 14:13 增加php, go类生成.2016-11-19 01:17 增加java类生成\"}" +
                "]";
    }


}
