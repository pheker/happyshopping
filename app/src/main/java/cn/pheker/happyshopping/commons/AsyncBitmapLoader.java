package cn.pheker.happyshopping.commons;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.ImageView;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.ref.SoftReference;
import java.util.HashMap;


/**
 * Created by cn.pheker on 2017/3/30.
 * Mail to hkbxoic@gmail.com
 */

public class AsyncBitmapLoader {

    private static final String TAG = "AsyncBitmapLoader";

    private Activity activity;
    //存储路径
    private final String fileCachePath;
    private final String appPath;


    /**
     * 内存图片软引用缓冲
     */
    private HashMap<String, SoftReference<Bitmap>> imageCache = null;

    public AsyncBitmapLoader(Activity activity){
        this.activity = activity;
        appPath = activity.getFilesDir().getPath();
        fileCachePath = appPath+"/image_cache/";
        Log.d(TAG, "AsyncBitmapLoader() called with: activity = [" + activity + "]");
        imageCache = new HashMap<>();
    }

    public Bitmap loadBitmap(final ImageView imageView, final String imageURL,
                             final ImageCallBack imageCallBack){
        //在内存缓存中，则返回Bitmap对象
        if(imageCache.containsKey(imageURL)){
            SoftReference<Bitmap> reference = imageCache.get(imageURL);
            Bitmap bitmap = reference.get();
            if(bitmap != null){
                return bitmap;
            }
        }
        else
        {
            /**
             * 加上一个对本地缓存的查找
             */
            String bitmapName = imageURL.substring(imageURL.lastIndexOf("/") + 1);
            File cacheDir = new File(fileCachePath);
            File[] cacheFiles = cacheDir.listFiles();
            int i = 0;
            if(null!=cacheFiles){
                for(; i<cacheFiles.length; i++){
                    if(bitmapName.equals(cacheFiles[i].getName())) break;
                }
                if(i < cacheFiles.length){
                    return BitmapFactory.decodeFile(fileCachePath + bitmapName);
                }
            }
        }

        final Handler handler = new Handler()
        {
            /* (non-Javadoc)
             * @see android.os.Handler#handleMessage(android.os.Message)
             */
            @Override
            public void handleMessage(Message msg)
            {
                imageCallBack.imageLoad(imageView, (Bitmap)msg.obj);
            }
        };

        //如果不在内存缓存中，也不在本地（被jvm回收掉），则开启线程下载图片
        new Thread()
        {
            /* (non-Javadoc)
             * @see java.lang.Thread#run()
             */
            @Override
            public void run()
            {
                InputStream bitmapIs = UtilStream.getStreamFromURL(imageURL);
                Log.i(TAG, "run: "+bitmapIs);
//                Toast.makeText(activity, imageURL, Toast.LENGTH_SHORT).show();
                Bitmap bitmap = BitmapFactory.decodeStream(bitmapIs);
                imageCache.put(imageURL, new SoftReference<Bitmap>(bitmap));
                Message msg = handler.obtainMessage(0, bitmap);
                handler.sendMessage(msg);

                File dir = new File(fileCachePath);
                if(!dir.exists()){
                    dir.mkdirs();
                }

                File bitmapFile = new File(fileCachePath+imageURL.substring(imageURL.lastIndexOf("/") + 1));
                if(!bitmapFile.exists()){
                    try{
                        UtilPermission.verifyStoragePermissions(activity);
                        bitmapFile.createNewFile();
                    } catch (IOException e){
                        Log.e(TAG, "run: ", new Throwable(e.getMessage()));
                    }
                }
//                Log.d(TAG, bitmapFile.getAbsolutePath());
                FileOutputStream fos;
                if(bitmap != null){
                    try{
                        fos = new FileOutputStream(bitmapFile);
                        bitmap.compress(Bitmap.CompressFormat.PNG,100, fos);
                        fos.close();
                    }catch (FileNotFoundException e){
                        Log.e(TAG, "run: ", new Throwable(e.getMessage()));
                    }catch (IOException e){
                        Log.e(TAG, "run: ", new Throwable(e.getMessage()));
                    }
                }
            }
        }.start();

        return null;
    }

    public interface ImageCallBack{
         void imageLoad(ImageView imageView, Bitmap bitmap);
    }

}
