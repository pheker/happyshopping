package cn.pheker.happyshopping.commons;

import android.content.Context;
import android.content.SharedPreferences;

import static android.content.Context.MODE_PRIVATE;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/3
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description
 *
 * </pre>
 */

public class UtilSP {

    private static SharedPreferences sp;
    public static SharedPreferences getSP(Context context){
        if(sp==null){
            sp = context.getSharedPreferences(Constants.SP_CONFIG,MODE_PRIVATE);
        }
        return sp;
    }

    public static SharedPreferences.Editor getEditor(Context context){
        return getSP(context).edit();
    }

    /**
     * 保存数据到文件
     * @param context
     * @param key
     * @param data
     */
    public static void put(Context context, String key,Object data){

        String type = data.getClass().getSimpleName();
        SharedPreferences.Editor editor = getEditor(context);
        if ("Integer".equals(type)){
            editor.putInt(key, (Integer)data);
        }else if ("Boolean".equals(type)){
            editor.putBoolean(key, (Boolean)data);
        }else if ("String".equals(type)){
            editor.putString(key, (String)data);
        }else if ("Float".equals(type)){
            editor.putFloat(key, (Float)data);
        }else if ("Long".equals(type)){
            editor.putLong(key, (Long)data);
        }

        editor.commit();
    }

    /**
     * 从文件中读取数据
     * @param context
     * @param key
     * @param defValue
     * @return
     */
    public static Object get(Context context, String key, Object defValue){

        String type = defValue.getClass().getSimpleName();
        SharedPreferences sharedPreferences = getSP(context);

        //defValue为为默认值，如果当前获取不到数据就返回它
        if ("Integer".equals(type)){
            return sharedPreferences.getInt(key, (Integer)defValue);
        }else if ("Boolean".equals(type)){
            return sharedPreferences.getBoolean(key, (Boolean)defValue);
        }else if ("String".equals(type)){
            return sharedPreferences.getString(key, (String)defValue);
        }else if ("Float".equals(type)){
            return sharedPreferences.getFloat(key, (Float)defValue);
        }else if ("Long".equals(type)){
            return sharedPreferences.getLong(key, (Long)defValue);
        }
        return null;
    }

    /* 清空数据 */
    public static void clearData(Context context) {
        getEditor(context).clear().commit();
    }




}
