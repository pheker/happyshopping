package cn.pheker.happyshopping.commons;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/8
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description
 *
 * </pre>
 */

public class UtilReflect {

    /*执行某个对象的方法*/
    public static Object invokeMethod(Object owner, String methodName, Object[] args) {
        Class ownerClass = owner.getClass();

        Class[] argsClass = new Class[args.length];

        for (int i = 0, j = args.length; i < j; i++) {

            argsClass[i] = args[i].getClass();

        }

        Method method = null;
        Object returnObj = null;
        try {
            method = ownerClass.getMethod(methodName, argsClass);
            returnObj = method.invoke(owner, args);
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        } catch (NoSuchMethodException e) {
            e.printStackTrace();
        }
        return returnObj;
    }
}
