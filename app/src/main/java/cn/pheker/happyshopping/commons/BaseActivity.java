package cn.pheker.happyshopping.commons;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.widget.Toast;

/**
 * 自己的Activity需要extends BaseActivity
 * 让BaseActivity来管理所有的Activity以及所有的Activity公共的一些操作
 */
public abstract class BaseActivity extends FragmentActivity {

    protected static final String TAG = "BaseActivity";
    public void showToast(Context context, String msg){
        Toast.makeText(context, msg, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        /* 显示当前Activity名称 */
        Log.d(TAG, getClass().getSimpleName());
        UtilActivity.addActivity(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        UtilActivity.removeActivity(this);
    }



    //上一次按下返回键的时间戳
    protected long mLastBackKeyPressTime;

    //退出逻辑
    protected void exit() {
        if(System.currentTimeMillis()-mLastBackKeyPressTime>1000){
            Toast.makeText(this, "再按一次退出", Toast.LENGTH_SHORT).show();
            mLastBackKeyPressTime = System.currentTimeMillis();
        }else{
            finish();
            System.exit(0);
        }
    }


    /**
     * 初始化视图
     */
    protected abstract void initView();

    /**
     * 初始化事件
     */
    protected abstract void initEvent();
}
