package cn.pheker.happyshopping.commons;

import android.content.Context;
import android.util.Log;

import java.util.Map;
import java.util.Set;

import cn.pheker.happyshopping.entities.HsUser;

import static cn.pheker.happyshopping.commons.UtilGson.fromJson;
import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/3
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description 登录工具类
 *
 * </pre>
 */

public class UtilLogin {

    /* 检测登录状态 */
    public static boolean  hasLogin(Context context){
        return (boolean) UtilSP.get(context, "hasLogin",false);
    }

    /* 设置为已登录状态 */
    public static void setLogin(Context context) {
        UtilSP.put(context,"hasLogin",true);
    }
    /* 设置为未登录状态 */
    public static void setUnLogin(Context context) {
        UtilSP.put(context,"hasLogin",false);
    }

    /* 保存用户登录信息 */
    public static void saveHsUser(Context context,String userUUID,String hsUser) {
        UtilSP.put(context,"useruuid_"+userUUID,hsUser);
        UtilSP.put(context,"loginUUID",userUUID);
    }
    /* 保存用户登录信息 */
    public static void saveHsUser(Context context,String userUUID,HsUser hsUser) {
        UtilSP.put(context,"useruuid_"+userUUID,UtilGson.toJson(hsUser));
        UtilSP.put(context,"loginUUID",userUUID);
    }

    /* 获取用户登录信息 */
    public static HsUser getHsUser(Context context) {
        String userUUID  = getLoginHsUserUuid(context);
        return UtilGson.fromJson((String) UtilSP.get(context, "useruuid_"+userUUID,""),HsUser.class);
    }
    public static HsUser getHsUser(Context context, String userUUID) {
        if(userUUID==null) {
            userUUID = getLoginHsUserUuid(context);
        }
        return fromJson((String) UtilSP.get(context, "useruuid_"+userUUID,""),HsUser.class);
    }

    public static String getLoginHsUserUuid(Context context) {
        String uuid = (String) UtilSP.get(context,"loginUUID","");
        if ("".equals(uuid)) {
            Map<String, ?> all = UtilSP.getSP(context).getAll();
            Set<String> keys = all.keySet();
            for (String key : keys) {
                Log.i(TAG, "getLoginHsUserUuid: "+key);
                Log.i(TAG, "getLoginHsUserUuid: "+all.get(key));
                if (key.startsWith("useruuid")) {
                    uuid = key;
                    break;
                }
            }
        }
        return uuid;
    }


    /**
     * 处理单张图片路径
     * @param picPath 图片路径
     * @return
     */
    public static String dealPicPath(String picPath) {
        return picPath.startsWith("http://") ? picPath : Constants.Url.SERVER_ADDRESS + picPath;
    }

    /**
     * 处理多张图片路径
     * @param picsPath 图片路径string 分割符为,
     * @return
     */
    public static String[] dealPicsPath(String picsPath) {
        String[] picsPathArr = picsPath.split(",");
        for (int i = 0; i < picsPathArr.length; i++) {
            picsPathArr[i] = dealPicPath(picsPathArr[i]);
        }
        return picsPathArr;
    }
}
