package cn.pheker.happyshopping.commons.ui_usetime_check;

import android.os.Looper;
import android.util.Printer;

/**
 * Created by cn.pheker on 2017/4/7.
 * Mail to hkbxoic@gmail.com
 *
 * 用于检测UI是否有卡顿,解决之后才能拥有丝滑的界面
 * @link http://blog.csdn.net/lmj623565791/article/details/58626355
 * 在onCreate中使用:
 *      BlockDetectByPrinter.start();
 * 如果检测到有卡顿(类似如下):
 *      02-21 00:26:26.408 2999-3014/com.zhy.testlp E/TAG:
             java.lang.VMThread.sleep(Native Method)
             java.lang.Thread.sleep(Thread.java:1013)
             java.lang.Thread.sleep(Thread.java:995)
             com.zhy.testlp.MainActivity$2.onClick(MainActivity.java:70)
             android.view.View.performClick(View.java:4438)
             android.view.View$PerformClick.run(View.java:18422)
             android.os.Handler.handleCallback(Handler.java:733)
             android.os.Handler.dispatchMessage(Handler.java:95)
 *
 */

public class BlockDetectByPrinter {


    public static void start() {

        Looper.getMainLooper().setMessageLogging(new Printer() {

            private static final String START = ">>>>> Dispatching";
            private static final String END = "<<<<< Finished";

            @Override
            public void println(String x) {
                if (x.startsWith(START)) {
                    LogMonitor.getInstance().startMonitor();
                }
                if (x.startsWith(END)) {
                    LogMonitor.getInstance().removeMonitor();
                }
            }
        });

    }
}