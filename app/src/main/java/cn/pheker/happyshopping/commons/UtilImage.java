package cn.pheker.happyshopping.commons;

import android.annotation.TargetApi;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.util.Log;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import static android.content.ContentValues.TAG;

/**
 * Created by cn.pheker on 2017/3/30.
 * Mail to hkbxoic@gmail.com
 */

public class UtilImage {

    /**
     * 获取文件大小 
     * @param f
     * @return
     * @throws Exception
     */
    public static long getFileSizes(File f) throws Exception
    {
        long s = 0;
        if (f.exists())
        {
            FileInputStream fis = null;
            fis = new FileInputStream(f);
            s = fis.available();
        }
        else
        {
            f.createNewFile();
            System.out.println("文件不存在");
        }
        return s;
    }
    public static String DownloadImage(String imguri,String imgname)
    {
        String imagePath = null;
        URL url;
        byte[] bArr=null;
        try {
            url = new URL(imguri);   //设置URL  
            HttpURLConnection con;
            con = (HttpURLConnection)url.openConnection();  //打开连接  
            con.setRequestMethod("GET"); //设置请求方法  
            //设置连接超时时间为5s  
            con.setConnectTimeout(5000);
            InputStream in=con.getInputStream();  //取得字节输入流  
            bArr = readInputStream(in);
            Log.v("Save","getbyte");
            int width = 100,height = 100;//自己定
            Bitmap bitmap=decodeSampledBitmapFromStream(bArr,width-20,height-20);
            try
            {
                imagePath = saveJPGFile(bitmap, imgname,null);
                Log.d(TAG, "DownloadImage: "+imagePath);
                Log.v("Downloadimage","保存图片成功");
            }
            catch(Exception e)
            {
                Log.v("getimage","保存图片失败");
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
        return imagePath;
    }

    /**
     *
     * @param bm
     *            图片的bitmap 
     * @param fileName
     *            文件名 
     * @param folderName
     *            文件夹名 
     * @throws IOException
     */
    public static String saveJPGFile(Bitmap bm,String fileName,String folderName)
            throws IOException {
        if(folderName==null||"".equals(folderName)) folderName = "pheker_cache";
        String path = folderName + "/";
        File dirFile = new File(path);
        // 文件夹不存在则创建文件夹  
        if (!dirFile.exists()) {
            dirFile.mkdirs();
        }
        Log.v("保存文件函数", "创建文件夹成功");
        File myCaptureFile = new File(path + fileName + ".jpg");
        Log.v("保存文件函数", "文件路径");

        try{
            BufferedOutputStream bos = new BufferedOutputStream(
                    new FileOutputStream(myCaptureFile));
            Log.v("保存文件函数", "文件流");
            bm.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            Log.v("保存文件函数", "保存成功");
            bos.flush();
            bos.close();

            if(bm.isRecycled()==false)
            {
                bm.recycle();
                Log.v("Util","回收bitmap");
            }
        }catch(Exception e){

        }
        return myCaptureFile.getAbsolutePath();
    }

    public static byte[] readInputStream(InputStream in) throws Exception{
        int len=0;
        byte buf[]=new byte[1024];
        ByteArrayOutputStream out=new ByteArrayOutputStream();
        while((len=in.read(buf))!=-1){
            out.write(buf,0,len);  //把数据写入内存  
        }
        out.close();  //关闭内存输出流  
        return out.toByteArray(); //把内存输出流转换成byte数组  
    }

    public static Bitmap decodeSampledBitmapFromStream(byte[] b, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeByteArray(b, 0, b.length, options);
        // 调用上面定义的方法计算inSampleSize值    
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
        // 使用获取到的inSampleSize值再次解析图片    
        options.inJustDecodeBounds = false;
        Log.v("decode","返回bitmap");
        return BitmapFactory.decodeByteArray(b, 0, b.length, options);
    }
    private static int calculateInSampleSize(BitmapFactory.Options options,
                                      int reqWidth, int reqHeight) {
        // 源图片的高度和宽度    
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            // 计算出实际宽高和目标宽高的比率    
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            // 选择宽和高中最小的比率作为inSampleSize的值，这样可以保证最终图片的宽和高    
            // 一定都会大于等于目标的宽和高。    
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        Log.v("calculate"," "+inSampleSize);
        return inSampleSize;
    }



    /**
     * 裁剪原始的图片
     */
    public static Intent cropRawPhoto(Uri uri, int output_X, int output_Y) {

        Intent intent = new Intent("com.android.camera.action.CROP");
        intent.setDataAndType(uri, "image/*");

        // 设置裁剪
        intent.putExtra("crop", "true");

        // aspectX , aspectY :宽高的比例
        intent.putExtra("aspectX", 1);
        intent.putExtra("aspectY", 1);

        // outputX , outputY : 裁剪图片宽高
        intent.putExtra("outputX", output_X);
        intent.putExtra("outputY", output_Y);
        intent.putExtra("return-data", true);

        return intent;
    }

    /**
     * 获取裁剪程序Intent
     * @param imgUri
     * @return
     */
    public static Intent startCropPhotoActivity(Uri imgUri){
        // 设置intent为启动裁剪程序
        Intent intent = new Intent("com.android.camera.action.CROP");
        // 设置Data为刚才的imageUri和Type为图片类型
        intent.setDataAndType(imgUri, "image/*");
        // 设置可缩放
        intent.putExtra("scale", true);
        //设置可以裁剪
        intent.putExtra("crop", true);
        // 设置输出地址为imageUri
        intent.putExtra(MediaStore.EXTRA_OUTPUT, imgUri);
        return intent;
    }

    /**
     * 检查设备是否存在SDCard的工具方法
     */
    public static boolean hasSdcard() {
        String state = Environment.getExternalStorageState();
        if (state.equals(Environment.MEDIA_MOUNTED)) {
            // 有存储的SDCard
            return true;
        } else {
            return false;
        }
    }

    /**
     * 将Bitmap压缩并保存到本地
     * @param photo
     * @param spath
     */
    public static boolean saveImage(Bitmap photo, String spath) {
        try {
            BufferedOutputStream bos = new BufferedOutputStream(
                    new FileOutputStream(spath, false));
            photo.compress(Bitmap.CompressFormat.JPEG, 100, bos);
            bos.flush();
            bos.close();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }


    /**
     * 4.4版本下从Intent中获取图片路径
     * 通过BitmapFactory转换成bitmap对象，发现不可行
     * @param context
     * @param data
     * @return
     */
    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String  handleImageOnKitkat(Context context,Intent data) {
        String imagePath = null;
        Uri uri = data.getData();
        if (DocumentsContract.isDocumentUri(context, uri)) {
            // 如果是document类型的Uri，则通过document id处理
            String docId = DocumentsContract.getDocumentId(uri);
            if ("com.android.providers.media.documents".equals(uri.getAuthority())) {
                String id = docId.split(":")[1]; // 解析出数字格式的id
                String selection = MediaStore.Images.Media._ID + "=" + id;
                imagePath = getImagePath(context,MediaStore.Images.Media.EXTERNAL_CONTENT_URI, selection);
            } else if ("com.android.providers.downloads.documents".equals(uri
                    .getAuthority())) {
                Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"),
                        Long.valueOf(docId));
                imagePath = getImagePath(context,contentUri, null);
            }
        } else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // 如果不是document类型的Uri，则使用普通方式处理
            imagePath = getImagePath(context,uri, null);
        }
        return imagePath;
    }

    public static String getImagePath(Context context,Uri uri, String selection) {
        String path = null;
        // 通过uri和selection来获取真实的图片路径
        Cursor cursor = context.getContentResolver().query(uri, null, selection, null,
                null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                path = cursor.getString(cursor
                        .getColumnIndex(MediaStore.Images.Media.DATA));
            }
            cursor.close();
        }
        return path;
    }



}
