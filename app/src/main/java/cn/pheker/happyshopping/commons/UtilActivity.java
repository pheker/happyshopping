package cn.pheker.happyshopping.commons;

import android.app.Activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author cn.pheker
 * @Email hkbxoic@gmail.com
 * @Date 2017-02-28
 * @Version v1.0.0
 * @Description Activity工具类，管理所有Activity以及常用操作
 *              可以让自己的Activity继承BaseActivity来自动实现
 *              或自己手动调用也可
 */

public class UtilActivity {

    /**
     *  Activity管理器
     */
    private static Map<String,Activity> activitiesManager = new HashMap<>();
    private static List<String> activitiesManagerKeys = new ArrayList<>();
    /* 常用操作 */
    public static  void addActivity(Activity activity){
        String key = activity.getClass().getName();
        activitiesManager.put(key,activity);
    }

    public static void removeActivity(Activity activity){
        String key = activity.getClass().getName();
        activitiesManager.remove(key);
        activitiesManagerKeys.remove(key);
    }
    public static void removeActivity(String activityClassName){
        activitiesManager.remove(activityClassName);
        activitiesManagerKeys.remove(activityClassName);
    }
    public static void removeActivities(String[] activityClassNames){
        for (String activityClassName : activityClassNames) {
            activitiesManager.remove(activityClassName);
            activitiesManagerKeys.remove(activityClassName);
        }
    }

    public static Activity getActivity(int position) {
        Activity activity;
        if (position < 0 || position > size() - 1) {
            activity = null;
        } else {
            activity = activitiesManager.get(activitiesManagerKeys.get(position));
        }
        return activity;
    }

    /**
     * 结束所有Activity，这意味着在任何地方只要调用此方法就可以退出系统
     */
    public static void finishAll(){
        for (String activityClassName:activitiesManager.keySet()){
            Activity activity = activitiesManager.get(activityClassName);
            if (!activity.isFinishing()){
                activity.finish();
            }
        }
        activitiesManager.clear();
        activitiesManagerKeys.clear();
    }

    public static int size(){
        return activitiesManager.size();
    }


    /**
     * 通过className 获取position
     * */
    public static int getPosition(String className) {
        int size = activitiesManagerKeys.size();
        int position = -1;
        for (int i = 0; i <size; i++) {
            if(activitiesManagerKeys.get(i).equals(className)){
               position = i;
                break;
            }
        }
        return position;
    }

}
