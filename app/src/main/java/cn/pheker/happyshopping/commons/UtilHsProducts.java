package cn.pheker.happyshopping.commons;

import android.content.Context;
import android.widget.Toast;

import com.google.gson.reflect.TypeToken;
import com.zhy.http.okhttp.OkHttpUtils;
import com.zhy.http.okhttp.callback.StringCallback;

import java.util.List;
import java.util.Map;

import cn.pheker.happyshopping.entities.HsProduct;
import cn.pheker.happyshopping.entities.RetrieveDataByPage;
import okhttp3.Call;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/12
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description  商品分页获取工具类
 *
 * </pre>
 */

public class UtilHsProducts {

    public static void getRemoteData(final Context context, final Map<String,String> params, final RemoteDataCallback rdcb) {
        OkHttpUtils.get()
            .params(params)
            .url(Constants.Url.HSPRODUCT_BYPAGE_PATH)
            .build()
            .execute(new StringCallback() {
                @Override
                public void onError(Call call, Exception e, int id) {
                    Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                }
                @Override
                public void onResponse(final String response, int id) {
                    RetrieveDataByPage<List<HsProduct>> data = UtilGson.fromJson(response,
                            new TypeToken<RetrieveDataByPage<List<HsProduct>>>() {}.getType());
//                    RetrieveDataByPage<List<HsProduct>> data =  JSON.parseObject(response,new TypeReference<RetrieveDataByPage<List<HsProduct>>>(){}.getType());
                    int code = data.getCode();
                    if (code == 0) {
                        Toast.makeText(context, data.getMsg(), Toast.LENGTH_SHORT).show();
                    } else if (code == 1) {//表明已获取到数据
                        rdcb.dealData(data);
                    }
                }
            });//OkHttpUtils end
    }

    public static void getRemoteDataWithCondition(final Context context, final Map<String,String> params, final RemoteDataCallback rdcb) {
        OkHttpUtils.get()
                .params(params)
                .url(Constants.Url.HSPRODUCT_BYPAGE_CONDITION_PATH)
                .build()
                .execute(new StringCallback() {
                    @Override
                    public void onError(Call call, Exception e, int id) {
                        Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    @Override
                    public void onResponse(final String response, int id) {
                        RetrieveDataByPage<List<HsProduct>> data = UtilGson.fromJson(response,
                                new TypeToken<RetrieveDataByPage<List<HsProduct>>>() {}.getType());
//                    RetrieveDataByPage<List<HsProduct>> data =  JSON.parseObject(response,new TypeReference<RetrieveDataByPage<List<HsProduct>>>(){}.getType());
                        int code = data.getCode();
                        if (code == 0) {
                            Toast.makeText(context, data.getMsg(), Toast.LENGTH_SHORT).show();
                        } else if (code == 1) {//表明已获取到数据
                            rdcb.dealData(data);
                        }
                    }
                });//OkHttpUtils end
    }

    /*自定义回调接口*/
    public interface RemoteDataCallback  {
        public void dealData(RetrieveDataByPage<List<HsProduct>> dataByPage);
    }


}
