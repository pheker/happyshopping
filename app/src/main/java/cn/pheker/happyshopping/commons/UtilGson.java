package cn.pheker.happyshopping.commons;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/5
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description
 *
 * </pre>
 */

public class UtilGson {
    private static Gson gson;
    public static Gson getInstance(){
        if(gson==null){
            gson = new GsonBuilder()
//                    .setDateFormat("yyyy-MM-dd HH:mm:ss").registerTypeAdapter(Date.class, new JsonDeserializer<Date>() {
//                public Date deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
//                    return new Date(json.getAsJsonPrimitive().getAsLong());
//                }
//            }).create();
            .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .create();
            //此种方式,不会报Caused by: java.lang.IndexOutOfBoundsException: Invalid time zone indicator ' '
        }
        return gson;
    }

    public static <T> T fromJson(String json,Class<T> clzz){
        return getInstance().fromJson(json,clzz);
    }

    public static <T> T fromJson(String json,Type typeOfT){
        return getInstance().fromJson(json,typeOfT);
    }

    public static String toJson(Object src){
        return getInstance().toJson(src);
    }

}
