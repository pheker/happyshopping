package cn.pheker.happyshopping.adapter;

import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import cn.pheker.happyshopping.R;

/**
 * Created by cn.pheker on 2017/4/3.
 * Mail to hkbxoic@gmail.com
 */

public class GuidePagerAdapter extends PagerAdapter {

    private int[] titles = new int[]{/*R.mipmap.cartoon_001,R.mipmap.cartoon_002,R.mipmap.cartoon_003,*/
            R.drawable.ic_guide_one,R.drawable.ic_guide_two,R.drawable.ic_guide_three,R.drawable.ic_guide_four};
    private List<View> views = new ArrayList<>();

    public GuidePagerAdapter() {
        super();
    }

    public int getCount() {
        return views.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        container.addView(views.get(position));
        return views.get(position);
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView(views.get(position));
    }

    public List<View> getViews(){
        return views;
    }

    public int[] getTitles() {
        return titles;
    }
}
