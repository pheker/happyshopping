package cn.pheker.happyshopping.adapter;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bm.library.PhotoView;

import java.util.List;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.AsyncBitmapLoader;
import cn.pheker.happyshopping.entities.Product;

/**
 * Created by cn.pheker on 2017/5/1.
 * Mail to hkbxoic@gmail.com
 * 商品列表适配器
 */
public class ProductAdapter extends BaseAdapter{

    private List<Product> data;
    private LayoutInflater layoutInflater;
    private Activity activity;

    //异步图片加载器
    private AsyncBitmapLoader asyncBitmapLoader;
    //自定义ListView Item 点击回调函数
    private CustomListViewItemCallback mCustomListViewItemCallback;

    //构造函数

    /**
     * 自定义ListView 适配器
     * @param activity              Activity
     * @param data                  数据
     */
    public ProductAdapter(Activity activity, List<Product> data){
        this.activity = activity;
        this.data = data;

        this.layoutInflater = LayoutInflater.from(activity.getApplicationContext());
        this.asyncBitmapLoader = new AsyncBitmapLoader(activity);
    }

    /**
     * 自定义ListView 适配器
     * @param activity              Activity
     * @param data                  数据
     * @param itemCallback          Item点击回调函数
     */
    public ProductAdapter(Activity activity, List<Product> data,
                          CustomListViewItemCallback itemCallback){
        this.activity = activity;
        this.data = data;
        this.mCustomListViewItemCallback = itemCallback;

        this.layoutInflater = LayoutInflater.from(activity.getApplicationContext());
        this.asyncBitmapLoader = new AsyncBitmapLoader(activity);
    }


    public final class ListItem{
//        public ImageView thumnail;
        public PhotoView thumnail;
        public TextView title;
        public TextView content;
        public ImageView itemdetail;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ListItem listItem = null;
        if(convertView==null){
            listItem = new ListItem();
            //获得组件，并实例化组件
            convertView = layoutInflater.inflate(R.layout.custom_list_item,null);
            listItem.thumnail = (PhotoView) convertView.findViewById(R.id.item_thumnail);
            listItem.title = (TextView)convertView.findViewById(R.id.item_title);
            listItem.content = (TextView)convertView.findViewById(R.id.item_content);
            listItem.itemdetail = (ImageView) convertView.findViewById(R.id.item_detail);
            convertView.setTag(listItem);
        }else{
            listItem = (ListItem) convertView.getTag();
        }

        //绑定数据
        Product product = data.get(position);
//        ImageView image = listItem.thumnail;
        PhotoView image = (PhotoView) listItem.thumnail;
        image.enable();
        Bitmap bitmap = asyncBitmapLoader.loadBitmap(image, product.getPicpath(),
                new AsyncBitmapLoader.ImageCallBack() {
                    @Override
                    public void imageLoad(ImageView imageView, Bitmap bitmap) {
                        if (imageView != null) {
                            imageView.setImageBitmap(bitmap);
                        }
                    }
                }
        );
        if (bitmap == null) {
            image.setImageResource(R.drawable.load_fail);
        } else {
            image.setImageBitmap(bitmap);
        }

        listItem.title.setText(product.getTitle());
        listItem.content.setText(product.getContent());

//        convertView.setOnClickListener(this);

        return convertView;
    }



    /**
     * 自定义ListView Item 点击事件回调接口
     */
    public interface CustomListViewItemCallback{
        void click(View v, int position);
    }


}//--CustomListViewAdapter end


/*
    http://www.cnblogs.com/ivan-xu/p/4124967.html
    在Activity中响应ListView内部按钮的点击事件
    最近交流群里面有人问到一个问题：如何在Activity中响应ListView内部按钮的点击事件，不要在Adapter中响应？

    对于这个问题，我最初给他的解答是，在Adapter中定义一个回调接口，在Activity中实现该接口，从而实现对点击事件的响应。

    下班后思考了一下，觉得有两种方式都能比较好的实现：使用接口回调和使用抽象类回调。

    两种方式的区别在于:
        抽象类在Activity中实现的时候，只能定义一个成员变量来实现，不能由Activity直接实现，因为Java不支持多继承。
        而接口既可以由Activity直接实现，也可以由其成员变量来实现。
* */