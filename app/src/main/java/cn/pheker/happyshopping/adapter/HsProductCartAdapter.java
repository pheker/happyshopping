package cn.pheker.happyshopping.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.bm.library.PhotoView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.HashMap;
import java.util.List;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.entities.HsProduct;

/**
 * Created by cn.pheker on 2017/5/1.
 * Mail to hkbxoic@gmail.com
 * 商品列表适配器
 */
public class HsProductCartAdapter extends BaseAdapter{

    private static final String TAG = "HsProductCartAdapter";

    private List<HsProduct> data;
    private LayoutInflater layoutInflater;
    private Activity activity;
    // 用来控制CheckBox的选中状况
    private static HashMap<Integer, Boolean> isSelected = new HashMap<>();

    //自定义ListView Item 点击回调函数
    private CustomListViewItemCallback mCustomListViewItemCallback;
    private CheckboxClickCallback mCheckboxClickCallback;

    //构造函数

    /**
     * 自定义ListView 适配器
     * @param activity              Activity
     * @param data                  数据
     */
    public HsProductCartAdapter(Activity activity, List<HsProduct> data){
        this.activity = activity;
        this.data = data;
        this.layoutInflater = LayoutInflater.from(activity.getApplicationContext());
        initCheckBox();
    }

    /**
     * 自定义ListView 适配器
     * @param activity              Activity
     * @param data                  数据
     */
    public HsProductCartAdapter(Activity activity, List<HsProduct> data,CheckboxClickCallback checkboxClickCallback){
        this.activity = activity;
        this.data = data;
        this.mCheckboxClickCallback = checkboxClickCallback;
        this.layoutInflater = LayoutInflater.from(activity.getApplicationContext());
        initCheckBox();
    }


    /**
     * 自定义ListView 适配器
     * @param activity              Activity
     * @param data                  数据
     * @param itemCallback          Item点击回调函数
     */
    public HsProductCartAdapter(Activity activity, List<HsProduct> data,
                                CustomListViewItemCallback itemCallback){
        this.activity = activity;
        this.data = data;
        this.mCustomListViewItemCallback = itemCallback;
        this.layoutInflater = LayoutInflater.from(activity.getApplicationContext());
        initCheckBox();
    }


    public static final class ListItem{
        public CheckBox cb;
        //        public ImageView thumnail;
        public PhotoView thumnail;
        public TextView title;
        public TextView content;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        ListItem listItem;
        if(convertView==null){
            listItem = new ListItem();
            //获得组件，并实例化组件
            convertView = layoutInflater.inflate(R.layout.cart_product_list_item,null);
            listItem.cb = (CheckBox) convertView.findViewById(R.id.item_cb);
            listItem.cb.setTag(position);
            listItem.thumnail = (PhotoView) convertView.findViewById(R.id.item_thumnail);
            listItem.title = (TextView)convertView.findViewById(R.id.item_title);
            listItem.content = (TextView)convertView.findViewById(R.id.item_content);
            convertView.setTag(listItem);
        }else{
            listItem = (ListItem) convertView.getTag();
        }

        //绑定数据
        HsProduct hsProduct = data.get(position);
        PhotoView image = listItem.thumnail;
        image.enable();
        /*使用Glide加载图片*/
        Glide.with(activity)
            .load(hsProduct.getPicspath())
            .crossFade()
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .placeholder(R.drawable.img_loading)
            .error(R.drawable.loading_fail)
            .into(image);
        listItem.title.setText(hsProduct.getName());
        listItem.content.setText(hsProduct.getDescription());
        // 根据isSelected来设置checkbox的选中状况
        boolean isTrue = getIsSelected().get(position);
        Log.i(TAG, "getView: "+isTrue);
        listItem.cb.setChecked(isTrue);// todo NPE错误
        listItem.cb.setOnClickListener(mCheckboxClickCallback);
//        convertView.setOnClickListener(this);

        return convertView;
    }


    // 初始化isSelected的数据
    private void initCheckBox() {
        for (int i = 0; i < data.size(); i++) {
            getIsSelected().put(i, false);
        }
    }
    public static HashMap<Integer, Boolean> getIsSelected() {
        return isSelected;
    }

    public static void setIsSelected(HashMap<Integer, Boolean> isSelected) {
        HsProductCartAdapter.isSelected = isSelected;
    }



    /**
     * 自定义ListView Item 点击事件回调接口
     */
    public interface CustomListViewItemCallback{
        void click(View v, int position);
    }

    public interface CheckboxClickCallback extends  View.OnClickListener{
        @Override
        void onClick(View v);
    }


}//--CustomListViewAdapter end


/*
    http://www.cnblogs.com/ivan-xu/p/4124967.html
    在Activity中响应ListView内部按钮的点击事件
    最近交流群里面有人问到一个问题：如何在Activity中响应ListView内部按钮的点击事件，不要在Adapter中响应？

    对于这个问题，我最初给他的解答是，在Adapter中定义一个回调接口，在Activity中实现该接口，从而实现对点击事件的响应。

    下班后思考了一下，觉得有两种方式都能比较好的实现：使用接口回调和使用抽象类回调。

    两种方式的区别在于:
        抽象类在Activity中实现的时候，只能定义一个成员变量来实现，不能由Activity直接实现，因为Java不支持多继承。
        而接口既可以由Activity直接实现，也可以由其成员变量来实现。
* */