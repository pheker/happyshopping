package cn.pheker.happyshopping.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.entities.HsReceiveAddress;

/**
 * Created by cn.pheker on 2017/5/1.
 * Mail to hkbxoic@gmail.com
 * 商品列表适配器
 */
public class HsReceiveAddressAdapter extends BaseAdapter{

    private List<HsReceiveAddress> data;
    private LayoutInflater layoutInflater;
    private Activity activity;

    //构造函数
    /**
     * 自定义ListView 适配器
     * @param activity              Activity
     * @param data                  数据
     */
    public HsReceiveAddressAdapter(Activity activity, List<HsReceiveAddress> data){
        this.activity = activity;
        this.data = data;
        this.layoutInflater = LayoutInflater.from(activity.getApplicationContext());
    }


    public final class ListItem{
//        public ImageView thumnail;
        public TextView alias;
        public TextView address;
        public TextView name;
        public TextView phone;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ListItem listItem = null;
        if(convertView==null){
            listItem = new ListItem();
            //获得组件，并实例化组件
            convertView = layoutInflater.inflate(R.layout.item_receive_address,null);
            listItem.alias = (TextView)convertView.findViewById(R.id.position_alias);
            listItem.address = (TextView)convertView.findViewById(R.id.position_address);
            listItem.name = (TextView)convertView.findViewById(R.id.item_receive_person_name);
            listItem.phone = (TextView)convertView.findViewById(R.id.item_receive_person_phone);
            convertView.setTag(listItem);
        }else{
            listItem = (ListItem) convertView.getTag();
        }

        //绑定数据
        HsReceiveAddress hsReceiveAddress = data.get(position);
        listItem.alias.setText(hsReceiveAddress.getReceiveAddressAlias());
        listItem.address.setText(hsReceiveAddress.getDeliveryAddress());
        listItem.name.setText(hsReceiveAddress.getReceivePersonName());
        listItem.phone.setText(hsReceiveAddress.getReceivePersonPhone());

//        convertView.setOnClickListener(this);

        return convertView;
    }



}//--CustomListViewAdapter end
