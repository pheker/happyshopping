package cn.pheker.happyshopping.adapter;

import android.database.DataSetObserver;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.entities.HsOrder;
import cn.pheker.happyshopping.entities.OrderGroup;
import cn.pheker.happyshopping.utils.DateUtil;

/**
 * <pre>
 * @user cn.pheker
 * @date 2017/5/22
 * @email hkbxoic@gmail.com
 * @version 1.0.0
 * @description
 *
 * </pre>
 */

public class BaseExpandableListAdapter implements ExpandableListAdapter {

    private FragmentActivity activity;
    private List<OrderGroup> mOrderGroups;
    private List<List<HsOrder>> mOrderItems;

    public BaseExpandableListAdapter(FragmentActivity activity,List<OrderGroup> mOrderGroups, List<List<HsOrder>> mOrderItems) {
        this.activity = activity;
        this.mOrderGroups = mOrderGroups;
        this.mOrderItems = mOrderItems;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {

    }

    @Override
    public int getGroupCount() {
        return mOrderGroups.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return mOrderItems.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return mOrderGroups.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return mOrderItems.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        View view=null;
        GroupHolder groupHolder = null;
        if(convertView!=null){
            view = convertView;
            groupHolder = (GroupHolder) view.getTag();
        }else{
            view = View.inflate(activity,R.layout.order_group, null);
            groupHolder = new GroupHolder();
            groupHolder.uuid = (TextView) view.findViewById(R.id.order_group_uuid);
            groupHolder.sumPrice = (TextView) view.findViewById(R.id.order_group_sumprice);
            groupHolder.delete = (TextView) view.findViewById(R.id.order_group_delete);
            view.setTag(groupHolder);
        }
        OrderGroup orderGroup = mOrderGroups.get(groupPosition);
        groupHolder.uuid.setText(orderGroup.getUuid());
        groupHolder.sumPrice.setText(orderGroup.getSumPrice());
        return view;
    }

    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        View view=null;
        ChildHolder childholder = null;
        if(convertView!=null){
            view = convertView;
            childholder = (ChildHolder) view.getTag();
        }else{
            view = View.inflate(activity,R.layout.order_item, null);
            childholder = new ChildHolder();
            childholder.mImage = (ImageView) view.findViewById(R.id.order_item_thumnail);
            childholder.mTitle = (TextView) view.findViewById(R.id.order_item_title);
            childholder.mNum = (TextView) view.findViewById(R.id.order_item_num);
            childholder.mPrice = (TextView) view.findViewById(R.id.order_item_price);
            childholder.mDate = (TextView) view.findViewById(R.id.order_item_date);
            view.setTag(childholder);
        }
        childholder.mImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(activity, "第"+groupPosition+"组的第"+childPosition+"圖標被点击了", Toast.LENGTH_SHORT).show();
            }
        });
        HsOrder hsOrder = mOrderItems.get(groupPosition).get(childPosition);
//        Glide.with(activity).load(R.drawable.order_gray);
        childholder.mTitle.setText(hsOrder.getUuid());
        childholder.mNum.setText(hsOrder.getNum()+"");
        childholder.mPrice.setText(hsOrder.getSumMoney()+"");
        childholder.mDate.setText(DateUtil.showAsQQFormat(DateUtil.getStrFromDate(hsOrder.getCreateTime())));

        return view;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return mOrderGroups.isEmpty();
    }

    @Override
    public void onGroupExpanded(int groupPosition) {

    }

    @Override
    public void onGroupCollapsed(int groupPosition) {

    }

    @Override
    public long getCombinedChildId(long groupId, long childId) {
        return 0;
    }

    @Override
    public long getCombinedGroupId(long groupId) {
        return 0;
    }





    static class GroupHolder{
        TextView uuid;
        TextView sumPrice;
        TextView delete;
    }

    static class ChildHolder{
        ImageView mImage;
        TextView mTitle;
        TextView mNum;
        TextView mPrice;
        TextView mDate;
    }

}
