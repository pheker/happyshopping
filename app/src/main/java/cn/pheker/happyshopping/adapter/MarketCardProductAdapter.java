package cn.pheker.happyshopping.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bm.library.PhotoView;
import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;

import java.util.List;

import cn.pheker.happyshopping.R;
import cn.pheker.happyshopping.commons.AsyncBitmapLoader;
import cn.pheker.happyshopping.entities.HsProduct;

import static com.bumptech.glide.gifdecoder.GifHeaderParser.TAG;

/**
 * Created by cn.pheker on 2017/3/30.
 * Mail to hkbxoic@gmail.com
 * 自定义ListView适配器
 */
public class MarketCardProductAdapter extends BaseAdapter implements OnClickListener {

    private List<HsProduct> data;
    private LayoutInflater layoutInflater;
    private Activity activity;



    private AsyncBitmapLoader asyncBitmapLoader;
    //自定义ListView Item 点击回调函数
    private CustomListViewItemCallback mCustomListViewItemCallback;
    //自定义ListView Item 缩略图thumnail 点击回调函数
    private ListViewThumnailClickListener mCustomListViewThumnailClickListener;
    //自定义ListView Item 图片detail 点击回调函数
    private CustomListViewItemDetailCallback mCustomListViewItemDetailCallback;

    //构造函数

    /**
     * 自定义ListView 适配器
     * @param activity              Activity
     * @param data                  数据
     */
    public MarketCardProductAdapter(Activity activity, List<HsProduct> data){
        this.activity = activity;
        this.data = data;

        this.layoutInflater = LayoutInflater.from(activity.getApplicationContext());
        this.asyncBitmapLoader = new AsyncBitmapLoader(activity);
    }

    /**
     * 自定义ListView 适配器
     * @param activity              Activity
     * @param data                  数据
     * @param itemCallback          Item点击回调函数
     */
    public MarketCardProductAdapter(Activity activity, List<HsProduct> data,
                                    CustomListViewItemCallback itemCallback){
        this.activity = activity;
        this.data = data;
        this.mCustomListViewItemCallback = itemCallback;

        this.layoutInflater = LayoutInflater.from(activity.getApplicationContext());
        this.asyncBitmapLoader = new AsyncBitmapLoader(activity);
    }

    /**
     * 自定义ListView 适配器
     * @param activity              Activity
     * @param data                  数据
     * @param itemCallback          Item点击回调函数
     * @param thumnailClickListener Item缩略图thumnail点击回调函数
     */
    public MarketCardProductAdapter(Activity activity, List<HsProduct> data,
                                    CustomListViewItemCallback itemCallback,
                                    ListViewThumnailClickListener thumnailClickListener){
        this.activity = activity;
        this.data = data;
        this.mCustomListViewItemCallback = itemCallback;
        this.mCustomListViewThumnailClickListener = thumnailClickListener;

        this.layoutInflater = LayoutInflater.from(activity.getApplicationContext());
        this.asyncBitmapLoader = new AsyncBitmapLoader(activity);
    }

    /**
     * 自定义ListView 适配器
     * @param activity              Activity
     * @param data                  数据
     * @param itemCallback          Item点击回调函数
     * @param thumnailClickListener Item缩略图thumnail点击回调函数
     * @param detailCallback        Item详情图片点击回调函数
     */
    public MarketCardProductAdapter(Activity activity, List<HsProduct> data,
                                    CustomListViewItemCallback itemCallback,
                                    ListViewThumnailClickListener thumnailClickListener,
                                    CustomListViewItemDetailCallback detailCallback){
        this.activity = activity;
        this.data = data;
        this.mCustomListViewItemCallback = itemCallback;
        this.mCustomListViewThumnailClickListener = thumnailClickListener;
        this.mCustomListViewItemDetailCallback = detailCallback;

        this.layoutInflater = LayoutInflater.from(activity.getApplicationContext());
        this.asyncBitmapLoader = new AsyncBitmapLoader(activity);

    }



    /**
     * ListView中单独的一项，可以复用,优化ListView性能
     * 对应custom_list_item.xml
     */
    public final class ListItem{
//        public ImageView thumnail;
        public PhotoView thumnail;
        public TextView title;
        public TextView content;
        public TextView price;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ListItem listItem = null;
        if(convertView==null){
            listItem = new ListItem();
            //获得组件，并实例化组件
            convertView = layoutInflater.inflate(R.layout.product_card,null);
            listItem.thumnail = (PhotoView) convertView.findViewById(R.id.item_thumnail);
            listItem.title = (TextView)convertView.findViewById(R.id.item_title);
            listItem.content = (TextView)convertView.findViewById(R.id.item_content);
            listItem.price = (TextView)convertView.findViewById(R.id.item_price);
            convertView.setTag(listItem);
        }else{
            listItem = (ListItem) convertView.getTag();
        }

        //绑定数据
        HsProduct tmpMap = data.get(position);
//        ImageView image = listItem.thumnail;
        PhotoView image = listItem.thumnail;
        image.enable();
         /*使用Glide加载图片*/
        Glide.with(activity)
                .load(tmpMap.getPicspath())
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .placeholder(R.drawable.img_loading)
                .error(R.drawable.loading_fail)
                .into(image);

        listItem.title.setText( tmpMap.getName());
        listItem.content.setText(tmpMap.getDescription());
        listItem.price.setText(tmpMap.getPrice()+"");
        //ListView子控件点击事件
        listItem.thumnail.setOnClickListener(this);
//        listItem.title.setOnClickListener(this);
//        listItem.content.setOnClickListener(this);
//        listItem.itemdetail.setOnClickListener(this);

        //OnItemClickListener
//        convertView.setOnClickListener(this);

        return convertView;
    }


    @Override
    public void onClick(View v) {
        Log.d(TAG, "onClick: "+v.getTag());
        switch (v.getId()){
            case R.id.item_thumnail:
                if (mCustomListViewThumnailClickListener != null) {
                    mCustomListViewThumnailClickListener.onClick(v);
                }
                break;
            case R.id.item_title:
                Log.d(TAG, "onClick: "+((TextView)v).getText());
                break;
            case R.id.item_content:
                Log.d(TAG, "onClick: "+((TextView)v).getText());
                break;
            case R.id.item_detail:
                Log.d(TAG, "onClick: "+v.getTag());
                break;
            default:
                break;
        }

    }

    /**
     * 自定义ListView Item 点击事件回调接口
     */
    public interface CustomListViewItemCallback{
        void click(View v, int position);
    }
    /**
     * 自定义Listview Item Detail 点击事件回调接口
     */
    public interface CustomListViewItemDetailCallback{
        void click(View v, int position);
    }




    /**
     * 自定义ListView Item Thumnail 点击事件抽象类
     */
    public static abstract class ListViewThumnailClickListener implements OnClickListener {
        /**
         * 基类的onClick方法
         */
        @Override
        public void onClick(View v) {
            Log.d(TAG, "onClick: "+(int)v.getTag());
            myOnClick((int)v.getTag(), v);
        }
        public abstract void myOnClick(int position, View v);
    }


}//--CustomListViewAdapter end


/*
    http://www.cnblogs.com/ivan-xu/p/4124967.html
    在Activity中响应ListView内部按钮的点击事件
    最近交流群里面有人问到一个问题：如何在Activity中响应ListView内部按钮的点击事件，不要在Adapter中响应？

    对于这个问题，我最初给他的解答是，在Adapter中定义一个回调接口，在Activity中实现该接口，从而实现对点击事件的响应。

    下班后思考了一下，觉得有两种方式都能比较好的实现：使用接口回调和使用抽象类回调。

    两种方式的区别在于:
        抽象类在Activity中实现的时候，只能定义一个成员变量来实现，不能由Activity直接实现，因为Java不支持多继承。
        而接口既可以由Activity直接实现，也可以由其成员变量来实现。
* */